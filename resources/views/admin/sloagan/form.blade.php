{{csrf_field()}}
<div class="card-body">
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label value=""> انتخاب زبان: <span class="text-danger">*</span></label>
                <select id="optlist" class="form-control" name="lang_id" value="@if(isset($data->lang_id)){{$data->lang_id}}@else {{ old('lang_id') }}@endif">
                    <option value="">انتخاب کنید </option>
                    @foreach($langs as $row)
                        <option value="{{$row['id']}}"
                                @if(isset($data->lang_id))
                                @if($data->lang_id==$row['id']) selected @endif
                            @endif
                        >{{$row['title']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="exampleFormControlInput15">نام</label><span class="text-danger">*</span>
                <input name="title" class="form-control input-air-primary"  type="text" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }} @endif">
            </div>
        </div>
    </div>

    <div class="row">


        <div class="col-md-6">
            <div class="media">
                <label class="col-form-label m-r-10">نمایش در سایت</label>
                <div class="media-body text-right">
                    <label class="switch">
                        <input value="1" @if(isset($data->status) && ($data->status == 1)) checked="checked" @endif name="status" type="checkbox" ><span class="switch-state"></span>
                    </label>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="media">
                <label class="col-form-label m-r-10">نمایش در صفحه اول</label>
                <div class="media-body text-right">
                    <label class="switch">
                        <input value="1" @if(isset($data->view_sp) && ($data->view_sp == 1)) checked="checked" @endif name="view_sp" type="checkbox" ><span class="switch-state"></span>
                    </label>
                </div>
            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیحات</label><span class="text-danger">*</span>
                <textarea name="description" class="form-control input-air-primary" id="exampleFormControlTextarea19" rows="3">@if(isset($data->description)){{$data->description}}@else {{ old('description') }} @endif</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"><span class="text-danger">*</span> تصویر کاور </label>
                <div class="col-sm-9">
                    <input name="image" class="form-control" type="file">
                    @if(isset($data->image))<img src="{{asset('assets/uploads/sloagan/'.$data->image)}}" style="height: 100px; width: 100px;" alt="">
                    @else<img src="{{asset('assets/site/images/not-found1.png')}}" style="height: 100px; width: 100px;" alt="">@endif
                </div>
            </div>
        </div>
    </div>


</div>
<input type="hidden" name="type" value="1">
<div class="card-footer">
    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
    <a class="btn btn-light btn-pill" type="reset"  href="{{URL::action('Admin\SloaganController@getSloagan')}}">بازگشت</a>
</div>
