<style>
    .sd-checkbox{height:25vh;overflow-y:scroll;background:transparent}
    .sd-checkbox .custom-ch{display:block;position:relative;padding:5px 10px;margin-bottom:12px;cursor:pointer;font-size:17px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background:transparent;box-shadow:0 0 5px 0 rgba(0,0,0,.2);border-radius:10px}
    .sd-checkbox .custom-ch input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}
    .sd-checkbox .checkmark{position:absolute;top:0;left:0;bottom:0;height:100%;width:30px;background-color:#eee;border-radius:10px 0 0 10px}
    .sd-checkbox .custom-ch:hover input~.checkmark{background-color:#ccc}
    .sd-checkbox .custom-ch input:checked~.checkmark{background-color:#2196f3}
    .sd-checkbox .checkmark:after{content:"";position:absolute;display:none}
    .sd-checkbox .custom-ch input:checked~.checkmark:after{display:block}
    .sd-checkbox .custom-ch .checkmark:after{left:10px;top:5px;width:10px;height:20px;border:solid #fff;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}
    .selectize-input .item{display:inline;margin-left:5px;background:#eee;padding:0 6px;border-radius:5px}
    @media (max-width: 576px) {
        .col-xs-6{flex: 0 0 auto;width: 50%;}
    }

</style>

{{csrf_field()}}
<div class="card-body">
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label value=""> انتخاب زبان: <span class="text-danger">*</span></label>
                <select id="optlist" class="form-control" name="lang_id" value="@if(isset($data->lang_id)){{$data->lang_id}}@else {{ old('lang_id') }}@endif">
                    <option value="">انتخاب کنید </option>
                    @foreach($langs as $row)
                        <option value="{{$row['id']}}"
                                @if(isset($data->lang_id))
                                @if($data->lang_id==$row['id']) selected @endif
                            @endif
                        >{{$row['title']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="exampleFormControlInput15">عنوان</label><span class="text-danger">*</span>
                <input name="title" class="form-control input-air-primary"  type="text" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }}@endif">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="exampleInputPassword16">آدرس url</label><span class="text-danger">*</span>
                <input name="url" class="form-control input-air-primary"  type="text" value="@if(isset($data->url)){{$data->url}}@else {{ old('url') }}@endif" >
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-6  form-group">
            <div class=" p-1">
                <label for="category_id" class="col-form-label">
                    انتخاب دسته :
                </label>
                <div class=" p-3">
                    <input type="text" class="form-control mb-2" id="myInput" onkeyup="myFunction()" placeholder="جستجو ..">
                    <div class="sd-checkbox ">
                        <ul id="myUL" class="p-0 m-0" style="list-style-type:none">
                            @foreach($categories as $key=>$row2)
                                @php $cat =\App\Models\Category::find($row2['id']);
                                @endphp
                                <li>
                                    <label class="custom-ch text-dark">

                                        {{$row2['title']}}
                                        <input  type="radio" value="{{$row2['id']}}"
                                                @if(isset($data['category_id']))
                                                @if ($data->category_id == $row2['id'])
                                                checked="checked"

                                                @endif
                                                @endif
                                                name="category_id" class="form-control"   @if(@$cat->parent_id == null) disabled @endif>
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="media">
                <label class="col-form-label m-r-10">نمایش در سایت</label>
                <div class="media-body text-right">
                    <label class="switch">
                        <input value="1" @if(isset($data->status) && ($data->status == 1)) checked="checked" @endif name="status" type="checkbox" ><span class="switch-state"></span>
                    </label>
                </div>
            </div>
            <div class="media">
                <label class="col-form-label m-r-10">نمایش در صفحه اول</label>
                <div class="media-body text-right">
                    <label class="switch">
                        <input value="1" @if(isset($data->view_sp) && ($data->view_sp == 1)) checked="checked" @endif name="view_sp" type="checkbox" ><span class="switch-state"></span>
                    </label>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیحات کوتاه</label>
                <textarea name="short_description" class="form-control input-air-primary" id="exampleFormControlTextarea19" rows="3">@if(isset($data->short_description)){{$data->short_description}}@else {{ old('short_description') }}@endif</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیحات</label><span class="text-danger">*</span>
                <textarea name="description" class="form-control input-air-primary ckeditor" id="exampleFormControlTextarea19" rows="3">@if(isset($data->description)){{$data->description}}@else {{ old('description') }}@endif</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"><span class="text-danger">*</span> تصویر  </label>
                <div class="col-sm-9">
                    <input name="image" class="form-control" type="file">
                    @if(isset($data->image))<img src="{{asset('assets/uploads/service/small/'.$data->image)}}" style="height: 100px; width: 100px;" alt="">@else<img src="{{asset('assets/site/images/not-found1.png')}}" style="height: 100px; width: 100px;" alt="">@endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"><span class="text-danger">*</span> آیکن  </label>
                <div class="col-sm-9">
                    <input name="icon" class="form-control" type="file">
                    @if(isset($data->icon))<img src="{{asset('assets/uploads/service/'.$data->icon)}}" style="height: 100px; width: 100px;" alt="">@else<img src="{{asset('assets/uploads/content/service/small/'.old('icon'))}}" style="height: 100px; width: 100px;" alt="">@endif
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="title_seo">نام سئو:</label>
            <input type="text"  class="form-control input-air-primary" name="title_seo" value="@if(isset($data->title_seo)){{$data->title_seo}}@else {{ old('title_seo') }}@endif">
        </div>


        <div class="form-group">
            <label for="des_seo">توضیحات سئو:</label>
            <textarea type="text" rows="3" class="form-control input-air-primary" name="des_seo" >@if(isset($data->des_seo)){{$data->des_seo}}@else {{ old('des_seo') }}@endif</textarea>
        </div>

    </div>
</div>
<div class="card-footer">
    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
    <a class="btn btn-light btn-pill" type="reset"  href="{{URL::action('Admin\ServiceController@getService')}}">بازگشت</a>
</div>
@section('js')
    <script type="text/javascript">
        function myFunction() {

            var input, filter, ul, li, la, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            ul = document.getElementById("myUL");
            li = ul.getElementsByTagName("li");


            for (i = 0; i < li.length; i++) {
                la = li[i].getElementsByTagName("label")[0];
                txtValue = la.textContent || la.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    </script>
@endsection
