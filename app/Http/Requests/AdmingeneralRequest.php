<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdmingeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->segment(3)){
            case 'add' :
                return[
                    'title'=>'required|min:3|max:255',
                    'image'=>'required',
                    'icon'=>'required',
                    'description'=>'required',
                    'url'=>'required'
                ];
                break;
            case 'edit' :
                return [
                    'title'=>'required|min:3|max:255',
                    'description'=>'required',
                    'url'=>'required',
                ];
                break;
            case 'delete' :
                return [
                    'deleteId' => 'required',
                ];
                break;
        }
    }
    public function messages()
    {
        return [
            'title.required' => '  عنوان اجباری است',
            'title.min'=>'نام کمتر از 3 حرف است',
            'title.max'=>'نام بیشتر از 255 کاراکتر است',
            'image.required' => ' عکس اجباری است',
            'icon.required' => ' آیکن اجباری است',
            'description.required' => ' توضیحات اجباری است',
            'url.required' => ' آدرس url اجباری است',
        ];
    }

}
