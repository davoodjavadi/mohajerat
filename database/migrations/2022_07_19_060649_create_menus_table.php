<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->string('lang_id')->nullable();
            $table->string('home')->nullable();
            $table->string('home_link')->nullable();
            $table->string('service')->nullable();
            $table->string('service_link')->nullable();
            $table->string('news')->nullable();
            $table->string('news_link')->nullable();
            $table->string('blog')->nullable();
            $table->string('blog_link')->nullable();
            $table->string('about_us')->nullable();
            $table->string('about_us_link')->nullable();
            $table->string('contact_us')->nullable();
            $table->string('contact_us_link')->nullable();
            $table->string('title_in_footer')->nullable();
            $table->string('title_contact_footer')->nullable();
            $table->string('title_phone_footer')->nullable();
            $table->string('title_email_footer')->nullable();
            $table->text('title_address_footer')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
