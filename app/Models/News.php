<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class News extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'lang_id','title','description','image','title_seo','description_seo','url','status'];


    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');
    }
}
