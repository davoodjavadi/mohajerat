<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Content;
use App\Models\Sloagan;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getIndex(){
//        $views =
        $news = Content::orderby('id', 'DESC')->News()->count();
        $arts = Content::orderby('id', 'DESC')->Article()->count();
        $slos = Sloagan::orderby('id', 'DESC')->count();
        $sloas = Sloagan::orderby('id', 'DESC')->where('status','1')->get();


        return View('admin.index')
//            ->with('views',$views)
            ->with('news',$news)
            ->with('arts',$arts)
            ->with('slos',$slos)
            ->with('sloas',$sloas);
    }
}
