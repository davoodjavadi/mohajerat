@extends('layouts.site.master')
@section('content')
    @include('layouts.site.blocks.header')
    @include('layouts.site.blocks.services')
    <div class="consultations">
        <div class="container">
            @include('layouts.site.blocks.consultations')
        </div>
        @include('layouts.site.blocks.news')
    </div>
    @include('layouts.site.blocks.banner')
    @include('layouts.site.blocks.costumer')
@stop
