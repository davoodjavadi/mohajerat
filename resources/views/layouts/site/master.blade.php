<!doctype html>
<html lang="fa" dir="rtl">

@include('layouts.site.blocks.head')

<body>
@include('layouts.message-swal')
@include('layouts.site.blocks.menu')
@yield('content')



@include('layouts.site.blocks.footer')
@include('layouts.site.blocks.script')

</body>

</html>
