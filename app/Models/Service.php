<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    protected $table='services';
    use SoftDeletes;
    protected $fillable=['title','image','icon','description','short_description','category_id','title_seo','des_seo','url','status','view_sp','lang_id'];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');

    }
    public function images(){
        return $this->hasMany('App/Models/Image','parent_id');
    }
    public function scopeShow($query)
    {
        $data = $query->where('view_sp','1');
        return $data;
    }
    public function scopeActive($query)
    {
        $data = $query->where('status','1');
        return $data;
    }
    public function comments(){
        return $this->morphMany('App\Models\Comment','Commentable')->whereNull('parent_id');
    }

    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');
    }
}
