<div class="row w-100 m-0">
    <div class="col-xl-6 col-md-6 col-sm-12 col-xs-12 align-self-center">
        <div class="whatsapp">
            <div class="back-what">
                <div class="frame">
                    <svg width="14px" height="60px" class="line line-1">
                        <path d="M2,58 C14,45 14,15 2,2"></path>
                    </svg>
                    <svg width="14px" height="60px" class="line line-2">
                        <path d="M2,58 C14,45 14,15 2,2"></path>
                    </svg>
                    <svg width="14px" height="60px" class="line line-3">
                        <path d="M2,58 C14,45 14,15 2,2"></path>
                    </svg>
                    <svg width="14px" height="60px" class="line line-4">
                        <path d="M12,58 C0,45 0,15 12,2"></path>
                    </svg>
                    <svg width="14px" height="60px" class="line line-5">
                        <path d="M12,58 C0,45 0,15 12,2"></path>
                    </svg>
                    <svg width="14px" height="60px" class="line line-6">
                        <path d="M12,58 C0,45 0,15 12,2"></path>
                    </svg>
                    <a href="{{@$lang->whatsapp}}">
                        <img src="{{asset('assets/uploads/lang/service/'.@$lang->service_image)}}" width="200" height="400">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-6 col-sm-12 col-xs-12 align-self-center">
        <div class="consultation-in">
            <h3 class="text-one fw-bolder text-center pb-5 mt-xs-5">{{ Illuminate\Support\Str::limit(@$lang->about_title, 50, $end='...')}}</h3>
            <p class="des text-justify">
                {!! Illuminate\Support\Str::limit(@$lang->about_description, 300, $end='...') !!}
            </p>
            <div class="boxes-con mt-3">
                <div class="d-flex justify-content-between">
                    <div class="">
{{--                        <a href="{{url('/service/'.@$services[0]->url)}}">--}}
                        <a href="#">
                            <div class="box-in">
                                <i class="bi bi-file-earmark-text fs-1 d-flex text-white"></i>
                            </div>
{{--                            <h5 class="text-one fw-bolder text-center mt-4">{{@$services[0]->title}}</h5>--}}
                            <h5 class="text-one fw-bolder text-center mt-4">رفتن</h5>
                        </a>
                    </div>
                    <div class="">
                        <a href="#">
                            <div class="box-in">
                                <i class="bi bi-file-earmark-text fs-1 d-flex text-white"></i>
                            </div>
                            <h5 class="text-one fw-bolder text-center mt-4">ماندن</h5>
                        </a>
                    </div>
                    <div class="">
                        <a href="#">
                            <div class="box-in">
                                <i class="bi bi-file-earmark-text fs-1 d-flex text-white"></i>
                            </div>
                            <h5 class="text-one fw-bolder text-center mt-4">شدن</h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
