<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdmingeneralRequest;
use App\Library\Helper;
use app\Library\MakeTree;
use app\Library\UploadImg;
use App\Models\Category;
use App\Models\Content;
use App\Models\Lang;
use App\Models\Service;
use App\Models\Redirects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    public static function  is_english($str){
        if (strlen($str) == strlen(utf8_decode($str))) {
            return false;
        } else {
            return true;
        }
    }
    public function getCategory(Request $request ){

        $query= Category::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.Helper::persian2LatinDigit($request->get('title')).'%');

        }

        $categories=$query->paginate(50);
        if (!empty($categories)) {
            MakeTree::getData($categories);
            $categories = MakeTree::GenerateArray(array('get'));
        }
        return view('admin.category.index')
            ->with('categories',$categories);
    }


    public function getAddCategory(){
        $langs = Lang::all();
        $categories=Category::orderby('id','DESC')->get();
        if (!empty($categories)) {
            MakeTree::getData($categories);
            $categories = MakeTree::GenerateArray(array('get'));
        }
        return View('admin.category.add')
            ->with('categories',$categories)
            ->with('langs',$langs);
    }


    public function postAddCategory(AdmingeneralRequest $request){
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        $u = Category::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();

        if ($u) {
            return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
        }

        if ($request->hasFile('image')){
            $pathMain="assets/uploads/content/cat/";
            $extension=$request->file('image')->getClientOriginalExtension();
            $fileName=md5(microtime()).".$extension";
            $request->file('image')->move($pathMain,$fileName);
            $input['image']=$fileName;
        }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }

        if ($request->hasFile('icon')){
            $pathMain="assets/uploads/content/cat/";
            $extension=$request->file('icon')->getClientOriginalExtension();
            $fileName=md5(microtime()).".$extension";
            $request->file('icon')->move($pathMain,$fileName);
            $input['icon']=$fileName;
        }else{
            return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
        }



//        dd($input);
        $category=Category::create($input);
        return Redirect::action('Admin\CategoryController@getCategory')->with('success','دسته بندی با موفقیت افزوده شد');
    }


    public function getEditCategory($id){
        $langs = Lang::all();
        $data = Category::find($id);
        $categories = Category::orderby('id','DESC')->whereNull('parent_id')->where('id','<>',$data->id)->get();
        if (!empty($categories)) {
            MakeTree::getData($categories);
            $categories = MakeTree::GenerateArray(array('get'));
        }
        return view('admin.category.edit')
            -> with('data',$data)
            ->with('categories',$categories)
            ->with('langs',$langs);
    }


    public function postEditCategory($id,AdmingeneralRequest $request){

        $input = $request->all();
        $category=Category::find($id);
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        if ($input['url'] != $category->url) {
            $u = Category::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();

            if ($u) {
                return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
            }
            $redirects=Redirects::create([
                "old_address"=>'category/'.$category->url,
                "new_address"=>'category/'.$input['url'],
            ]);
        }



        if ($request->hasFile('image')){
            File::delete('assets/uploads/content/cat/'. $category->image);
            $pathMain="assets/uploads/content/cat/";
            $extension=$request->file('image')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('image')->move($pathMain,$fileName);
                $input['image']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['image'] = $category->image;
        }
        if ($request->hasFile('icon')){
            File::delete('assets/uploads/content/cat/'. $category->icon);
            $pathMain="assets/uploads/content/cat/";
            $extension=$request->file('icon')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('icon')->move($pathMain,$fileName);
                $input['icon']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['icon'] = $category->icon;
        }
        if ($request->has('parent_id')) {
            $input['parent_id'] = $request->get('parent_id');
        } else {
            $input['parent_id'] = null;
        }
        $category->update($input);
        return Redirect::action('Admin\CategoryController@getCategory')->with('success','تغییرات دسته بندی با موفقیت افزوده شد');
    }


    public function getDeleteCategory($id){
        $category=Category::find($id);
        $service=Service::where('category_id',$category->id)->get();
        if ($service->count() > 0){
            foreach ($service as $pro){
                $redirect = Redirects::create([
                    "old_address" =>'service/'.@$pro->url,
                    "new_address" => '',]);
                Service::destroy($pro['id']);

            }
        }

        if ($category->parent_id != null){
            $redirect = Redirects::create([
                "old_address" => 'category/'.$category->url,
                "new_address" => '',

            ]);
        }
        if ($category->parent_id == null){
            $redirect = Redirects::create([
                "old_address" => 'category-detail/'.$category->url,
                "new_address" => '',

            ]);

            foreach ($category->childs as $row){

                $service2=Service::where('category_id',$row->id)->get();
                foreach ($service2 as $pro2){
                    Service::destroy($pro2['id']);
                    $redirect = Redirects::create([
                        "old_address" =>'service/'.@$pro2->url,
                        "new_address" => '',]);

                }
                $redirect = Redirects::create([
                    "old_address" => 'category/'.$category->url,
                    "new_address" => '',]);
                Category::destroy($row['id']);
            }

        }
        File::delete('assets/uploads/content/cat/'. $category->image);
        Category::destroy($id);
        return Redirect::action('Admin\CategoryController@getCategory')->with('success','دسته بندی مورد نظر با موفقیت حذف شد');
    }
}
