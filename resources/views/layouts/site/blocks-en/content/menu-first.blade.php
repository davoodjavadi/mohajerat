<div class="menu-1st">
    <div class="container">
        <div class="d-xs-none d-sm-none d-md-block">
            <div class="row w-100 m-0">
                <div class="col-md-2 col-sm-4 col-xs-6 p-0">
{{--                    <a href="{{URL::action('Site\HomeController@getIndex')}}">--}}
                        <div class="menu-in">
                            <figure class="m-0">
                                <div class="img-sr">
                                    <i class="bi bi-house fs-1 d-flex"></i>
                                </div>
                            </figure>
                            <h5 class="title-sr">
                                {{ @$menu->home }}
                            </h5>
                        </div>
{{--                    </a>--}}
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 p-0">
{{--                    <a href="{{URL::action('Site\HomeController@getCategory')}}">--}}
                        <div class="menu-in">
                            <figure class="m-0">
                                <div class="img-sr">
                                    <i class="bi bi-send fs-1 d-flex"></i>
                                </div>
                            </figure>
                            <h5 class="title-sr">
                                {{ @$menu->service }}
                            </h5>
                        </div>
{{--                    </a>--}}
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 p-0">
{{--                    <a href="{{URL::action('Site\HomeController@getNews')}}">--}}
                        <div class="menu-in">
                            <figure class="m-0">
                                <div class="img-sr">
                                    <i class="bi bi-newspaper fs-1 d-flex"></i>

                                </div>
                            </figure>
                            <h5 class="title-sr">
                                {{ @$menu->news }}
                            </h5>
                        </div>
{{--                    </a>--}}
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 p-0">
{{--                    <a href="{{URL::action('Site\HomeController@getBlog')}}">--}}
                        <div class="menu-in">
                            <figure class="m-0">
                                <div class="img-sr">
                                    <i class="bi bi-file-earmark-text fs-1 d-flex"></i>

                                </div>
                            </figure>
                            <h5 class="title-sr">
                                {{ @$menu->blog }}
                            </h5>
                        </div>
{{--                    </a>--}}
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 p-0">
{{--                    <a href="{{URL::action('Site\HomeController@getAbout')}}">--}}
                        <div class="menu-in">
                            <figure class="m-0">
                                <div class="img-sr">
                                    <i class="bi bi-info-lg fs-1 d-flex"></i>
                                </div>
                            </figure>
                            <h5 class="title-sr">
                                {{ @$menu->about_us }}
                            </h5>
                        </div>
{{--                    </a>--}}
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 p-0">
{{--                    <a href="{{URL::action('Site\HomeController@getContact')}}">--}}
                        <div class="menu-in">
                            <figure class="m-0">
                                <div class="img-sr">
                                    <i class="bi bi-telephone fs-1 d-flex"></i>
                                </div>
                            </figure>
                            <h5 class="title-sr">
                                {{ @$menu->contact_us }}
                            </h5>
                        </div>
                    {{--</a>--}}
                </div>
            </div>
        </div>

    </div>
</div>
