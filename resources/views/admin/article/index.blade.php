@extends('layouts.admin.master')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row d-flex justify-content-between">
                <div class="col-lg-6 main-header">
                    <h2 class="text-dark">مقالات</h2>

                </div>
                <div class="col-lg-6">
                    <a href="" data-target="#exampleModalCenter" data-toggle="modal" class="btn btn-pill btn-outline-dark"><i class="fa fa-search"></i></a>
                    <a href="{{URL::action('Admin\ArticleController@getAddArticle')}}" class="btn btn-pill btn-primary btn-air-primary"> افزودن </a>

                </div>
                </div>
        </div>
    </div>
<div class="container-fluid">
    <div class="card-block row">
        <div class="col-sm-12 col-lg-12 col-xl-12">
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-center text-light">
                    <tr>
                        <th>#</th>
                        <th>عکس</th>
                        <th>نام</th>
                        <th>نمایش در سایت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $key=>$art)
                    <tr>
                        <th class="text-center">
                            {{($articles->appends(Request::except('page'))->currentPage() - 1) *
                            ($articles->appends(Request::except('page'))->perPage()) + $key+1}}
                        </th>
                        <td class="text-center">
                            @if(file_exists('assets/uploads/article/small/'.$art->image))
                            <img class="rounded-circle" width="50" src="{{asset('assets/uploads/article/small/'.$art->image)}}" alt="">
                            @else
                                <img class="rounded-circle" width="50" src="{{asset('assets/site/images/not-found1.png')}}">
                            @endif
                        </td>
                        <td class="text-center">{{ Illuminate\Support\Str::limit(@$art->title,20, $end='...')}}</td>
                        <td class="text-center">@if($art->status==1) فعال @else غیرفعال@endif  </td>
                        <td class="text-center">
                            <a data-target="tooltip" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" title="حذف" class="btn btn-sm btn-danger" href="{{URL::action('Admin\ArticleController@getDeleteArticle',$art->id)}}"><i class="fa fa-trash fa-md"></i></a>
                            <a data-target="tooltip" title="ویرایش" class="btn btn-sm btn-warning"  href="{{URL::action('Admin\ArticleController@getEditArticle',$art->id)}}"><i class="fa fa-edit fa-md"></i></a>
                            @if($art->status==1)
                                <a target="_blank" data-target="tooltip" title="مشاهده در سایت" class="btn btn-sm btn-primary"  href="{{URL::action('Site\HomeController@getBlogDetail',@$art->url)}}"><i class="fa fa-eye fa-md"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagii">
                @if(count($articles))
                    {!! $articles->appends(Request::except('page'))->render() !!}
                @endif
            </div>        </div>
        {!! $articles->links() !!}
    </div>
</div>
{{--    search modal--}}
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{URL::current()}}" method="GET" class="m-0">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLabel">جستجو</h5>

                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">نام :</label>
                            <input type="text" class="form-control" name="title" >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger text-light" data-dismiss="modal">بستن</button>
                        <button type="submit" class="btn btn-success text-light">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
