<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lang extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'default',
        'image',
        'description',
        'link',
        'phone',
        'call_us',
        'mobile',
        'whatsApp',
        'instagram',
        'telegram',
        'twitter',
        'email',
        'address',
        'map',
        'url',
        'service_image',
        'service_title',
        'service_description',
        'image_middle',
        'image_middle_title',
        'image_middle_description',
        'about_title',
        'about_description',
        'title_seo',
        'description_seo',
        ];

    public function category()
    {
        return $this->hasMany('App\Models\Category');
    }

    public function service()
    {
        return $this->hasMany('App\Models\Service');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function news()
    {
        return $this->hasMany('App\Models\News');
    }

    public function article()
    {
        return $this->hasMany('App\Models\Article');
    }

    public function slider()
    {
        return $this->hasMany('App\Models\Slider');
    }

    public function sloagan()
    {
        return $this->hasMany('App\Models\Sloagan');
    }

    public function menu()
    {
        return $this->hasMany('App\Models\Menu');
    }
}
