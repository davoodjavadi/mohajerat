<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Social extends Model
{
    protected $table='socials';
    use SoftDeletes;
    protected $fillable=['title','link','icon','tooltip','status'];
}
