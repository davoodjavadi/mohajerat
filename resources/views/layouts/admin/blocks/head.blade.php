<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <meta name="robots" content="@yield('robots','noindex','nofollow')" />
    <link rel="icon" href="" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/site/images/logo.png')}}" type="image/x-icon">
    <title>پنل مدیریت</title>
    <!--font-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/fonts/fonts.css')}}">
    <!-- Font Awesome-->
    <script src="https://use.fontawesome.com/4e7411b485.js"></script>
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/feather-icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/animate.css')}}">
    <!-- Plugins css start-->

    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/prism.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/material-design-icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/pe7-icon.css')}}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/style.css?v0.4')}}">
    <link id="color" rel="stylesheet" href="{{asset('/assets/admin/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/admin/css/responsive.css')}}">
    <script src="{{asset('assets/admin/js/sweetalert.min.js')}}"></script>
    @yield('style')
</head>
