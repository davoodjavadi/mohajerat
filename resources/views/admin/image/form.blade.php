{{csrf_field()}}
<div class="card-body">
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="exampleFormControlInput15">نام</label>
                <input name="title" class="form-control input-air-primary"  type="text" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }}@endif">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"> بارگذاری  </label>
                <div class="col-sm-9">
                    <input name="image" class="form-control" type="file">
                    @if(isset($data->image))<img src="{{asset('assets/uploads/content/image/small/'.$data->image)}}" style="height: 100px; width: 100px;" alt="">@else<img src="{{asset('assets/uploads/content/image/small/'.old('image'))}}" style="height: 100px; width: 100px;" alt="">@endif
                </div>
            </div>
        </div>
    </div>


</div>
<div class="card-footer">
    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
    
</div>
