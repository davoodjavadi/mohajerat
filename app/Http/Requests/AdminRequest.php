<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->segment(3)){
            case 'add' :
                return[
                    'title'=>'required|min:3|max:255',
                    'image'=>'required',
                    'description'=>'required',
                    'lang_id' => 'required',
                ];
                break;
            case 'edit' :
                return [
                    'title'=>'required|min:3|max:255',
                    'description'=>'required',
                    'lang_id' => 'required',
                ];
                break;
            case 'delete' :
                return [
                    'deleteId' => 'required',
                ];
                break;
        }
    }
    public function messages()
    {
        return [
            'title.required' => '  نام اجباری است',
            'title.min'=>'نام کمتر از 3 حرف است',
            'title.max'=>'نام بیشتر از 255 کاراکتر است',
            'image.required' => ' عکس اجباری است',
            'description.required' => ' توضیحات اجباری است',
            'lang_id.required' => 'انتخاب زبان اجباریست.',
        ];
    }

}
