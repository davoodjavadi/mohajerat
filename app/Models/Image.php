<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    protected $table='images';
    use SoftDeletes;
    protected $fillable=['title','image','parent_id'];

    public function service(){
        return $this->belongsTo(Service::class);

    }
}
