<div class="modal fade" id="messageModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn btn-sm" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="messageModalLabel">
                    مشاهده سریع
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{URL::action('Admin\MessageController@postCommentEdit',$row->id)}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="readat" value="1">
                    <div class="row w-100 m-0">
                        <div class="col-md-12 col-sm-12 col-xs-12 p-1 align-self-center">
                            <label for=""> نمایش در صفحه</label>
                            <select class="form-control" name="status">
                                <option  disabled="disabled">
                                    انتخاب وضعیت . . .
                                </option>
                                @foreach($status as $key=>$item)
                                    <option value="{{$key}}" @if(isset($row->status) and $row->status==$key) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 p-1 align-self-center">
                            <label></label>
                            @php $com = \App\Models\Comment::where('id', $row->parent_id)->first(); @endphp
                            <h6> @if($row->commentable_type == 'App\Models\Product')
                                    مربوط به: محصول/ {{@$row->product->title}}
                                @elseif($row->commentable_type == 'App\Models\Article')مربوط به: مقاله/ {{@$row->article->title}}
                                @elseif($row->commentable_type == 'App\Models\News')مربوط به: خبر/ {{@$row->news->title}}

                                @endif </h6>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">نام :</label>
                                    <input class="form-control" type="text" name="name" disabled value="@if(isset($row->name)) {{$row->name}} @endif">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">ایمیل</label>
                                    <input class="form-control" type="text" name="content" disabled value="@if(isset($row->name)) {{$row->name}} @endif">
                                </div>
                            </div>
                            @if(isset($row->title))
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">نام گیرنده</label>
                                    <input class="form-control" type="text" name="title" disabled value="{{$row->title}}">
                                </div>
                            </div>
                            @endif

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 p-1 align-self-center">
                            <label>پیام:</label>
                            <textarea class="form-control" type="text" rows="5" id="message" name="message" disabled
                            >@if(isset($row->message)){{$row->message}}@endif</textarea>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 px-1 pt-3 align-self-center">
                            <button type="submit" class="btn btn-success form-control"> ذخیره </button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>
