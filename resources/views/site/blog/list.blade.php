@extends('layouts.site.master')
@section('title'){{'مقالات'}}@stop
@section('image_seo'){{ asset('assets/uploads/content/'.@$setting_main->logo)}}@stop
@section('description')@stop

@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder">لیست وبلاگ </h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>
                        خانه</a></li>
                <li class="breadcrumb-item active" aria-current="page">لیست وبلاگ</li>
            </ol>
        </div>

    </nav>
    <div class="content py-3">
        <div class="blog-list mt-3">
            <div class="container">
                <div class="row w-100 m-0">
                    @foreach($blogs as $row)
                    <div class="col-xl-3 col-md-4 col-sm-6  p-1">
                        <a href="{{URL::action('Site\HomeController@getBlogDetail',@$row->url)}}">
                            <div class="blog-box border rounded-3">
                                <figure class="m-0">
                                    <div class="img-box fullup-me position-relative">
                                        <img src="{{asset('assets/uploads/article/medium/'.@$row->image)}}" class="w-100">
                                        <span class="caption">
                                        </span>
                                        <h5 class="position-absolute top-0 end-0 start-0 bottom-0 d-flex justify-content-center align-items-center text-white fw-bolder">{{@$row->title}}</h5>
                                    </div>
                                </figure>
                                <div class="title-blog mt-2 p-2">
                                    <div class="date my-1 d-flex justify-content-between align-items-center">
                                        <span class="d-flex align-items-center text-two"><i
                                                class="bi bi-calendar-event me-1 text-two"></i>{{jdate('Y/m/d',@$row->created_at->timestamp)}}</span>
                                    </div>

                                    <p class="text text-dark">
                                        {!! @$row->short_description !!}</p>

                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
