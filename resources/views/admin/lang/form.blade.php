{{csrf_field()}}
<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleFormControlInput15">زبان (کشور)</label><span class="text-danger"> * </span>
                <input name="title" class="form-control input-air-primary"  type="text" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"> تصویر (پرچم)</label>
                <div class="col-sm-9">
                    <input name="image" class="form-control" type="file">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group row">
                    @if(isset($data->image))<img src="{{asset('assets/uploads/lang/small/'.$data->image)}}" style="height: 100px; width: 100px;" alt="">@else<img src="{{asset('assets/site/images/not-found1.png')}}" style="height: 100px; width: 100px;" alt="">@endif
        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title_seo">لینک آدرس :</label><span class="text-danger"> * </span>
                <input type="text"  class="form-control input-air-primary" name="url" value="@if(isset($data->url)){{$data->url}}@else {{ old('url') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">تلفن:</label>
                <input type="text"  class="form-control input-air-primary" name="phone" value="@if(isset($data->phone)){{$data->phone}}@else {{ old('phone') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن "با ما تماس بگیرید" :</label>
                <input type="text"  class="form-control input-air-primary" name="call_us" value="@if(isset($data->call_us)){{$data->call_us}}@else {{ old('call_us') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">موبایل:</label>
                <input type="text"  class="form-control input-air-primary" name="mobile" value="@if(isset($data->mobile)){{$data->mobile}}@else {{ old('mobile') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">لینک واتساپ:</label>
                <input type="text"  class="form-control input-air-primary" name="whatsApp" value="@if(isset($data->whatsApp)){{$data->whatsApp}}@else {{ old('whatsApp') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">لینک اینستاگرام :</label>
                <input type="text"  class="form-control input-air-primary" name="instagram" value="@if(isset($data->instagram)){{$data->instagram}}@else {{ old('instagram') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">لینک توییتر:</label>
                <input type="text"  class="form-control input-air-primary" name="twitter" value="@if(isset($data->twitter)){{$data->twitter}}@else {{ old('twitter') }}@endif">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">لینک تلگرام:</label>
                <input type="text"  class="form-control input-air-primary" name="telegram" value="@if(isset($data->telegram)){{$data->telegram}}@else {{ old('telegram') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">پست الکترونیک :</label>
                <input type="text"  class="form-control input-air-primary" name="email" value="@if(isset($data->email)){{$data->email}}@else {{ old('email') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">آدرس : </label>
                <textarea name="address" class="form-control input-air-primary" rows="3">@if(isset($data->address)){{$data->address}}@else {{ old('address') }}@endif</textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">نقشه گوگل : </label>
                <textarea name="map" class="form-control input-air-primary"  rows="3">@if(isset($data->map)){{$data->map}}@else {{ old('map') }}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان خدمات:</label>
                <input type="text"  class="form-control input-air-primary" name="service_title" value="@if(isset($data->service_title)){{$data->service_title}}@else {{ old('service_title') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان تصویر وسط صفحه :</label>
                <input type="text"  class="form-control input-air-primary" name="image_middle_title" value="@if(isset($data->image_middle_title)){{$data->image_middle_title}}@else {{ old('image_middle_title') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان "درباره ما" :</label>
                <input type="text"  class="form-control input-air-primary" name="about_title" value="@if(isset($data->about_title)){{$data->about_title}}@else {{ old('about_title') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیح خدمات : </label>
                <textarea name="service_description" class="form-control input-air-primary" rows="3">@if(isset($data->service_description)){{$data->service_description}}@else {{ old('service_description') }}@endif</textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیح روی تصویر وسط صفحه : </label>
                <textarea name="image_middle_description" class="form-control input-air-primary" rows="3">@if(isset($data->image_middle_description)){{$data->image_middle_description}}@else {{ old('image_middle_description') }}@endif</textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیح درباره ما : </label>
                <textarea name="about_description" class="form-control input-air-primary" rows="3">@if(isset($data->about_description)){{$data->about_description}}@else {{ old('about_description') }}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title_seo">عنوان سئو :</label>
                <input type="text"  class="form-control input-air-primary" name="title_seo" value="@if(isset($data->title_seo)){{$data->title_seo}}@else {{ old('title_seo') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleFormControlTextarea19">توضیحات سئو : </label>
                <textarea name="description_seo" class="form-control input-air-primary" rows="3">@if(isset($data->description_seo)){{$data->description_seo}}@else {{ old('description_seo') }}@endif</textarea>
            </div>
        </div>
    </div>


    <div class="row">
            <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"> تصویر خدمات (واتساپ) </label>
                <div class="col-sm-9">
                    <input name="service_image" class="form-control" type="file">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                @if(isset($data->service_image))<img src="{{asset('assets/uploads/lang/service/'.$data->service_image)}}" style="height: 100px; width: 100px;" alt="">@else<img src="{{asset('assets/site/images/not-found1.png')}}" style="height: 100px; width: 100px;" alt="">@endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"> تصویر وسط صفحه : </label>
                <div class="col-sm-9">
                    <input name="image_middle" class="form-control" type="file">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                @if(isset($data->image_middle))<img src="{{asset('assets/uploads/lang/middle/small/'.$data->image_middle)}}" style="height: 100px; width: 100px;" alt="">@else<img src="{{asset('assets/site/images/not-found1.png')}}" style="height: 100px; width: 100px;" alt="">@endif
            </div>
        </div>
    </div>


</div>
<input type="hidden" name="type" value="2">
<div class="card-footer">
    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
    <a class="btn btn-light btn-pill" type="reset"  href="{{URL::action('Admin\LangController@getLang')}}">بازگشت</a>
</div>
