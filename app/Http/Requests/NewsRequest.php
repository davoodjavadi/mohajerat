<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            switch ($this->segment(3)){
                case 'add' :
                    return[
                        'title'=>'required',
                        'image'=>'required|image',
                        'description'=>'required',
                        'lang_id'=>'required',
                    ];
                    break;
                case 'edit' :
                    return [
                        'title'=>'required',
//                        'image'=>'required|image',
                        'description'=>'required',
                        'lang_id'=>'required'
                    ];
                    break;
                case 'delete' :
                    return [
                        'deleteId' => 'required',
                    ];
                    break;
            }
        }
        public function messages()
    {
        return [
            'title.required' => 'نوشتن عنوان خبر ضروریست.',
            'description.required' => 'نوشتن شرح خبر ضروریست.',
            'lang_id.required' => 'انتخاب زبان ضروریست.',
            'image.required' => 'ارسال تصویر ضروریست.',
            'image.image' => 'تصویر ارسالی نامعتبرست.',

        ];
    }
}
