<div class="sidebar d-sm-none d-xs-none d-md-block">
    <div class="sticky">
        <div class="related mb-3">
            <div class="row w-100 m-0">
                @if($related->isNotEmpty())
                <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">
                    <div class="title-related">
                        <p class="h5 text-end fw-bolder text-one">
                            بلاگ مرتبط
                        </p>
                    </div>
                </div>
                @foreach($related as $rel)
                <div class="col-xl-12 col-md-12 col-sm-12 p-0 py-2 ">
                    <a href="{{URL::action('Site\HomeController@getBlogDetail',@$rel->url)}}">
                        <div class="related-box">
                            <div class="row w-100 m-0">
                                <div
                                    class="col-xl-9 col-md-9 col-sm-8 col-xs-8 p-0 pe-2 align-self-center">
                                    <p class="h6 m-0 fw-bolder text-end">{{@$rel->title}}</p>
                                </div>
                                <div
                                    class="col-xl-3 col-md-3 col-sm-4 col-xs-4 p-0 align-self-center h-60">
                                    <div class="border-box h-100">
                                        <div class="img-box h-100">
                                            @if(file_exists('assets/uploads/article/small/'.@$rel->image))
                                            <img src="{{asset('assets/uploads/article/small/'.@$rel->image)}}">
                                            @else
                                                <img src="{{asset('assets/site/images/not-found1.png')}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                    @endif
            </div>
        </div>
{{--        <div class="cat-side mb-3">--}}
{{--            <div class="row w-100 m-0">--}}
{{--                <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">--}}
{{--                    <div class="title-cat">--}}
{{--                        <p class="h5 text-end fw-bolder text-one">--}}
{{--                            دسته بندی--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-xl-12 col-md-12 col-sm-12 p-0 py-2">--}}
{{--                    <a href="">--}}
{{--                        <div class="cat-box py-1 ms-4 m-xs-0">--}}
{{--                            <p--}}
{{--                                class="h6 fw-bolder text-end text-dark d-flex justify-content-end align-items-center text-dark">--}}
{{--                                زندگی--}}

{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-xl-12 col-md-12 col-sm-12 p-0 py-2">--}}
{{--                    <a href="">--}}
{{--                        <div class="cat-box py-1 ms-4 m-xs-0">--}}
{{--                            <p--}}
{{--                                class="h6 fw-bolder text-end text-dark d-flex justify-content-end align-items-center text-dark">--}}
{{--                                مشاوره--}}

{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-xl-12 col-md-12 col-sm-12 p-0 py-2">--}}
{{--                    <a href="">--}}
{{--                        <div class="cat-box py-1 ms-4 m-xs-0">--}}
{{--                            <p--}}
{{--                                class="h6 fw-bolder text-end text-dark d-flex justify-content-end align-items-center text-dark">--}}
{{--                                اوکراین--}}

{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="tags ms-4 mb-3 m-xs-0">--}}
{{--            <div class="row w-100 m-0">--}}
{{--                <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">--}}
{{--                    <div class="title-cat mt-2">--}}
{{--                        <p class="h5 text-end fw-bolder text-one">--}}
{{--                            برچسب ها--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="tag-list justify-content-end p-0">--}}
{{--                    <a href="">--}}
{{--                        <div class="tag-box">--}}
{{--                            <p class="h6 fw-bolder m-0">--}}
{{--                                زندگی--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <a href="">--}}
{{--                        <div class="tag-box">--}}
{{--                            <p class="h6 fw-bolder m-0">--}}
{{--                                زندگی--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <a href="">--}}
{{--                        <div class="tag-box">--}}
{{--                            <p class="h6 fw-bolder m-0">--}}
{{--                                زندگی--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <a href="">--}}
{{--                        <div class="tag-box">--}}
{{--                            <p class="h6 fw-bolder m-0">--}}
{{--                                زندگی--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <a href="">--}}
{{--                        <div class="tag-box">--}}
{{--                            <p class="h6 fw-bolder m-0">--}}
{{--                                زندگی--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <a href="">--}}
{{--                        <div class="tag-box">--}}
{{--                            <p class="h6 fw-bolder m-0">--}}
{{--                                زندگی--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="share ms-4 m-xs-0">
            <div class="row w-100 m-0">
                <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">
                    <div class="title-cat mt-2">
                        <p class="h5 text-end fw-bolder text-one">
                            اشتراک گذاری در :
                        </p>
                    </div>
                </div>
                <div class="share-list p-0 d-flex justify-content-end">
                    <a href="https://www.instagram.com/?url={{url('/'.@$blog->url)}}">
                        <i class="bi bi-instagram d-flex  fs-4"></i>
                    </a>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{url('/'.@$blog->url)}}">
                        <i class="bi bi-facebook d-flex  fs-4"></i>
                    </a>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url('/'.@$blog->url)}}">
                        <i class="bi bi-linkedin d-flex  fs-4"></i>
                    </a>
                    <a href="whatsapp://send?text={{url('/'.@$blog->url)}}">
                        <i class="bi bi-whatsapp d-flex  fs-4"></i>
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
