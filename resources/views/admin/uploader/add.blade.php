@extends("layouts.admin.master")

@section('content')
    <div class="card card-outline-primary rounded border m-md-1 mb-md-5 p-md-3">
        <div class="card-header"><i class="text-info fa fa-plus  fa-md"></i>&nbsp;آپلود عکس</div>
        <div class="card-body">
            <form action="{{URL::action('Admin\UploaderController@postAddUploader')}}" method="post" enctype="multipart/form-data">


                {{  csrf_field() }}
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">نام عکس<span class="text-danger">*</span>:</label>
                            <input type="text" class="form-control" name="title" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }}  @endif">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="image">عکس<span class="text-danger">*</span>:</label>
                            <input type="file" class="form-control" name="image" >
                        </div>

                    </div>
                </div>

                <div class="box-footer">

                    <button type="submit" class="btn btn-success form-control">ذخیره</button>
                </div>

            </form>
        </div>
@endsection
