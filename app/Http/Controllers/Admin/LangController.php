<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Lang;
use App\Http\Requests\LangRequest;
use App\Models\Redirects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class LangController extends Controller
{
    public static function  is_english($str){
        if (strlen($str) == strlen(utf8_decode($str))) {
            return false;
        } else {
            return true;
        }
    }



    public function getLang(Request $request){

        $query= Lang::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.Helper::persian2LatinDigit($request->get('title')).'%');
        }

        $langs=$query->paginate(50);
        return view('admin.lang.index')
            ->with('langs',$langs);
    }




    public function getAddLang(){
        $langs = Lang::orderby('id','DESC')->get();
        return View('admin.lang.add')
            ->with('langs',$langs);
    }





    public function postAddLang(LangRequest $request){
        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/lang/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }

        if ($request->hasFile('service_image')) {
            $pathMain = "assets/uploads/lang/service/";
            //$extension = $request->file('file')->getClientOriginalExtension();
            if (true) {
                $fileName = mt_rand(1000, 9999) . ".jpeg";
                $request->file('service_image')->move($pathMain, $fileName);
                $input['service_image'] = $fileName;
            }
        }

        if ($request->hasFile('image_middle')) {
            $pathM = "assets/uploads/lang/middle/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image_middle'), $pathM);
            if($fileName){
                $input['image_middle'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }

//        dd($input);
        $news = Lang::create($input);
        return Redirect::action('Admin\LangController@getLang')->with('success','آیتم با موفقیت افزوده شد');
    }




    public function getEditLang($id){
        $data = Lang::find($id);
        return view('admin.lang.edit')
            -> with('data',$data);
    }



    public function postEditLang($id, LangRequest $request){

        $input = $request->all();
        //dd($input);
        $lang = Lang::find($id);

        if ($request->hasFile('image')) {
            $path = "assets/uploads/lang/";
            File::delete($path . $lang->image);

            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
//        }
//        else {
//            $input['image'] = $lang->image;
        }

        if ($request->hasFile('service_image')) {
            $pathMain = "assets/uploads/lang/service/";
            File::delete($pathMain . $lang->image);
            //$extension = $request->file('file')->getClientOriginalExtension();
            if (true) {
                $fileName = mt_rand(1000, 9999) . ".jpeg";
                $request->file('service_image')->move($pathMain, $fileName);
                $input['service_image'] = $fileName;
            }
        }

        if ($request->hasFile('image_middle')) {
            $pathM = "assets/uploads/lang/middle";
            File::delete($pathM . $lang->image);
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image_middle'), $pathM);
            if($fileName){
                $input['image_middle'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }

        $lang->update($input);
        return Redirect::action('Admin\LangController@getLang')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }



    public function getDeleteLang($id){
        $lang = Lang::find($id);
        $path = "assets/uploads/lang/";
        File::delete($path . $lang->image);

        Lang::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }



    public function setDefaultLang(Request $request)
    {
        $id = $request['default'];
        $default = Lang::find($id);
        $def = $default->update([
            'default' => 1,
        ]);
        $another = Lang::where('id', '<>', $default->id)->get();

        foreach ($another as $row) {
            if($row->default == 1){
                $row->update([
                    'default' => 0,
                ]);
            }
        }

        return back()->with('success', 'زبان مورد نظر به عنوان پیش فرض ثبت شد.');
    }
}
