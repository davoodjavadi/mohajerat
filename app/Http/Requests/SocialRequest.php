<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->segment(3)){
            case 'add' :
                return[
                    'title'=>'required|min:3|max:255',
                    'link'=>'required',
                    'icon'=>'required',
                ];
                break;
            case 'edit' :
                return [
                    'title'=>'required|min:3|max:255',
                    'link'=>'required',
                    'icon'=>'required',
                ];
                break;
            case 'delete' :
                return [
                    'deleteId' => 'required',
                ];
                break;
        }
    }
    public function messages()
    {
        return [
            'title.required' => '  نام اجباری است',
            'title.min'=>'نام کمتر از 3 حرف است',
            'title.min'=>'نام بیشتر از 255 حرف است',
            'link.required' => ' لینک اجباری است',
            'icon.required' => ' آیکون اجباری است',

        ];
    }

}
