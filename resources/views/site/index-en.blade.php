@extends('layouts.site.master-en')
@section('content')
        @include('layouts.site.blocks-en.header')
        <div class="consultations">
            <div class="container">
                @include('layouts.site.blocks-en.consultations')
            </div>
        </div>
        @include('layouts.site.blocks-en.banner')
        @include('layouts.site.blocks-en.costumer')

@stop
