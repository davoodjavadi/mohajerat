<!doctype html>
<html lang="en" dir="ltr">

@include('layouts.site.blocks.head')

<body>
<div class="en">
@include('layouts.message-swal')
@include('layouts.site.blocks-en.menu')
@yield('content')



@include('layouts.site.blocks-en.footer')
@include('layouts.site.blocks-en.script')
</div>
</body>

</html>
