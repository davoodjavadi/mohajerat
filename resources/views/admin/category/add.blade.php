@extends('layouts.admin.master')
@section('content')
<div class="card border">
    <div class="card-header">
        <h5>افزودن دسته بندی</h5>
    </div>
    <form class="form theme-form" action="{{URL::action('Admin\CategoryController@postAddCategory')}}" method="POST" enctype="multipart/form-data">
@include('admin.category.form')
    </form>
</div>
@endsection
