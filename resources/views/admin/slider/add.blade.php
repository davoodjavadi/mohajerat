@extends('layouts.admin.master')
@section('content')
<div class="card border">
    <div class="card-header">
        <h5>افزودن تصویر</h5>
    </div>
    <form class="form theme-form" action="{{URL::action('Admin\SliderController@postAddSlider')}}" method="POST" enctype="multipart/form-data">
@include('admin.slider.form')
    </form>
</div>
@endsection
