@extends('layouts.admin.master')
@section('content')
    <div class="card border">
        <div class="card-header">
            <h5>ویرایش مقاله</h5>
        </div>
        <form class="form theme-form" action="{{URL::action('Admin\ArticleController@postEditArticle',$data->id)}}" method="POST" enctype="multipart/form-data">
            @include('admin.article.form')

        </form>
    </div>
@endsection
