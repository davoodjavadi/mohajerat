<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdmingeneralRequest;
use App\Http\Requests\NewsRequest;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Content;
use App\Models\Category;
use App\Models\Lang;
use App\Models\News;
use App\Models\Service;
use App\Models\Redirects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class NewsController extends Controller
{
    public static function  is_english($str){
        if (strlen($str) == strlen(utf8_decode($str))) {
            return false;
        } else {
            return true;
        }
    }

    public function getNews(Request $request ){

        $query = News::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.Helper::persian2LatinDigit($request->get('title')).'%');

        }

        $newses=$query->paginate(50);
        return view('admin.news.index')
            ->with('newses',$newses);
    }


    public function getAddNews(){
        $langs = Lang::all();
        $newses= News::orderby('id','DESC')->get();
        return View('admin.news.add')
            ->with('newses',$newses)
            ->with('langs',$langs);
    }


    public function postAddNews(NewsRequest $request){
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        $u = News::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();

        if ($u) {
            return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
        }
        if ($request->hasFile('image')) {
            $path = "assets/uploads/news/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }

//        dd($input);
        $news= News::create($input);
        return Redirect::action('Admin\NewsController@getNews')->with('success','آیتم با موفقیت افزوده شد');
    }


    public function getEditNews($id){
        $data = News::find($id);
        $langs = Lang::all();

        return view('admin.news.edit')
            -> with('data',$data)
            -> with('langs',$langs);
    }


    public function postEditNews($id,NewsRequest $request){

        $input = $request->all();
        //dd($input);
        $news=News::find($id);
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        if ($input['url'] != $news->url) {
            $u = Content::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();

            if ($u) {
                return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
            }

            $redirects=Redirects::create([
                "old_address"=>'news/'.$news->url,
                "new_address"=>'news/'.$input['url'],
            ]);
        }



        if ($request->hasFile('image')) {
            $path = "assets/uploads/news/";
            File::delete($path . '/big/' . $news->image);
            File::delete($path . '/medium/' . $news->image);
            File::delete($path . '/small/' . $news->image);
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        else {
            $input['image'] = $news->image;
        }

        $news->update($input);
        return Redirect::action('Admin\NewsController@getNews')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }




    public function getDeleteNews($id){
        $news = News::find($id);
        $redirects=Redirects::create([
            "old_address"=>'news/'.$news->url,
            "new_address"=>'',
        ]);
        $path = "assets/uploads/news/";
        File::delete($path . '/big/' . $news->image);
        File::delete($path . '/medium/' . $news->image);
        File::delete($path . '/small/' . $news->image);
        News::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }
}
