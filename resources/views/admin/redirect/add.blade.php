@extends("layouts.admin.master")

@section('content')
    <div class="card card-outline-primary rounded border m-md-1 mb-md-5 p-md-3">
        <div class="card-header"><i class="text-info fa fa-plus  fa-md"></i>&nbsp;ریدایرکت </div>
        <div class="card-body">
            <form action="{{URL::action('Admin\RedirectController@postRedirectAdd')}}" method="post" enctype="multipart/form-data">


                {{  csrf_field() }}

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="old_address">ادرس قدیمی <span class="text-danger">*</span>:</label>
                            <input type="text" class="form-control" name="old_address" value="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="new_address">ادرس جدید <span class="text-danger">*</span>:</label>
                            <input type="text" class="form-control" name="new_address" value="">
                        </div>
                    </div>



                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="type">نوع ریدایرکت :</label>
                        <select name="type" class="form-control" id="">
                            <option value="1">301</option>
                            <option value="2">302</option>
                        </select>
                    </div>
                </div>





                <div class="box-footer">

                    <button type="submit" class="btn btn-success form-control">ذخیره</button>
                </div>




            </form>
        </div>
@endsection
