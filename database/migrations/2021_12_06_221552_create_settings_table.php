<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id()->BigIncrement();

            $table->string('title')->nullable();
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->string('email')->nullable();
            $table->string('email2')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_number2')->nullable();
            $table->string('whatsapp_number')->nullable();
            $table->text('contact_address')->nullable();
            $table->text('map')->nullable();
            $table->string('index_title')->nullable();
            $table->text('index_description')->nullable();
            $table->string('aboutindex_title')->nullable();
            $table->text('aboutindex_description')->nullable();

            $table->string('about_title')->nullable();
            $table->string('about_img')->nullable();
            $table->text('about_description')->nullable();
            $table->string('aboutfooter_title')->nullable();
            $table->text('aboutfooter_description')->nullable();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
