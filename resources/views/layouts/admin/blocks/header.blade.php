<div class="page-main-header">
    <div class="main-header-right">
        <div class="main-header-left text-center">
            <div class="logo-wrapper"><a href=""><img src="" alt=""></a></div>
        </div>
        <div class="mobile-sidebar">
            <div class="media-body text-right switch-sm">
                <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
            </div>
        </div>
        <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar">               </i></div>
        <div class="col-md-12  " style="position: absolute; left: 0; top: 50%; transform:translate(0px,-50%);
">

        <li><a data-toggle="tooltip" data-placement="right" title="خروج" class="bar-icons" href="{{url('/logout')}}"><i class=" text-danger fa fa-arrow-left fa-lg"></i></a>


        </div>

        <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">
                <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
                <div class="ProfileCard-details">
                    <div class="ProfileCard-realName"></div>
                </div>
            </div>
        </script>
    </div>
</div>
