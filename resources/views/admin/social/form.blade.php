{{csrf_field()}}
<div class="card-body">
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="exampleFormControlInput15">نام</label>
                <input name="title" class="form-control input-air-primary"  type="text" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }} @endif">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="exampleInputPassword16">لینک</label>
                <p>(مثل :https://www.instagram.com/user_name)</p>
                <input name="link" class="form-control input-air-primary"  type="text" value="@if(isset($data->link)){{$data->link}}@else {{ old('link') }} @endif" >
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-4">
            <div class="media">
                <label class="col-form-label m-r-10">نمایش در سایت</label>
                <div class="media-body text-right">
                    <label class="switch">
                        <input value="1" @if(isset($data->status) && ($data->status == 1)) checked="checked" @endif name="status" type="checkbox" ><span class="switch-state"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="form-group">
            <label for="title_seo">نام آیکن:<button class="btn btn-pill btn-primary" type="button" data-toggle="modal" data-target="#exampleModalCenter">راهنما</button></label>
            <input type="text"  class="form-control input-air-primary" name="icon" value="@if(isset($data->icon)){{$data->icon}}@endif">
        </div>


    </div>
</div>
<div class="card-footer">
    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
    <a class="btn btn-light btn-pill" type="reset"  href="{{URL::action('Admin\SocialController@getSocial')}}">بازگشت</a>
</div>

@section('modal')
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">نمادهای برند</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="row icon-lists">
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-facebook"></i> fa-facebook</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-facebook-square"></i> fa-facebook-square</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-google-plus-square"></i> fa-google-plus-square</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-google-plus"></i> fa-google-plus</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-google-plus"></i> fa-google-plus</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-instagram"></i> fa-instagram</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-linkedin"></i> fa-linkedin</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-paypal"></i> fa-paypal</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-skype"></i> fa-skype</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-snapchat-square"></i> fa-snapchat-square</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-soundcloud"></i> fa-soundcloud</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-spotify"></i> fa-spotify</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-telegram"></i> fa-telegram</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-tumblr-square"></i> fa-tumblr-square</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-twitch"></i> fa-twitch</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-twitter-square"></i> fa-twitter-square</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-vimeo"></i> fa-vimeo</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-whatsapp"></i> fa-whatsapp</div>
                                <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-youtube"></i> fa-youtube</div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
@endsection
