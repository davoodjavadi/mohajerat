<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class AdminPermission
{
    public function __construct(Guard $auth)
    {

        $this->auth = $auth;

    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->check() && Auth::user()->admin == 1&& Auth::user()->status == 1) {
            return $next($request);
        }
        return redirect('/login');
    }
}
