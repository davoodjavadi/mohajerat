<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Lang;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class MenuController extends Controller
{
//    public static function  is_english($str){
//        if (strlen($str) == strlen(utf8_decode($str))) {
//            return false;
//        } else {
//            return true;
//        }
//    }



    public function getMenu(Request $request){

        $menu = Menu::orderBy('id','DESC')->paginate(50);
        return view('admin.menu.index')
            ->with('menu',$menu);
    }




    public function getAddMenu(){
        $langs = Lang::orderby('id','DESC')->get();
        return View('admin.menu.add')
            ->with('langs',$langs);
    }





    public function postAddMenu(Request $request){
        $input = $request->all();
        $data = $this->validate($request, [
            'lang_id' => 'required',
        ], [
            'lang_id.required' => 'انتخاب زبان الزامیست.',
        ]);

//        dd($input);
        $menu = Menu::create($input);
        return Redirect::action('Admin\MenuController@getMenu')->with('success','آیتم با موفقیت افزوده شد');
    }




    public function getEditMenu($id){
        $data = Menu::find($id);
        $langs = Lang::all();

        return view('admin.menu.edit')
            -> with('data',$data)
            -> with('langs',$langs);
    }



    public function postEditMenu($id, Request $request){

        $input = $request->all();
        //dd($input);
        $menu = Menu::find($id);

        $menu->update($input);
        return Redirect::action('Admin\MenuController@getMenu')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }



    public function getDeleteMenu($id){
        $menu = Menu::find($id);

        Menu::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }


}
