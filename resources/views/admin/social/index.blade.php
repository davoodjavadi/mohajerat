@extends('layouts.admin.master')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row d-flex justify-content-between">
                <div class="col-lg-6 main-header">
                    <h2 class="text-dark">سوشال مدیا</h2>

                </div>
                <div class="col-lg-6">
                    <a href="" data-target="#exampleModalCenter" data-toggle="modal" class="btn btn-pill btn-outline-dark"><i class="fa fa-search"></i></a>
                    <a href="{{URL::action('Admin\SocialController@getAddSocial')}}" class="btn btn-pill btn-primary btn-air-primary"> افزودن </a>
                </div>
                </div>
        </div>
    </div>
<div class="container-fluid">
    <div class="card-block row">
        <div class="col-sm-12 col-lg-12 col-xl-12">
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-center text-light">
                    <tr>
                        <th>#</th>
                        <th>آیکن</th>
                        <th>نام</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($socials as $key=>$social)
                    <tr>
                        <th class="text-center">
                            {{($socials->appends(Request::except('page'))->currentPage() - 1) *
                            ($socials->appends(Request::except('page'))->perPage()) + $key+1}}
                        </th>
                        <td class="text-center"><i class="fa {{@$social->icon}} "></i></td>
                        <td class="text-center"> {{@$social->title}} </td>
                        <td class="text-center">@if($social->status==1) فعال @else غیرفعال@endif  </td>
                        <td class="text-center">
                            <a data-target="tooltip" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" title="حذف" class="btn btn-sm btn-danger" href="{{URL::action('Admin\SocialController@getDeleteSocial',$social->id)}}"><i class="fa fa-trash fa-md"></i></a>
                            <a data-target="tooltip" title="ویرایش" class="btn btn-sm btn-warning"  href="{{URL::action('Admin\SocialController@getEditSocial',$social->id)}}"><i class="fa fa-edit fa-md"></i></a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagii">
                @if(count($socials))
                    {!! $socials->appends(Request::except('page'))->render() !!}
                @endif
            </div>        </div>
    </div>
</div>
{{--    search modal--}}
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{URL::current()}}" method="GET" class="m-0">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLabel">جستجو</h5>

                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">نام :</label>
                            <input type="text" class="form-control" name="title" >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger text-light" data-dismiss="modal">بستن</button>
                        <button type="submit" class="btn btn-success text-light">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
