<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryRequest;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Content;
use App\Models\Lang;
use App\Models\Slider;
use App\Models\Redirects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SliderController extends Controller
{
    public function getSlider(Request $request ){

        $query= Slider::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.Helper::persian2LatinDigit($request->get('title')).'%');

        }

        $sliders=$query->paginate(50);
        return view('admin.slider.index')
            ->with('sliders',$sliders);
    }



    public function getSliderLang($id){

        $sliders=Slider::where('lang_id',$id)->paginate(50);
        return view('admin.slider.index')
            ->with('sliders',$sliders);
    }


    public function getAddSlider(){
        $sliders = Slider::orderby('id','DESC')->get();
        $langs = Lang::all();
        return View('admin.slider.add')
            ->with('sliders',$sliders)
            ->with('langs',$langs);
    }

    public function postAddSlider(GalleryRequest $request){
        $input = $request->all();
        $input['status'] = $request->has('status');

        if ($request->hasFile('image')){
            $pathMain="assets/uploads/slider/";
            $extension=$request->file('image')->getClientOriginalExtension();
            $fileName=md5(microtime()).".$extension";
            $request->file('image')->move($pathMain,$fileName);
            $input['image']=$fileName;
        }else{
            return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
        }

//        dd($input);
        $slider= Slider::create($input);
        return Redirect::action('Admin\SliderController@getSlider')->with('success','آیتم با موفقیت افزوده شد');
    }


    public function getEditSlider($id){
        $data = Slider::find($id);
        $langs = Lang::all();

        return view('admin.slider.edit')
            -> with('data',$data)
            -> with('langs',$langs);
    }


    public function postEditSlider($id, GalleryRequest $request){

        $input = $request->all();
        $slider= Slider::find($id);
        $input['status'] = $request->has('status');

//
//        if ($request->get('url') != $slider->url){
//            $redirects=Redirects::create([
//                "old_address"=>$slider->url,
//                "new_address"=>$input['url'],
//            ]);
//        }
        if ($request->hasFile('image')){
            File::delete('assets/uploads/slider/'. $slider->image);
            $pathMain="assets/uploads/slider/";
            $extension=$request->file('image')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('image')->move($pathMain,$fileName);
                $input['image']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['image'] = $slider->image;
        }

        $slider->update($input);
        return Redirect::action('Admin\SliderController@getSlider')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }


    public function getDeleteSlider($id){
        $slider= Slider::find($id);
//        $redirect = Redirects::create([
//            "old_address" => @$slider->url,
//            "new_address" => '',
//
//        ]);
        File::delete('assets/uploads/slider/'. $slider->image);
        Slider::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }
}
