<div class="modal fade" id="messageModalrep{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn btn-sm" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="messageModalLabel">
پاسخ به پیام                </h4>
            </div>
            <div class="modal-body">
                <label for="">مربوط به پیام :</label>
                <textarea type="text" rows="3"  class="form-control " disabled > {{@$row->message}} </textarea>
                <form method="post" action="{{URL::action('Site\HomeController@postComment')}}">
                    {{ csrf_field() }}
                        <input type="hidden" name="parent_id" value="{{$row->id}}">
                        <input type="hidden" name="commentable_id" value="{{$row->commentable_id}}">
                        <input type="hidden" name="commentable_type" value="{{$row->commentable_type}}">
                        <input type="hidden" name="status" value="1">
                        <input type="hidden" name="readat" value="1">
                        <input type="hidden" name="name" value="ادمین">
                    <div class="row w-100 m-0">
                        <div class="col-md-12 col-sm-12 col-xs-12 p-1 align-self-center">
                            <label>دسته بندی:</label>
                            @php $com = \App\Models\Comment::where('id', $row->parent_id)->first(); @endphp
                            <p> @if($row->commentable_type == 'App\Models\Product')
                                    محصول/ {{@$row->product->title}}
                                @elseif($row->commentable_type == 'App\Models\Article') مقاله/ {{@$row->article->title}}
                                @elseif($row->commentable_type == 'App\Models\News') خبر/ {{@$row->news->title}}

                                @endif </p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 p-1 align-self-center">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>نام:</label>
                                        <input type="text" class="form-control"  name="name" value="مدیر سایت">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ایمیل:</label>
                                        <input type="text" class="form-control"  name="email" value="">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control"  name="read_at" value="1">


                            <div class="form-group">
                                <label>پاسخ:</label>
                                <textarea class="form-control" type="text" rows="5" id="message" name="message"></textarea>
                            </div>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 px-1 pt-3 align-self-center">
                            <button type="submit" class="btn btn-success form-control"> ذخیره </button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>
