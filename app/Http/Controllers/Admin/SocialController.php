<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SocialRequest;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Category;
use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SocialController extends Controller
{
    public function getSocial(Request $request ){

        $query= Social::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.Helper::persian2LatinDigit($request->get('title')).'%');
        }

        $socials=$query->paginate(50);
        return view('admin.social.index')
            ->with('socials',$socials);
    }
    public function getAddSocial(){
        $socials=Social::orderby('id','DESC')->get();
        return View('admin.social.add')
            ->with('socials',$socials);
    }

    public function postAddSocial(SocialRequest $request){
        $input = $request->all();
        $input['status'] = $request->has('status');


//        dd($input);
        $social=Social::create($input);
        return Redirect::action('Admin\SocialController@getSocial')->with('success','آیتم  با موفقیت افزوده شد');
    }
    public function getEditSocial($id){
        $data = Social::find($id);

        return view('admin.social.edit')
            -> with('data',$data);
    }

    public function postEditSocial($id,SocialRequest $request){

        $input = $request->all();
        $social=Social::find($id);
        $input['status'] = $request->has('status');

        $social->update($input);
        return Redirect::action('Admin\SocialController@getSocial')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }

    public function getDeleteSocial($id){
        $social=Social::find($id);
        Social::destroy($id);
        return Redirect::back()->with('success','دسته بندی مورد نظر با موفقیت حذف شد');
    }
}
