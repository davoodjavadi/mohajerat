@extends('layouts.site.master')

@section('title'){{@$blog->title_seo ? $blog->title_seo : $blog->title}}@stop
@section('image_seo'){{ @$blog->image[0]->file ? asset('assets/uploads/content/article/small/'.$blog->image[0]->file) : asset('assets/uploads/content/'.@$setting_main->logo)}}@endsection

@section('description')@if($blog->des_seo != null){!! $blog->description_seo !!}@else{!! strip_tags(\Illuminate\Support\Str::words($blog->short_description,100)) !!}@endif @stop

@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder"> {{@$blog->title}} </h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>
                        خانه</a></li>
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getBlog')}}" class="text-one">
                        لیست بلاگ</a></li>
                <li class="breadcrumb-item active" aria-current="page"> {{@$blog->title}}</li>
            </ol>
        </div>

    </nav>
    <div class="content detail-blog py-3">
        <div class="container">
            <div class="row w-100 m-0">
                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-xs-12 p-1">
                    <div class="description text-justify ">
                        <h2 class="text-one fw-bolder">{{@$blog->title}}</h2>
                        <img src="{{asset('assets/uploads/article/big/'.@$blog->image)}}" class="w-100 m-0 my-3">
                        <p class="text-justify">
                            {{@$blog->short_description}}

                        </p>
                        <p class="text-justify">
                            {!! @$blog->description !!}
                        </p>
                    </div>
                    <div class="">
                        <div class="date me-3 d-flex justify-content-between align-items-center">
                            <span class="d-flex align-items-center text-two me-3">
                                <i class="bi bi-calendar-event d-flex me-2 text-two"></i>
                                {{jdate('Y/m/d',@$blog->created_at->timestamp)}}
                            </span>
                        </div>
{{--                        <div class="tag-relate my-2">--}}
{{--                            <span class="d-flex align-items-center text-two">--}}
{{--                                <i class="bi bi-tag d-flex text-two me-1"></i>--}}
{{--                                برچسب ها :--}}
{{--                            </span>--}}
{{--                            <div class="tag-list p-0">--}}
{{--                                <a href="">--}}
{{--                                    <div class="tag-box">--}}
{{--                                        <p class=" fw-bolder m-0">--}}
{{--                                            زندگی--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                                <a href="">--}}
{{--                                    <div class="tag-box">--}}
{{--                                        <p class=" fw-bolder m-0">--}}
{{--                                            زندگی--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                                <a href="">--}}
{{--                                    <div class="tag-box">--}}
{{--                                        <p class=" fw-bolder m-0">--}}
{{--                                            زندگی--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                                <a href="">--}}
{{--                                    <div class="tag-box">--}}
{{--                                        <p class=" fw-bolder m-0">--}}
{{--                                            زندگی--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                                <a href="">--}}
{{--                                    <div class="tag-box">--}}
{{--                                        <p class=" fw-bolder m-0">--}}
{{--                                            زندگی--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                                <a href="">--}}
{{--                                    <div class="tag-box">--}}
{{--                                        <p class=" fw-bolder m-0">--}}
{{--                                            زندگی--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="comments-box my-3">
                        <h3 class=" fw-bolder text-one">
                            نظرات و دیدگاه ها
                        </h3>
                        <div class="row w-100 m-0">
                            <div class="col-12 p-0 mb-3">
                                @include('site.blog.content.form-comment')
                            </div>
                            @if($comments->count() > 0)
                                <div class="col-12 p-0">
                                    <h4 class=" fw-bolder text-one">
                                        نظرات کاربران
                                    </h4>
                                    @include('site.blog.content.comment')
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-xs-12 p-1">
                    @include('site.blog.content.sidebar')
                    @include('site.blog.content.sidebar-xs')

                </div>
            </div>
        </div>
    </div>
@stop
