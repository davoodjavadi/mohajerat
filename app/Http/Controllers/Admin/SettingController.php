<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;
use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SettingController extends Controller
{
    public function getSetting(){

        $data = Setting::first();

//        dd($data);
        return view('admin.setting.edit')
            ->with('data', $data);
    }
    public function postSetting($id,Request $request){

        $input = $request->all();
//        dd($input);
//        if(strlen($input['aboutfooter_title']) > 30){
//            return Redirect::back()->with('error',' تایتل درباره ما فوتر بیشتر از 30 کاراکتر است.');
//        }
//        if(strlen($input['aboutfooter_description']) > 600){
//            return Redirect::back()->with('error','توضیحات درباره ما فوتر بیشتر از 300 کاراکتر است.');
//        }
        $setting = Setting::find($id);
        if ($request->hasFile('logo')){
            File::delete('assets/uploads/content/'. $setting->logo);
            $pathMain="assets/uploads/content/";
            $extension=$request->file('logo')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('logo')->move($pathMain,$fileName);
                $input['logo']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['logo'] = $setting->logo;
        }

        if ($request->hasFile('favicon')){
            File::delete('assets/uploads/content/'. $setting->favicon);
            $pathMain="assets/uploads/content/";
            $extension=$request->file('favicon')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('favicon')->move($pathMain,$fileName);
                $input['favicon']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['favicon'] = $setting->favicon;
        }
        if ($request->hasFile('about_img')){
            File::delete('assets/uploads/content/'. $setting->about_img);
            $pathMain="assets/uploads/content/";
            $extension=$request->file('about_img')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('about_img')->move($pathMain,$fileName);
                $input['about_img']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['about_img'] = $setting->about_img;
        }
  //      dd($input);
        $setting->update($input);

        return Redirect::back();
    }
}
