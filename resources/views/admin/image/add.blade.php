@extends('layouts.admin.master')
@section('content')
<div class="card border">
    <div class="card-header">
        <h5>افزودن تصویر بیشتر</h5>
    </div>
    <form class="form theme-form" action="{{URL::action('Admin\ImageController@postAddImage')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="parent_id" value="{{$parent_id}}">
@include('admin.image.form')
    </form>
</div>
@endsection
