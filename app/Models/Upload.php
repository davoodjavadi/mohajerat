<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Upload extends Model
{
    protected $table='uploads';
    use SoftDeletes;
    protected $fillable=['title','image','link'];
}
