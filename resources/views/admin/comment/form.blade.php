{{ csrf_field() }}
<div class="box-body">

    <div class="form-group">
        <div class="row">
            <div class="col-md-4">
                <label>ایمیل:</label>
                <input class="form-control" type="text" id="name" name="email"
                       value="@if(isset($data->email)) {{$data->email}} @endif"
                       placeholder="عنوان  را وارد کنید . . ." disabled>
            </div>




            <div class="col-md-4">
                <label>وضعیت:</label>
                <select name="status" id="status" class="form-control">
                    @foreach($status as $key=>$item)
                        <option value="{{$key}}" @if(isset($data->status) and $data->status==$key) selected @endif>{{$item}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>



    <div class="form-group">
        <label>توضیحات:</label>
		<textarea class="form-control ckeditor" id="message" name="message"
                  >@if(isset($data->message)) {!!$data->message!!} @endif</textarea>
    </div>



    <div class="box-footer">
        <button type="submit" class="btn btn-primary">ذخیره</button>
    </div>
