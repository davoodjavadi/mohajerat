<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryRequest;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Content;
use App\Models\Image;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ImageController extends Controller
{
    public function getImages($id,Request $request ){
        $service=Service::find($id);
        $images=Image::orderBy('id','DESC')->where('parent_id',$id)->paginate(50);
        return view('admin.image.index')
            ->with('images',$images)
            ->with('service',$service);
    }
    public function getAddImage($parent_id){
        $service=Service::find($parent_id);
        $data=Image::orderby('id','DESC')->get();
        return View('admin.image.add')
            ->with('data',$data)
            ->with('parent_id',$parent_id)
            ->with('$service',$service);
    }
    public function postAddImage(GalleryRequest $request){

        $input = $request->all();

        if ($request->hasFile('image')) {
            $path = "assets/uploads/content/image/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }

//        dd($input);
        $image=Image::create($input);
        return Redirect::action('Admin\ImageController@getImages',$input['parent_id'])->with('success','آیتم با موفقیت افزوده شد');
    }

    public function getDeleteImage($id){
        $image=Image::find($id);
//        $redirect = Redirects::create([
//            "old_address" => @$news->url,
//            "new_address" => '',
//
//        ]);

        Image::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }
}
