<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\ContactRequest;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Menu;
use App\Models\News;
use App\Models\Service;
use App\Models\Content;
use App\Models\Slider;
use App\Models\Sloagan;
use App\Models\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function getIndex(){
        $lang = Lang::where('default', 1)->first();

        $sliders = Slider::where('lang_id', $lang->id)->orderby('id', 'DESC')->get();
        $news = News::where('lang_id', $lang->id)->orderby('id', 'DESC')->whereStatus('1')->take(8)->get();
        $sloagans = Sloagan::where('lang_id', $lang->id)->orderby('id', 'DESC')->take(4)->get();
        $menu = Menu::where('lang_id', $lang->id)->orderby('id', 'DESC')->first();
        $services = Service::where('lang_id', $lang->id)->orderby('id', 'DESC')->take(5)->get();

        return View('site.index')
            ->with('lang', $lang)
            ->with('menu', $menu)
            ->with('sliders',$sliders)
            ->with('services',$services)
            ->with('news',$news)
            ->with('sloagans',$sloagans);
    }



    public function getIndexLtr(Request $request)
    {
        $seg= request()->segments();
        $lang = Lang::where('url',$seg[0])->first();
        $id = @$lang->id;

        if (@$lang->default == 1) {
            return \redirect('/');
        } else {
            $sliders = Slider::where('lang_id', @$id)->orderby('id', 'DESC')->get();
            $services = Service::where('lang_id', @$id)->orderby('id', 'DESC')->take(5)->get();
            $news = News::where('lang_id', @$id)->orderby('id', 'DESC')->whereStatus('1')->take(8)->get();
            $sloagans = Sloagan::where('lang_id', @$id)->orderby('id', 'DESC')->take(4)->get();
            $menu = Menu::where('lang_id', @$id)->orderby('id', 'DESC')->first();

            return View('site.index-en')
                ->with('lang', $lang)
                ->with('menu', $menu)
                ->with('sliders', $sliders)
                ->with('services', $services)
                ->with('news', $news)
                ->with('sloagans', $sloagans);
        }

    }

    public function getCategories(){

        $categories = Category::orderby('id', 'DESC')->Active()->whereNotNull('parent_id')->get();

        return View('site.services.list')
            ->with('categories',$categories);
    }


    public function getNews()
    {
        $news = News::orderby('id','DESC')->get();
        return View('site.news.list')
            ->with('news',$news);
    }
    public function getNewsDetail(Request $request){
        $seg= request()->segments();
        $news=News::where('url',$seg[1])->first();
        $comments=Comment::orderby('id','DESC')->where('status','1')->where('commentable_type','App\Models\Content')->where('commentable_id',@$news->id)->whereNull('parent_id')->get();
        $related=News::orderby('id','DESC')->where('id','<>',@$news->id)->take(4)->get();
        return view('site.news.detail')
            ->with('news',$news)
            ->with('comments',$comments)
            ->with('related',$related);
    }

    public function getBlog()
    {
        $blogs = Article::orderby('id','DESC')->get();
        return View('site.blog.list')
            ->with('blogs',$blogs);
    }
    public function getBlogDetail(Request $request){
        $seg= request()->segments();
        $blog=Article::where('url',$seg[1])->first();
        $comments=Comment::orderby('id','DESC')->where('status','1')->where('commentable_type','App\Models\Content')->where('commentable_id',$blog->id)->whereNull('parent_id')->get();
        $related=Article::orderby('id','DESC')->where('id','<>',$blog->id)->take(4)->get();
        return view('site.blog.detail')
            ->with('blog',$blog)
            ->with('related',$related)
            ->with('comments',$comments);
    }
    public function postComment(CommentRequest $request)
    {
        $input = $request->all();
        $comments = Comment::create($input);
        return Redirect::back()->with('success', 'نظر شما با موفقیت ارسال شد.');
    }

    public function getAbout()
    {
        $lang = Lang::where('default', 1)->first();
        return view('site.about')->with('lang',$lang);
    }
    public function getContact()
    {
        $lang = Lang::where('default', 1)->first();
        return view('site.contact')->with('lang',$lang);
    }
    public function postContact(ContactRequest $request)
    {
        $input = $request->all();
        $contact = Contact::create($input);
        return Redirect::back()->with('success', 'پیام شما با موفقیت ارسال شد.');
    }
    public function getCategory(){

        $category=Category::orderby('id')->where('status','1')->whereNull('parent_id')->get();
        return view('site.services.cat-list')
            ->with('category',$category);
    }
    public function getSubCategory(Request $request){
        $seg= request()->segments();
        $category=Category::Active()->where('url',$seg[1])->first();
        return view('site.services.cat-detail')
            ->with('category',$category);
    }
    public function getCategoryDetail(Request $request){
        $seg= request()->segments();
        $category=Category::Active()->where('url',$seg[1])->first();
        $services=Service::orderby('id','DESC')->where('category_id',$category->id)->Active()->get();
        return view('site.services.list')
            ->with('category',$category)
            ->with('services',$services);
    }


    public function getServiceDetail(Request $request){
        $seg= request()->segments();
        $service=Service::Active()->where('url',$seg[1])->first();
        $related=Service::orderby('id', 'DESC')->where([['id','<>',$service->id],['lang_id', $service->lang_id]])->Active()->get()->random(4);
        return view('site.services.detail')
            ->with('service',$service)
            ->with('related',$related);
    }




//    public function getEn(){
//        $sliders = Content::orderby('id', 'DESC')->Slider()->get();
//        $sloagans = Sloagan::orderby('id', 'DESC')->Active()->Show()->take(4)->get();
//        return View('site.index-en')
//            ->with('sliders',$sliders)
//            ->with('sloagans',$sloagans);
//    }
//    public function getUa(){
//        $sliders = Content::orderby('id', 'DESC')->Slider()->get();
//        $sloagans = Sloagan::orderby('id', 'DESC')->Active()->Show()->take(4)->get();
//        return View('site.index-ua')
//            ->with('sliders',$sliders)
//            ->with('sloagans',$sloagans);
//    }
}
