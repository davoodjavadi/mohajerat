<menu class="p-0">
    <?php
    $lang = App\Models\Lang::where('default', 1)->first();
    $menu = App\Models\Menu::where('lang_id', $lang->id)->orderby('id', 'DESC')->first();
    ?>
    <div class="menu-back">
        <div class="container">
            <div class="menu">
                <nav class="navbar navbar-expand-lg navbar-light p-0 d-xs-none d-sm-none d-md-block">
                    <div class="container-fluid p-0">
                        <button class="navbar-toggler p-0 border-0" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <i class="bi bi-list d-flex text-white fs-1 p-0"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0 en-menu">
                                <li class="nav-item">
                                    <a class="nav-link text-white" aria-current="page" href="tel:{{@$lang->phone}}">
                                        {{ @$lang->call_us }} : {{@$lang->phone}}
                                    </a>
                                </li>

                            </ul>

                            <ul class="row ms-auto mb-2 mb-lg-0 right en-nav p-0 m-0" style="direction:ltr;">
                                @foreach($socials_main as $socialll)
                                    <li class="nav-item d-flex align-items-center justify-content-center col-md col-sm-3 col-xs-3 p-0">
                                        <a class="nav-link active" aria-current="page" href="{{@$socialll->link}}">
                                            <i class="fa {{@$socialll->icon}} d-flex fs-5 text-light"></i>
                                        </a>
                                    </li>
                                @endforeach

                                @foreach($langs as $lang)
                                    <li class="nav-item d-flex align-items-center justify-content-center col-md col-sm-3 col-xs-3 p-0">
                                        <a class="nav-link active" aria-current="page" href="{{url('/'.$lang->url)}}">
                                            <img src="{{asset('assets/uploads/lang/small/'.$lang->image)}}">
                                        </a>
                                    </li>
                                    @endforeach
{{--                                    <li class="nav-item d-flex align-items-center justify-content-center col-md col-sm-3 col-xs-3 p-0">--}}
{{--                                        <a class="nav-link active" aria-current="page" href="{{url('/english')}}">--}}
{{--                                            <img src="{{asset('assets/site/images/united-kingdom.png')}}">--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
                            </ul>
                        </div>
                    </div>
                </nav>
                @include('layouts.site.blocks.content.menu-xs')
            </div>
        </div>
    </div>
    <div class="logo mt-5 d-xs-none d-sm-none d-md-block">
        <div class="container">
            <div class="d-flex justify-content-end">
                <a href="{{URL::action('Site\HomeController@getIndex')}}">
                    <img src="{{asset('assets/uploads/content/'.@$setting_main->logo)}}" width="100" height="100">
                </a>
            </div>
        </div>
    </div>
</menu>
