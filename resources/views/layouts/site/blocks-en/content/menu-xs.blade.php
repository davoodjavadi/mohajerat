<div class="d-xs-block d-xs-block d-md-none position-relative">
    <div class="">
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn p-2" onclick="closeNav()"><i
                    class="bi bi-x d-flex"></i></a>

{{--            <a href="tel:">--}}
{{--                <i class="bi bi-person mx-2 d-flex"></i>--}}
{{--                Agent login--}}
{{--            </a>--}}
{{--            <a href="tel:">--}}
{{--                <i class="bi bi-person mx-2 d-flex"></i>--}}
{{--                costumer login--}}
{{--            </a>--}}

            <a href="{{URL::action('Site\HomeController@getCategory')}}">
                <i class="bi bi-send mx-2 d-flex"></i>
                {{ @$menu->service }}</a>
            <a href="{{URL::action('Site\HomeController@getBlog')}}">
                <i class="bi bi-file-earmark-text mx-2 d-flex"></i>
                {{ @$menu->blog }}</a>
            <a href="{{URL::action('Site\HomeController@getNews')}}">
                <i class="bi bi-file-earmark-text mx-2 d-flex"></i>
                {{ @$menu->news }}</a>
            <a href="{{URL::action('Site\HomeController@getAbout')}}">
                <i class="bi bi-info-square mx-2 d-flex"></i>
                {{ @$menu->about_us }}</a>

            <a href="{{URL::action('Site\HomeController@getContact')}}">
                <i class="bi bi-headset mx-2 d-flex"></i>
                {{ @$menu->contact_us }}</a>

            @if(@$lang->phone != NULL)
                <a href="tel:{{@$lang->phone}}">
                    <i class="bi bi-telephone mx-2 d-flex"></i>
                    {{@$lang->phone}}
                </a>
            @endif
            @if(@$lang->mobile != NULL)
                <a href="tel:{{@$lang->mobile}}">
                    <i class="bi bi-telephone mx-2 d-flex"></i>
                    {{@$lang->mobile}}
                </a>
            @endif
        </div>
        <span style="font-size:30px;cursor:pointer" onclick="openNav()"> <i
                class="bi bi-list d-flex text-two fs-1 p-2"></i></span>
    </div>
    <div class="">
        <div class="logo position-absolute top-0 end-0">
            <div class="container">
                <div class="d-flex justify-content-end">
                    <a href="">
                        <img src="{{asset('assets/site/images/logo.png')}}">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
