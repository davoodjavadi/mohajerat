<footer class="mt-5">
    <div class="container">
        <div class="footer-top mt-5 d-flex justify-content-center">
            @foreach(@$socials_main as $soc)
                <div class="footer-box mx-1">
                    <a href="{{@$soc->link}}">
                        <i class="fa {{@$soc->icon}} d-flex text-light"></i>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="footer-bottom mt-4 py-4">
        <div class="container">
            <div class="footer-in ">
                <div class="row w-100 m-0">
                    <div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 p-0 mb-3">
                        <div class="sr">
                            <p class="h5 text-one fw-bolder">{{@$menu->title_in_footer}}</p>
                            <ul class="p-0 ps-3 m-0">
                                <li class="py-2">
                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getCategory')}}">
                                        {{@$menu->service}}
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getNews')}}">
                                        {{@$menu->news}}
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getBlog')}}">
                                        {{@$menu->blog}}
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getContact')}}">
                                        {{@$menu->contact_us}}
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getAbout')}}">
                                        {{@$menu->about_us}}
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 p-0 mb-3">
                        <div class="sr">
                            <p class="h5 text-one fw-bolder">{{ @$menu->title_contact_footer }}</p>
                            <ul class="p-0 ps-3 m-0">
                                @if(@$lang->phone != NULL)
                                    <li class="py-2">
                                        <a href="tel:{{@$lang->phone}}">
                                            {{ @$menu->title_phone_footer }} : {{@$lang->phone}}
                                        </a>
                                    </li>
                                @endif

                                @if(@$lang->email != NULL)
                                    <li class="py-2">
                                        <a href="mailto:{{@$lang->email}}">
                                            {{ @$menu->title_email_footer }} : {{@$lang->email}}
                                        </a>
                                    </li>
                                @endif

                                @if(@$lang->address != NULL)
                                    <li class="py-2">
                                        <a >
                                            {{ @$menu->title_address_footer }} : {{@$lang->address}}
                                        </a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 p-0 mb-3">
                        <div class="sr">
                            <p class="h5 text-one fw-bolder">{{@$lang->service_title}}</p>
                            <ul class="p-0 ps-3 m-0">
                                @foreach(@$services as $service)
                                    <li class="py-2">
                                        {{--                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getSubCategory',@$service->url)}}">--}}
                                        <a href="{{url('/service/'.@$service->url)}}">
                                            {{@$service->title}}
                                        </a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 p-0 mb-3">
                        <div class="sr">
                            <p class="h5 text-one fw-bolder">{{ Illuminate\Support\Str::limit(@$lang->about_title, 50, $end='...')}}</p>
                            <ul class="p-0  m-0">
                                <li class="py-2 list-unstyled text-justify">
                                    {{ Illuminate\Support\Str::limit(@$lang->about_description, 300, $end='...')}}
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="left w-100 d-flex justify-content-center">
                    <span>Designed by <a href="https://www.rahweb.com/" target="_blank" >Rahweb</a></span>
                </div>
            </div>
        </div>

    </div>

</footer>
