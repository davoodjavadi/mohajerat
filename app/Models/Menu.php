<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'lang_id',
        'home',
        'home_link',
        'service',
        'service_link',
        'news',
        'news_link',
        'blog',
        'blog_link',
        'about_us',
        'about_us_link',
        'contact_us',
        'contact_us_link',
        'title_in_footer',
        'title_contact_footer',
        'title_phone_footer',
        'title_email_footer',
        'title_address_footer',
    ];



    public function lang()
    {
        return $this->belongsTo('App\Models\Lang','lang_id');
    }
}
