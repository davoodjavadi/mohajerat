<?php

//Admin Routes

Route::get('/login','Admin\LoginController@getLogin');
Route::post('/login','Admin\LoginController@postLogin');
Route::get('/logout','Admin\LoginController@getlogout');

Route::prefix('admin')->middleware('AdminPermission')->group(function(){

    Route::get('/','Admin\IndexController@getIndex');
//    category
    Route::get('/category','Admin\CategoryController@getCategory');
    Route::get('/category/add','Admin\CategoryController@getAddCategory');
    Route::post('/category/add','Admin\CategoryController@postAddCategory');
    Route::get('/category/edit/{id}','Admin\CategoryController@getEditCategory');
    Route::post('/category/edit/{id}','Admin\CategoryController@postEditCategory');
    Route::get('/category/delete/{id}','Admin\CategoryController@getDeleteCategory');

//services
    Route::get('/service','Admin\ServiceController@getService');
    Route::get('/service/lang/{id}','Admin\ServiceController@getServiceLang');
    Route::get('/service/add','Admin\ServiceController@getAddService');
    Route::post('/service/add','Admin\ServiceController@postAddService');
    Route::get('/service/edit/{id}','Admin\ServiceController@getEditService');
    Route::post('/service/edit/{id}','Admin\ServiceController@postEditService');
    Route::get('/service/delete/{id}','Admin\ServiceController@getDeleteService');

//    images
    Route::get('images/{id}','Admin\ImageController@getImages');
    Route::get('image-s/add/{id}','Admin\ImageController@getAddImage');
    Route::post('images/add','Admin\ImageController@postAddImage');
    Route::get('images/delete/{id}','Admin\ImageController@getDeleteImage');

//article
    Route::get('/article','Admin\ArticleController@getArticle');
    Route::get('/article/add','Admin\ArticleController@getAddArticle');
    Route::post('/article/add','Admin\ArticleController@postAddArticle');
    Route::get('/article/edit/{id}','Admin\ArticleController@getEditArticle');
    Route::post('/article/edit/{id}','Admin\ArticleController@postEditArticle');
    Route::get('/article/delete/{id}','Admin\ArticleController@getDeleteArticle');
//news
    Route::get('/news','Admin\NewsController@getNews');
    Route::get('/news/add','Admin\NewsController@getAddNews');
    Route::post('/news/add','Admin\NewsController@postAddNews');
    Route::get('/news/edit/{id}','Admin\NewsController@getEditNews');
    Route::post('/news/edit/{id}','Admin\NewsController@postEditNews');
    Route::get('/news/delete/{id}','Admin\NewsController@getDeleteNews');
//slider
    Route::get('/slider','Admin\SliderController@getSlider');
    Route::get('/slider/lang/{id}','Admin\SliderController@getSliderLang');
    Route::get('/slider/add','Admin\SliderController@getAddSlider');
    Route::post('/slider/add','Admin\SliderController@postAddSlider');
    Route::get('/slider/edit/{id}','Admin\SliderController@getEditSlider');
    Route::post('/slider/edit/{id}','Admin\SliderController@postEditSlider');
    Route::get('/slider/delete/{id}','Admin\SliderController@getDeleteSlider');
//sloagan
    Route::get('/sloagan','Admin\SloaganController@getSloagan');
    Route::get('/sloagan/lang/{id}','Admin\SloaganController@getSloaganLang');
    Route::get('/sloagan/add','Admin\SloaganController@getAddSloagan');
    Route::post('/sloagan/add','Admin\SloaganController@postAddSloagan');
    Route::get('/sloagan/edit/{id}','Admin\SloaganController@getEditSloagan');
    Route::post('/sloagan/edit/{id}','Admin\SloaganController@postEditSloagan');
    Route::get('/sloagan/delete/{id}','Admin\SloaganController@getDeleteSloagan');
//setting
    Route::get('/setting','Admin\SettingController@getSetting');
    Route::post('/setting/{id}','Admin\SettingController@postSetting');
//social
    Route::get('/social','Admin\SocialController@getSocial');
    Route::get('/social/add','Admin\SocialController@getAddSocial');
    Route::post('/social/add','Admin\SocialController@postAddSocial');
    Route::get('/social/edit/{id}','Admin\SocialController@getEditSocial');
    Route::post('/social/edit/{id}','Admin\SocialController@postEditSocial');
    Route::get('/social/delete/{id}','Admin\SocialController@getDeleteSocial');

//uploader
    Route::get('/uploader','Admin\UploaderController@getUploader');
    Route::get('/uploader/add','Admin\UploaderController@getAddUploader');
    Route::post('/uploader/add','Admin\UploaderController@postAddUploader');
    Route::get('/uploader/delete/{id}', 'Admin\UploaderController@getDeleteUploader');
//    redirect
    Route::get('/redirect','Admin\RedirectController@getRedirect');
    Route::get('/redirect/add','Admin\RedirectController@getRedirectAdd');
    Route::post('/redirect/add','Admin\RedirectController@postRedirectAdd');
    Route::get('/redirect/delete/{id}', 'Admin\RedirectController@getRedirectDelete');
    Route::post('redirect/delete', 'Admin\RedirectController@postDeleteRedirect');

    //contact
    Route::get('contact', 'Admin\MessageController@getContact');
    Route::post('contact/edit/{id}', 'Admin\MessageController@postContactEdit');
    Route::get('contact/delete/{id}', 'Admin\MessageController@getContactDelete');
    Route::post('contact/delete', 'Admin\MessageController@postContactDelete');

//    comment
    Route::get('comment', 'Admin\MessageController@getComment');
    Route::get('comment/edit/{id}', 'Admin\MessageController@getCommentEdit');
    Route::post('comment/edit/{id}', 'Admin\MessageController@postCommentEdit');
    Route::get('comment/delete/{id}', 'Admin\MessageController@getCommentDelete');

    //lang
    Route::get('/langs','Admin\LangController@getLang');
    Route::get('/lang/add','Admin\LangController@getAddLang');
    Route::post('/lang/add','Admin\LangController@postAddLang');
    Route::get('/lang/edit/{id}','Admin\LangController@getEditLang');
    Route::post('/lang/edit/{id}','Admin\LangController@postEditLang');
    Route::get('/lang/delete/{id}','Admin\LangController@getDeleteLang');
    Route::post('/setDefaultLang','Admin\LangController@setDefaultLang');

    //menu
    Route::get('/menu','Admin\MenuController@getMenu');
    Route::get('/menu/add','Admin\MenuController@getAddMenu');
    Route::post('/menu/add','Admin\MenuController@postAddMenu');
    Route::get('/menu/edit/{id}','Admin\MenuController@getEditMenu');
    Route::post('/menu/edit/{id}','Admin\MenuController@postEditMenu');
    Route::get('/menu/delete/{id}','Admin\MenuController@getDeleteMenu');
});
