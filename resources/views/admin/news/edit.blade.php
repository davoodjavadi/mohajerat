@extends('layouts.admin.master')
@section('content')
    <div class="card border">
        <div class="card-header">
            <h5>ویرایش خبر</h5>
        </div>
        <form class="form theme-form" action="{{URL::action('Admin\NewsController@postEditNews',@$data->id)}}" method="POST" enctype="multipart/form-data">
            @include('admin.news.form')
        </form>
    </div>
@endsection
