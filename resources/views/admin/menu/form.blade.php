{{csrf_field()}}
<div class="card-body">
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label value=""> انتخاب زبان: </label><span class="text-danger"> * </span>
                <select id="optlist" class="form-control" name="lang_id" value="@if(isset($data->lang_id)){{$data->lang_id}}@else {{ old('lang_id') }}@endif">
                    <option value="">انتخاب کنید </option>
                    @foreach($langs as $row)
                        <option value="{{$row['id']}}"
                                @if(isset($data->lang_id))
                                @if($data->lang_id==$row['id']) selected @endif
                            @endif
                        >{{$row['title']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن منو "صفحه اصلی" :</label>
                <input type="text"  class="form-control input-air-primary" name="home" value="@if(isset($data->home)){{$data->home}}@else {{ old('home') }}@endif">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="title_seo">لینک منو "صفحه اصلی" :</label>
                <input type="text"  class="form-control input-air-primary" name="home_link" value="@if(isset($data->home_link)){{$data->home_link}}@else {{ old('home_link') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن منو "خدمات" :</label>
                <input type="text"  class="form-control input-air-primary" name="service" value="@if(isset($data->service)){{$data->service}}@else {{ old('service') }}@endif">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="title_seo">لینک منو "خدمات" :</label>
                <input type="text"  class="form-control input-air-primary" name="service_link" value="@if(isset($data->service_link)){{$data->service_link}}@else {{ old('service_link') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن منو "اخبار" :</label>
                <input type="text"  class="form-control input-air-primary" name="news" value="@if(isset($data->news)){{$data->news}}@else {{ old('news') }}@endif">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="title_seo">لینک منو "اخبار" :</label>
                <input type="text"  class="form-control input-air-primary" name="news_link" value="@if(isset($data->news_link)){{$data->news_link}}@else {{ old('news_link') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن منو "مقالات" :</label>
                <input type="text"  class="form-control input-air-primary" name="blog" value="@if(isset($data->blog)){{$data->blog}}@else {{ old('blog') }}@endif">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="title_seo">لینک منو "مقالات" :</label>
                <input type="text"  class="form-control input-air-primary" name="blog_link" value="@if(isset($data->blog_link)){{$data->blog_link}}@else {{ old('blog_link') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن منو "درباره ما" :</label>
                <input type="text"  class="form-control input-air-primary" name="about_us" value="@if(isset($data->about_us)){{$data->about_us}}@else {{ old('about_us') }}@endif">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="title_seo">لینک منو "درباره ما" :</label>
                <input type="text"  class="form-control input-air-primary" name="about_us_link" value="@if(isset($data->about_us_link)){{$data->about_us_link}}@else {{ old('about_us_link') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">متن منو "ارتباط با ما" :</label>
                <input type="text"  class="form-control input-air-primary" name="contact_us" value="@if(isset($data->contact_us)){{$data->contact_us}}@else {{ old('contact_us') }}@endif">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="title_seo">لینک منو "ارتباط با ما" :</label>
                <input type="text"  class="form-control input-air-primary" name="contact_us_link" value="@if(isset($data->contact_us_link)){{$data->contact_us_link}}@else {{ old('contact_us_link') }}@endif">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان "ایمیل" در فوتر :</label>
                <input type="text"  class="form-control input-air-primary" name="title_email_footer" value="@if(isset($data->title_email_footer)){{$data->title_email_footer}}@else {{ old('title_email_footer') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان "آدرس" در فوتر :</label>
                <input type="text"  class="form-control input-air-primary" name="title_address_footer" value="@if(isset($data->title_address_footer)){{$data->title_address_footer}}@else {{ old('title_address_footer') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان "تلفن" در فوتر :</label>
                <input type="text"  class="form-control input-air-primary" name="title_phone_footer" value="@if(isset($data->title_phone_footer)){{$data->title_phone_footer}}@else {{ old('title_phone_footer') }}@endif">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان "منو" در فوتر :</label>
                <input type="text"  class="form-control input-air-primary" name="title_in_footer" value="@if(isset($data->title_in_footer)){{$data->title_in_footer}}@else {{ old('title_in_footer') }}@endif">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="title_seo">عنوان "تماس با ما" در فوتر :</label>
                <input type="text"  class="form-control input-air-primary" name="title_contact_footer" value="@if(isset($data->title_contact_footer)){{$data->title_contact_footer}}@else {{ old('title_contact_footer') }}@endif">
            </div>
        </div>
    </div>



</div>
<input type="hidden" name="type" value="2">
<div class="card-footer">
    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
    <a class="btn btn-light btn-pill" type="reset"  href="{{URL::action('Admin\MenuController@getMenu')}}">بازگشت</a>
</div>
