<form  action="{{URL::action('Site\HomeController@postComment')}}" method="POST">
    {{csrf_field()}}
    <div class="form-comment mt-3 p-4  rounded-3">
        <div class="row w-100 m-0">
            <div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 p-0 pe-3 p-xs-0 mb-3">
                <div class="">
                    <label for="exampleFormControlInput1"
                           class="form-label text-one">نام و نام خانوادگی</label>
                    <input name="name" type="text" class="form-control"
                           id="exampleFormControlInput1"
                           placeholder="برای مثال : رضا عباسی">
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 p-0 ps-3 p-xs-0 mb-3">
                <div class="">
                    <label for="exampleFormControlInput1"
                           class="form-label text-one">ایمیل</label>
                    <input name="email" type="email" class="form-control"
                           id="exampleFormControlInput1" placeholder="name@example.com">
                </div>
            </div>
            <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0 p-0 mb-3">
                <div class="">
                    <label for="exampleFormControlTextarea1"
                           class="form-label text-one">نظر خود را بنویسید
                    </label>
                    <textarea name="message" class="form-control" id="exampleFormControlTextarea1"
                              rows="4"></textarea>
                </div>
            </div>
            <input type="hidden" name="commentable_type" value="App\Models\Content">
            <input type="hidden" name="commentable_id" value="{{$blog->id}}">
            <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0 p-0 mb-3">
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-comment">
                        ارسال
                    </button>
                </div>

            </div>
        </div>
    </div>
</form>
