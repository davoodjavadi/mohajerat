<div class="menu-inner bg-two position-absolute end-0 start-0 d-xs-none d-sm-none d-md-block">
    <div class="container">
        <ul class="d-inline-flex  m-0 p-0 px-1">
            <li class="list-unstyled ">
                <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getIndex')}}" class="nav-link ">
                    خانه
                </a>
            </li>
            <li class="list-unstyled ">
                <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getCategory')}}" class="nav-link ">
                    خدمات
                </a>
            </li>
            <li class="list-unstyled ">
                <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getNews')}}" class="nav-link ">
                    اخبار
                </a>
            </li>
            <li class="list-unstyled ">
                <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getBlog')}}" class="nav-link ">
                    مقالات
                </a>
            </li>
            <li class="list-unstyled ">
                <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getAbout')}}" class="nav-link ">
                    درباره ما
                </a>
            </li>
            <li class="list-unstyled ">
                <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getContact')}}" class="nav-link ">
                    تماس با ما
                </a>
            </li>

        </ul>
    </div>
</div>
