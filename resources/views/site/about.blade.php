@extends('layouts.site.master')
@section('title')
    {{'درباره ما'}}
@stop
@section('image_seo'){{ asset('assets/uploads/content/'.@$setting_main->logo)}}@stop

@section('description')
@stop
@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder"> {{ @$lang->about_title }} </h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>
                        خانه</a></li>
                <li class="breadcrumb-item active" aria-current="page"> درباره ما</li>
            </ol>
        </div>

    </nav>
    <div class="content about py-3">
        <div class="costumer mt-3">
            <div class="container">
                <div class="about des text-justify">
                    <img src="{{asset('assets/uploads/content/'.$setting_main->about_img)}}">
                    <h3 class="fw-bolder text-one">{{ @$lang->about_title }}</h3>
                    <p>

                        {!! @$lang->about_description !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop
