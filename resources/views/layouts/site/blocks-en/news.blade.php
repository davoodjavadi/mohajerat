<div class="news">
    <div class="container">
        <div class="news-in">
            <div class="row w-100 m-0">
         @foreach($news as $row)
                <div class="col-xl-3 col-md-4 col-sm-6 col-xs-6 p-0">
                    <div class="news-inner" style="background-image: url({{asset('assets/uploads/content/news/medium/'.$row->image)}});">
                        <div class="over1">
                            <div class="">
                                <h6 class="fw-bolder">{!! @$row->title !!}</h6>
                                <p class="text-white text">
                               {!! @$row->short_description !!}
                                    </p>
                            </div>
                        </div>
                        <div class="over2">
                            <a href="{{url('/news/'.@$row->url)}}">
                                <h6 class="fw-bolder pb-4 text-white">{!! @$row->title !!}</h6>
                            </a>
                            <p class="text-center">
                                {!! Illuminate\Support\Str::limit(@$row->short_description,250, $end='...')!!}
                            </p>
                            <a class="btn btn-more py-1 px-3" href="{{url('/news/'.@$row->url)}}">بیشتر </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
