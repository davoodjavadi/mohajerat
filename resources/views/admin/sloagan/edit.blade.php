@extends('layouts.admin.master')
@section('content')
    <div class="card border">
        <div class="card-header">
            <h5>ویرایش شعار</h5>
        </div>
        <form class="form theme-form" action="{{URL::action('Admin\SloaganController@postEditSloagan',$data->id)}}" method="POST" enctype="multipart/form-data">
            @include('admin.sloagan.form')
        </form>
    </div>
@endsection
