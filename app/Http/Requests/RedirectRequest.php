<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RedirectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_address' => 'required ',

            'new_address' => 'required ',
        ];
    }
    public function messages()
    {
        return [
            'old_address.required' => '  آدرس قدیمی اجباری است',
            'new_address.required' => ' آدرس جدید اجباری است',



        ];
    }
}
