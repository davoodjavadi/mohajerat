<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    protected $table='comments';
    use SoftDeletes;
    protected $fillable=['name','email','commentable_id','commentable_type','status','reply_id','parent_id','title','message','read_at','star'];
    public function Commentable(){
        return $this->morphTo();
    }
    public function replies(){
        return $this->hasMany('App\Models\Comment','parent_id')->where('status','1');
    }

    public function content(){
        return $this->belongsTo('App\Models\Content','commentable_id','id');
    }
    public function service(){
        return $this->belongsTo('App\Models\Service','commentable_id','id');
    }

}
