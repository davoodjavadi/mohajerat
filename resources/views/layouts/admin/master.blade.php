<!DOCTYPE html>
<html lang="en" dir="rtl">
@include('layouts.admin.blocks.head')
{{--too head @yield('style') gozashtam--}}

<body main-theme-layout="rtl">
@include('layouts.message-swal')

<div class="page-wrapper">

    @include('layouts.admin.blocks.header')

    <div class="page-body-wrapper">

        @include('layouts.admin.blocks.sidebar')

        <div class="page-body">
            @yield('content')
            @include('layouts.admin.blocks.modal')
            @yield('modal')
        </div>


    </div>
</div>
@include('layouts.admin.blocks.script')
@yield('js')

</body>
</html>
