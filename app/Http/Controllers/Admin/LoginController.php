<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DriveLoginRequest;
use App\Model\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('admin.login.index');
    }
    public function postLogin(Request $request)
    {

        $input = $request->all();
        $login = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'admin' => true,
            'status' => true,
        ]);
//        dd($input);
        if ($login) {

            return Redirect::action('Admin\IndexController@getIndex')->with('success','خوش آمدید');
        } else {
            return Redirect::action('Admin\LoginController@getLogin')->with('error', 'اطلاعات ورودی اشتباه است');


        }
    }
    public function getlogout()
    {

        Auth::logout();

        return Redirect::action('Admin\LoginController@getLogin')->with('success', 'به امید دیدار مجدد');    }
}
