@extends('layouts.site.master')

@section('title'){{@$service->title_seo ? $service->title_seo : $service->title}} @stop
@section('image_seo'){{ @$service->image[0]->file ? asset('assets/uploads/content/service/small/'.$service->image[0]->file) : asset('assets/uploads/content/'.@$setting_main->logo)}}
@endsection

@section('description')
    @if(@$service->description_seo != null)
        {!! @$service->description_seo !!}
    @else
        {!! strip_tags(\Illuminate\Support\Str::words(@$service->description,100)) !!}
    @endif
@stop

@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder"> {{@$service->title}} </h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>
                        خانه</a></li>
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getCategory')}}" class="text-one">
                        خدمات</a></li>
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getSubCategory',@$service->category->parent->url)}}" class="text-one">
                        {{@$service->category->parent->title}}</a></li>
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getCategoryDetail',@$service->category->url)}}" class="text-one">
                    {{@$service->category->title}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{@$service->title}}</li>
            </ol>
        </div>

    </nav>
    <div class="content detail py-3">
        <div class="container">
            <div class="row w-100 m-0">
                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-xs-12 p-1">
                    <div class="description text-justify ">
                        <h2 class="text-one fw-bolder">{{@$service->title}}</h2>
                        <img src="{{asset('assets/uploads/service/big/'.@$service->image)}}" class="w-100 m-0 my-3">
                        <p class="text-justify">
                            {{@$service->short_description}}
                        </p>

                        <p class="text-justify">
                            {!! @$service->description !!}
                        </p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-xs-12 p-1">
                    <div class="sticky">
                        @include('site.services.content.sidebar')
                        @include('site.services.content.sidebar-xs')
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop
