<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table='categories';
    use SoftDeletes;
    protected $fillable=['parent_id','title','image','icon','description','url','status','title_seo','des_seo', 'lang_id'];
    public function parent(){
        return $this->belongsTo(Category::class,'parent_id','id');
    }
    public function childs(){
        return $this->hasMany(Category::class,'parent_id','id');
    }
    public function services(){
        return $this->hasMany('App/Models/Service');
    }
    public function scopeActive($query)
    {
        $data = $query->where('status','1');
        return $data;
    }

    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');
    }
}
