<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

                return[

                    'title'=>'required|min:3|max:255',
                    'email'=>'required',
                    'email2'=>'required',
                    'contact_number'=>'required',
                    'contact_number2'=>'required',
                    'whatsapp_number'=>'required',
                    'about_title'=>'required',
                    'about_description'=>'required',
                    'aboutfooter_title'=>'required',
                    'aboutfooter_description'=>'required',
                    'index_title'=>'required',
                    'index_description'=>'required',
                    'index_titlefa'=>'required',
                    'index_descriptionfa'=>'required',
                    'index_titleen'=>'required',
                    'index_descriptionen'=>'required',
                    'aboutindex_title'=>'required',
                    'aboutindex_description'=>'required',
                    'aboutindex_titlefa'=>'required',
                    'aboutindex_descriptionfa'=>'required',
                    'aboutindex_titleen'=>'required',
                    'aboutindex_descriptionen'=>'required',
                ];


    }
    public function messages()
    {
        return [
            'title.required' => '  نام اجباری است',
            'title.min'=>'نام کمتر از 3 حرف است',
            'title.max'=>'نام بیشتر از 255 حرف است',
            'email.required' => ' ایمیل 1 اجباری است',
            'email2.required' => ' ایمیل 2 اجباری است',
            'contact_number.required' => ' شماره تماس 1 اجباری است',
            'contact_number2.required' => ' شماره تماس 2 اجباری است',
//            'contact_address.required' => ' آدرس اجباری است',
            'whatsapp_number.required' => ' شماره واتساپ اجباری است',
            'about_title.required' => ' تایتل درباره ما صفحه اول واتساپ اجباری است',
            'about_description.required' => ' توضیحات درباره ما صفحه اول واتساپ اجباری است',
            'aboutfooter_title.required' => ' تایتل درباره ما  فوتر واتساپ اجباری است',
            'aboutfooter_description.required' => ' توضیحات درباره ما  فوتر واتساپ اجباری است',
            'index_title.required'=>' تایتل خدمات اکراینی اجباری است',
            'index_description.required'=>' توضیحات خدمات اکراینی اجباری است',
            'index_titlefa.required'=>'تایتل خدمات فارسی اجباری است',
            'index_descriptionfa.required'=>'توضیحات خدمات اکراینی اجباری است',
            'index_titleen.required'=>'تایتل خدمات انگلیسی اجباری است',
            'index_descriptionen.required'=>'توضیحات خدمات انگلیسی اجباری است',
            'aboutindex_title.required'=>'تایتل شعار های اکراینی اجباری است',
            'aboutindex_description.required'=>'توضیحات شعار های اکراینی اجباری است',
            'aboutindex_titlefa.required'=>'تایتل شعار های فارسی اجباری است',
            'aboutindex_descriptionfa.required'=>'توضیحات شعار های فارسی اجباری است',
            'aboutindex_titleen.required'=>'تایتل شعار های انگلیسی اجباری است',
            'aboutindex_descriptionen.required'=>'توضیحات شعار های انگلیسی اجباری است',

        ];
    }

}
