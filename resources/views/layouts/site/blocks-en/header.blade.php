<div class="header">
    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach($sliders as $key=>$slider)
                <div class="carousel-item @if($key == 0) active @endif">
                    <img src="{{asset('assets/uploads/slider/'.$slider->image)}}" class="d-block w-100"
                         alt="{!! $slider->title !!}">
                    <div class="carousel-caption ">
{{--                        @if($key %2==1)--}}
                        <h1 class="fw-bolder">{{ $slider->title }}</h1>
                        <p>
                            {!! $slider->description !!}

                        </p>
{{--                            @endif--}}
{{--                            @if($key %2==0)--}}
{{--                                <h1 class="fw-bolder">IT'S ALL ABOUT FREEDOM</h1>--}}
{{--                                <p>--}}
{{--                                    Freedom of mind to focus on what’s important.--}}
{{--                                    <br>--}}
{{--                                    for you and your family--}}

{{--                                </p>--}}
{{--                            @endif--}}
                    </div>
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    @include('layouts.site.blocks-en.content.menu-first')
</div>
