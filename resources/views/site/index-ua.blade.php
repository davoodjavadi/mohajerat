@extends('layouts.site.master-ua')
@section('content')
    @include('layouts.site.blocks-ua.header')
    <div class="consultations">
        <div class="container">
            @include('layouts.site.blocks-ua.consultations')
        </div>
    </div>
    @include('layouts.site.blocks-ua.banner')
    @include('layouts.site.blocks-ua.costumer')

@stop
