
@extends('layouts.admin.master')
@section('style')
        <style>
    ul.tabs {
    margin: 0;
    padding: 0;
    float: right;
    list-style: none;
    height: 32px;
    width: 100%;
    font-size:15px ;
    }
    ul.tabs li {
    float: right;
    margin: 0;
    cursor: pointer;
    padding: 0px 21px ;
    height: 31px;
    line-height: 31px;
    font-weight: bold;
    background: #CCCCCC;
    overflow: hidden;
    position: relative;
    }
    ul.tabs li:hover {
    background: #CCCC;
    opacity: 0.7;

    }
    ul.tabs li:first-child{
    border-radius:0 5px 0 0;
    -moz-border-radius:0 5px 0 0;
    -ms-border-radius:0 5px 0 0;
    -webkit-border-radius:0 5px 0 0;
    -o-border-radius:0 5px 0 0;
    -khtml-border-radius:0 5px 0 0;
    border-right:1px solid #999;
    }
    ul.tabs li:last-child{
    border-radius:5px 0 0 0;
    -moz-border-radius:5px 0 0 0;
    -ms-border-radius:5px 0 0 0;
    -webkit-border-radius:5px 0 0 0;
    -o-border-radius:5px 0 0 0;
    -khtml-border-radius:5px 0 0 0;
    }
    ul.tabs li.active{
    background: whitesmoke;

    }
    .alltabs {
    clear: both;
    float: right;
    width: 100%;
    background: transparent;

    }
    .tab {
    padding: 10px;
    font-size:15px ;
    text-align:right;
    display: none;
    }
    .shadow:hover {
    opacity: 0.7;

    }
        </style>
@endsection
@section('content')
<div class="col-sm-12  xl-100">
    <form class="form theme-form" action="{{URL::action('Admin\SettingController@postSetting',$data->id)}}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
        <div id="container">

        <ul class="nav tabs nav-tabs nav-primary" >

            <li class="active" rel="tab01">تنظیمات</li>
{{--            <li rel="tab02">تماس با ما</li>--}}
{{--            <li rel="tab03">درباره ما</li>--}}
{{--            <li rel="tab04"> خدمات صفحه اول</li>--}}
{{--            <li rel="tab05">شعار ها صفحه اول</li>--}}



        </ul>
        <div class="alltabs">

            <div id="tab01" class="tab p-md-5 p-xs-1">

                <div class="form-group">
                    <label for="exampleFormControlInput15"><span class="text-danger">*</span>نام سایت</label>
                    <input name="title" class="form-control input-air-primary"  type="text" value="@if(isset($data->title)){{$data->title}}@else {{ old('title') }}@endif">
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"> بارگذاری لوگو </label>
                            <div class="col-sm-10">
                                <input name="logo" class="form-control input-air-primary" type="file">
                                @if(isset($data->logo))<img src="{{asset('assets/uploads/content/'.$data->logo)}}" style="height: 100px; width: 100px;" alt="">@endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"> بارگذاری آیکن </label>
                            <div class="col-sm-10">
                                <input name="favicon" class="form-control input-air-primary" type="file">
                                @if(isset($data->favicon))<img src="{{asset('assets/uploads/content/'.$data->favicon)}}" style="height: 100px; width: 100px;" alt="">@endif
                            </div>
                        </div>
                    </div>
                </div>


                <br>
                <div class="form-group">
                    <label>متن سئو:</label>
                    <textarea rows="3" class="form-control input-air-primary" name="des_seo" >@if(isset($data->des_seo)){{$data->des_seo}}@else {{ old('des_seo') }}@endif</textarea>
                </div>
{{--                <div class="form-group">--}}
{{--                    <label>گوگل مپ</label>--}}
{{--                    <textarea rows="3" class="form-control input-air-primary" name="map" >@if(isset($data->map)){{$data->map}}@else {{ old('map') }}@endif</textarea>--}}
{{--                </div>--}}
                <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label"> بارگذاری تصویر درباره ما : </label>
                        <div class="col-sm-10">
                            <input name="about_img" class="form-control input-air-primary" type="file">
                            @if(isset($data->about_img))<img src="{{asset('assets/uploads/content/'.$data->about_img)}}" style="height: 100px; width: 100px;" alt="">@endif
                        </div>
                    </div>
                </div>



                <div class="">
                    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>
                </div>            </div>

{{--            <div id="tab02" class="tab p-md-5 p-xs-1">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-6">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="">شماره تماس 1<span class="text-danger">*</span></label>--}}
{{--                            <input type="text" class="form-control input-air-primary" name="contact_number" value="@if(isset($data->contact_number)){{$data->contact_number}}@else {{ old('contact_number') }}@endif">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="">شماره تماس 2 <span class="text-danger">*</span></label>--}}
{{--                            <input type="text" class="form-control input-air-primary" name="contact_number2" value="@if(isset($data->contact_number2)){{$data->contact_number2}}@else {{ old('contact_number2') }}@endif">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="">شماره واتساپ  <span class="text-danger">*</span></label>--}}
{{--                            <input type="text" class="form-control input-air-primary" name="whatsapp_number" value="@if(isset($data->whatsapp_number)){{$data->whatsapp_number}}@else {{ old('whatsapp_number') }}@endif">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="">ایمیل 1 <span class="text-danger">*</span></label>--}}
{{--                            <input type="text" class="form-control input-air-primary" name="email" value="@if(isset($data->email)){{$data->email}}@else {{ old('email') }}@endif">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="">ایمیل 2<span class="text-danger">*</span></label>--}}
{{--                            <input type="text" class="form-control input-air-primary" name="email2" value="@if(isset($data->email2)){{$data->email2}}@else {{ old('email2') }}@endif">--}}
{{--                        </div>--}}
{{--                    </div>--}}



{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label>آدرس اکراینی: </label>--}}
{{--                    <textarea rows="3" class="form-control input-air-primary " name="contact_address" >@if(isset($data->contact_address)){{$data->contact_address}}@else {{ old('contact_address') }}@endif</textarea>--}}
{{--                    </textarea>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>آدرس فارسی: </label>--}}
{{--                    <textarea rows="3" class="form-control input-air-primary " name="contact_addressfa" >@if(isset($data->contact_addressfa)){{$data->contact_addressfa}}@else {{ old('contact_addressfa') }}@endif</textarea>--}}
{{--                    </textarea>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>آدرس انگلیسی: </label>--}}
{{--                    <textarea rows="3" class="form-control input-air-primary " name="contact_addressen" >@if(isset($data->contact_addressen)){{$data->contact_addressen}}@else {{ old('contact_addressen') }}@endif</textarea>--}}
{{--                    </textarea>--}}
{{--                </div>--}}
{{--                <div class="">--}}
{{--                    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div id="tab03" class="tab p-md-5 p-xs-1">--}}

{{--                <div class="form-group">--}}
{{--                    <label>تایتل  درباره ما:<span class="text-danger">*</span></label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="about_title" value="@if(isset($data->about_title)){{$data->about_title}}@else {{ old('about_title') }} @endif">--}}
{{--                </div>--}}
{{--                <br>--}}

{{--                <div class="form-group">--}}
{{--                    <label>درباره ما صفحه اول:</label>--}}
{{--                    <textarea rows="3" class="form-control " name="about_description2" >@if(isset($data->about_description2)){{$data->about_description2}}@endif</textarea>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                <div class="form-group">--}}
{{--                    <label>درباره ما<span class="text-danger">*</span>:</label>--}}
{{--                    <textarea rows="3" class="form-control ckeditor" name="about_description" >@if(isset($data->about_description)){{$data->about_description}}@else {{ old('about_description') }} @endif</textarea>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                <div class="col-md-12">--}}
{{--                    <div class="form-group row">--}}
{{--                        <label class="col-sm-2 col-form-label"> بارگذاری تصویر </label>--}}
{{--                        <div class="col-sm-10">--}}
{{--                            <input name="about_img" class="form-control input-air-primary" type="file">--}}
{{--                            @if(isset($data->about_img))<img src="{{asset('assets/uploads/content/'.$data->about_img)}}" style="height: 100px; width: 100px;" alt="">@endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                <div class="form-group">--}}
{{--                    <label>تایتل  درباره ما فوتر<span class="text-danger">*</span>:</label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="aboutfooter_title" value="@if(isset($data->aboutfooter_title)){{$data->aboutfooter_title}}@else {{ old('aboutfooter_title') }} @endif">--}}

{{--                </div>--}}
{{--                 <div class="form-group">--}}
{{--                    <br>--}}
{{--                    <label>درباره ما فوتر<span class="text-danger">*</span>:</label>--}}
{{--                    <textarea rows="3" class="form-control " name="aboutfooter_description" >@if(isset($data->aboutfooter_description)){{$data->aboutfooter_description}}@else {{ old('aboutfooter_description') }} @endif</textarea>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <br>--}}
{{--                    <label>گوگل مپ (تهران)<span class="text-danger">*</span>:</label>--}}
{{--                    <textarea rows="3" class="form-control" name="map_tehran" >@if(isset($data->map_tehran)){{$data->map_tehran}}@else {{ old('map_tehran') }} @endif</textarea>--}}
{{--                </div>--}}
{{--                <div class="">--}}
{{--                    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>--}}
{{--                </div>--}}


{{--            </div>--}}
{{--            <div id="tab04" class="tab p-md-5 p-xs-1">--}}
{{--                <div class="form-group">--}}
{{--                    <label for=""><span class="text-danger">*</span>تایتل خدمات  :</label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="index_title" value="@if(isset($data->index_title)){{$data->index_title}}@else {{ old('index_title') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>توضیحات خدمات :<span class="text-danger">*</span></label>--}}
{{--                    <textarea rows="3" class="form-control  " name="index_description" >@if(isset($data->index_description)){{$data->index_description}}@else {{ old('index_description') }} @endif</textarea>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label for="">تایتل خدمات فارسی :<span class="text-danger">*</span></label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="index_titlefa" value="@if(isset($data->index_titlefa)){{$data->index_titlefa}}@else {{ old('index_titlefa') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>توضیحات خدمات فارسی:<span class="text-danger">*</span></label>--}}
{{--                    <textarea rows="3" class="form-control  " name="index_descriptionfa" >@if(isset($data->index_descriptionfa)){{$data->index_descriptionfa}}@else {{ old('index_descriptionfa') }} @endif</textarea>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label for="">تایتل خدمات انگلیسی :<span class="text-danger">*</span></label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="index_titleen" value="@if(isset($data->index_titleen)){{$data->index_titleen}}@else {{ old('index_titleen') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label><span class="text-danger">*</span>توضیحات خدمات انگلیسی:</label>--}}
{{--                    <textarea rows="3" class="form-control  " name="index_descriptionen" >@if(isset($data->index_descriptionen)){{$data->index_descriptionen}}@else {{ old('index_descriptionen') }} @endif</textarea>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label for=""><span class="text-danger">*</span>تایتل خدمات اکراینی :</label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="ua_title" value="@if(isset($data->ua_title)){{$data->ua_title}}@else {{ old('ua_title') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>توضیحات خدمات اکراینی:<span class="text-danger">*</span></label>--}}
{{--                    <textarea rows="3" class="form-control  " name="ua_description" >@if(isset($data->ua_description)){{$data->ua_description}}@else {{ old('ua_description') }} @endif</textarea>--}}
{{--                </div>--}}
{{--                <hr>--}}


{{--                <div class="">--}}
{{--                    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div id="tab05" class="tab p-md-5 p-xs-1">--}}
{{--                <div class="form-group">--}}
{{--                    <label for="">تایتل شعارها اکراینی :<span class="text-danger">*</span></label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="aboutindex_title" value="@if(isset($data->aboutindex_title)){{$data->aboutindex_title}}@else {{ old('aboutindex_title') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>توضیحات شعارها اکراینی:<span class="text-danger">*</span></label>--}}
{{--                    <textarea rows="3" class="form-control  " name="aboutindex_description" >@if(isset($data->aboutindex_description)){{$data->aboutindex_description}}@else {{ old('aboutindex_description') }} @endif</textarea>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label for="">تایتل شعارها فارسی :<span class="text-danger">*</span></label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="aboutindex_titlefa" value="@if(isset($data->aboutindex_titlefa)){{$data->aboutindex_titlefa}}@else {{ old('aboutindex_titlefa') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>توضیحات شعارها فارسی:<span class="text-danger">*</span></label>--}}
{{--                    <textarea rows="3" class="form-control  " name="aboutindex_descriptionfa" >@if(isset($data->aboutindex_descriptionfa)){{$data->aboutindex_descriptionfa}}@else {{ old('aboutindex_descriptionfa') }} @endif</textarea>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label for=""><span class="text-danger">*</span>تایتل شعارها انگلیسی :</label>--}}
{{--                    <input type="text" class="form-control input-air-primary" name="aboutindex_titleen" value="@if(isset($data->aboutindex_titleen)){{$data->aboutindex_titleen}}@else {{ old('aboutindex_titleen') }} @endif">--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label>توضیحات شعارها انگلیسی:<span class="text-danger">*</span></label>--}}
{{--                    <textarea rows="3" class="form-control  " name="aboutindex_descriptionen" >@if(isset($data->aboutindex_descriptionen)){{$data->aboutindex_descriptionen}}@else {{ old('aboutindex_descriptionen') }} @endif</textarea>--}}
{{--                </div>--}}

{{--                <hr>--}}


{{--                <div class="">--}}
{{--                    <button class="btn btn-primary btn-pill" type="submit"> ارسال </button>--}}
{{--                </div>--}}
{{--            </div>--}}

        </div>




    </div>
    </form>
</div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {

            $(".tab").slideUp();
            $(".tab:first").slideDown();

            $("ul.tabs li").click(function() {
                $("ul.tabs li").removeClass("active");
                $(this).addClass("active");
                $(".tab").slideUp();
                var activeTab = $(this).attr("rel");
                $("#"+activeTab).slideToggle();
            });

        });

    </script>
@endsection
