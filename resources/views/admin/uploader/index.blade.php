@extends('layouts.admin.master')


@section('content')
    <div id="page-wrapper">
        <div class="container-fluid ">

            <div class="col-lg-12">
                <div class="page-header">
                    <div class="row d-flex justify-content-between">
                        <div class="col-lg-6 main-header">
                            <h2 class="text-dark">آپلود تصاویر</h2>

                        </div>
                        <div class="col-lg-6">
                            <a href="{{URL::action('Admin\UploaderController@getAddUploader')}}" class="btn btn-pill btn-primary btn-air-primary"> افزودن </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <form method="post" action="">
                        {{ csrf_field() }}
{{--                        <div class="col-12 d-flex justify-content-end">--}}
{{--                            <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"--}}
{{--                                    data-toggle="tooltip" data-original-title="Delete selected items"--}}
{{--                                    class="btn mb-md-3 btn-space btn-secondary">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"--}}
{{--                                     class="bi bi-trash me-2" viewBox="0 0 16 16">--}}
{{--                                    <path--}}
{{--                                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />--}}
{{--                                    <path fill-rule="evenodd"--}}
{{--                                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />--}}
{{--                                </svg>--}}
{{--                                حذف انتخاب شده ها--}}
{{--                            </button>--}}
{{--                        </div>--}}
                        <table class="table ">
                            <thead class="bg-primary text-center text-light">
                            <tr>
{{--                                <th>--}}
{{--                                    <input id="check-all" style="opacity: 1;position:static;" type="checkbox" class="me-1" >--}}
{{--                                    انتخاب همه </th>--}}
                                <th>شماره</th>
                                <th>نام</th>
                                <th>تصویر</th>
                                <th>لینک</th>
                                <th>عملیات</th>


                            </tr>
                            </thead>
                            <tbody>

                            @foreach($uploaders as $key=>$uploader)

                                <tr>
{{--                                    <td class="">--}}
{{--                                        <input style="opacity: 1;position:static;"--}}
{{--                                               name="deleteId[]" class="delete-all"--}}
{{--                                               type="checkbox"--}}
{{--                                               value="{{$uploader->id}}" />--}}
{{--                                    </td>--}}
                                    <td class="text-center">{{$key+1}}</td>
                                    <td class="text-center">{{$uploader->title}}</td>

                                    <td class="text-center">
                                        @if(file_exists('assets/uploads/content/uploader/small/'.$uploader->image))
                                            <img src="{{asset('assets/uploads/content/uploader/small/'.$uploader->image)}}" style="height: 100px; width: 100px;" alt="">
                                        @else
                                            <img class="rounded-circle" width="50" src="{{asset('assets/site/images/not-found1.png')}}" alt="">
                                        @endif
                                    </td>

                                    <td class="text-center">{{$uploader->link}}</td>
{{--                                    <td class="text-center">{{asset('assets/uploads/content/uploader/small/'.$uploader->image)}}</td>--}}


                                    <td class="text-center">
                                        <a class="btn btn-sm btn-danger" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" href="{{URL::action('Admin\UploaderController@getDeleteUploader',$uploader->id)}}"><i class="fa fa-trash  fa-md"></i></a>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </form>
                    <div class="pagii">
                        @if(count($uploaders))
                            {!! $uploaders->appends(Request::except('page'))->render() !!}
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection


@section('js')

    <script>
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>
@stop
