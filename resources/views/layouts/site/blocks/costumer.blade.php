<div class="costumer mt-3">
    <div class="container">
        <div class="row w-100 m-0">
       @foreach($sloagans as $sloagan)
            <div class="col-xl-3 col-md-6 col-sm-6">
{{--                <a href="{{url('sloagan/'.$sloagan->id)}}">--}}
                    <div class="costumer-in position-relative">
                        <div class="box-costumer position-absolute">
                            <div class="img-box">
                                <img src="{{asset('assets/uploads/sloagan/'.@$sloagan->image)}}" class="w-100">
                            </div>
                        </div>
                        <h5 class="fw-bolder">{!! @$sloagan->title !!}</h5>
                        <p class="text-justify">
                            {!! Illuminate\Support\Str::limit(@$sloagan->description,250, $end='...')!!}
                        </p>
                    </div>
{{--                </a>--}}
            </div>
            @endforeach
        </div>
    </div>
</div>
