@extends('layouts.site.master')

@section('title')
    {{'تماس با ما'}}
@stop
@section('image_seo'){{ asset('assets/uploads/content/'.@$setting_main->logo)}}@stop

@section('description')
@stop

@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder"> تماس با ما </h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>
                        خانه</a></li>
                <li class="breadcrumb-item active" aria-current="page"> تماس با ما</li>
            </ol>
        </div>

    </nav>
    <div class="content contact py-3">
        <div class="costumer mt-3">
            <div class="container">
                <div class="row w-100 m-0">
                    <div class="col-md-6 col-sm-12 col-xs-12 p-2 d-grid">
                        <div class="bg-light p-3">
                            <h2 class="fw-bolder text-one">فرم تماس با ما</h2>
                            <div class="form-contact my-3">
                                <form action="{{URL::action('Site\HomeController@postContact')}}" method="POST">
                                    {{csrf_field()}}
                                    <input type="hidden" name="type" value="صفحه تماس با ما">
                                    <div class="row w-100 m-0">
                                        <div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 p-0 pe-3 p-xs-0">
                                            <div class="mb-3">
                                                <label for="exampleFormControlInput1" class="form-label text-one">نام و نام خانوادگی</label>
                                                <input name="name" type="text" class="form-control" id="exampleFormControlInput1" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-md-6 col-sm-6 col-xs-12 p-0 ps-3 p-xs-0">
                                            <div class="mb-3">
                                                <label for="exampleFormControlInput1" class="form-label text-one">ایمیل</label>
                                                <input name="email" type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0">
                                            <div class="mb-3">
                                                <label for="exampleFormControlInput1" class="form-label text-one">عنوان</label>
                                                <input name="subject" type="text" class="form-control" id="exampleFormControlInput1" placeholder="عنوان پیام">
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0">
                                            <div class="mb-3">
                                                <label for="exampleFormControlTextarea1" class="form-label text-one">متن پیام</label>
                                                <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end p-0">
                                            <button type="submit" class="btn btn-comment">ارسال</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 p-2 d-grid">
                        <div class="bg-light p-3">
                            <h2 class="fw-bolder text-one">اطلاعات تماس با ما</h2>
                            <div class="boxes-con mt-3">
                                <div class="info-contact">
                                    <div class="">
                                        <a href="tel:{{@$lang->phone}}" class="d-flex align-items-center my-3">
                                            <div class="box-in me-2">
                                                <i class="bi bi-telephone fs-5 d-flex text-white"></i>
                                            </div>
                                            <p class=" text-dark m-0">تلفن :{{@$lang->phone}}</p>
                                        </a>
                                    </div>
                                    <div class="">
                                        <a href="tel:{{@$lang->mobile}}" class="d-flex align-items-center my-3">
                                            <div class="box-in me-2">
                                                <i class="bi bi-telephone fs-5 d-flex text-white"></i>
                                            </div>
                                            <p class=" text-dark m-0">تلفن :{{@$lang->mobile}}</p>
                                        </a>
                                    </div>
                                    <div class="">
                                        <a  class="d-flex align-items-center my-3">
                                            <div class="box-in me-2">
                                                <i class="bi bi-geo-alt fs-5 d-flex text-white"></i>
                                            </div>
                                            <p class="text-dark   m-0">آدرس : {{@$lang->address}}</p>
                                        </a>
                                    </div>
                                    <div class="">

                                        <a href="mlito:{{@$lang->email}}" class="d-flex align-items-center my-3">
                                            <div class="box-in me-2">
                                                <i class="bi bi-envelope fs-5 d-flex text-white"></i>
                                            </div>
                                            <p class="text-dark  m-0">ایمیل :  {{@$lang->email}}</p>
                                        </a>
                                    </div>
{{--                                    <div class="">--}}
{{--                                        <a href="mlito:{{@$setting_main->email2}}" class="d-flex align-items-center my-3">--}}
{{--                                            <div class="box-in me-2">--}}
{{--                                                <i class="bi bi-envelope fs-5 d-flex text-white"></i>--}}
{{--                                            </div>--}}
{{--                                            <p class="text-dark  m-0">ایمیل :  {{@$setting_main->email2}}</p>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
                                </div>
                                @if($socials_main->count() > 0)
                                <p class="text-one h4 fw-bolder p-2">با ما در ارتباط باشید : </p>
                                <div class="info-contact d-flex">
                                    @foreach(@$socials_main as $row)
                                    <div class="">
                                        <a href="{{@$row->link}}" class="d-flex align-items-center ">
                                            <div class="box-in me-2">
                                                <i class="fa {{@$row->icon}} fs-5 d-flex text-white"></i>
                                            </div>

                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                    @endif
                            </div>
                        </div>

                    </div>
                </div>
                 @if(@$lang != NULL)
                <div class="content about py-3">
                    <div class="costumer mt-3">
                        <div class="col-12 p-2">
                            <iframe src="{{ @$lang->map }}" style="border:0;" allowfullscreen="" loading="lazy" width="100%" height="250"></iframe>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@stop
