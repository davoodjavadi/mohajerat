@extends('layouts.admin.master')
@section('content')
<div class="card border">
    <div class="card-header">
        <h5>افزودن خدمات</h5>
    </div>
    <form class="form theme-form" action="{{URL::action('Admin\ServiceController@postAddService')}}" method="POST" enctype="multipart/form-data">
@include('admin.service.form')
    </form>
</div>
@endsection
