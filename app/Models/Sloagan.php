<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sloagan extends Model
{
    protected $table='sloagans';
    use SoftDeletes;
    protected $fillable=['lang_id','title','image','description','status','view_sp'];


    public function scopeShow($query)
    {
        $data = $query->where('view_sp','1');
        return $data;
    }
    public function scopeActive($query)
    {
        $data = $query->where('status','1');
        return $data;
    }

    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');
    }
}

