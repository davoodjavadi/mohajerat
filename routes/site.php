<?php


use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Lang;

$seg= request()->segments();
//dd($seg);

Route::namespace('Site')->group(function () {
    Route::post('/post-comment', 'HomeController@postComment');
    Route::post('/post-contact', 'HomeController@postContact');

    //First Pages
    Route::get('/', 'HomeController@getIndex')->name('site.home');

    //landings
    $url = Lang::whereNotNull('url')->get();
    foreach ($url as $row) {
        if ($row->url) {
                Route::get('/' . @$row->url , 'HomeController@getIndexLtr')->name('site.landing');
        }
    }

    //news
    Route::get('/news', 'HomeController@getNews')->name('site.news');

    if($seg[0]='news'){
        Route::get('/news/{url}', 'HomeController@getNewsDetail')->name('site.news.detail');

    }
    //blog
    Route::get('/blog', 'HomeController@getBlog')->name('site.blog');

    if($seg[0]='blog'){
        Route::get('/blog/{url}', 'HomeController@getBlogDetail')->name('site.blog.detail');

    }
//    about-us
    Route::get('/about-us', 'HomeController@getAbout')->name('site.about');
//    contact-us
    Route::get('/contact-us', 'HomeController@getContact')->name('site.contact');
//services
    Route::get('/categories', 'HomeController@getCategory')->name('site.categories');
    Route::get('/category-detail/{url}', 'HomeController@getSubCategory');
    if($seg[0]='category'){
        Route::get('/category/{url}', 'HomeController@getCategoryDetail')->name('site.category.detail');

    }
    if($seg[0]='service'){
        Route::get('/service/{url}', 'HomeController@getServiceDetail')->name('site.service.detail');

    }
    Route::get('/english', 'HomeController@getEn');
    Route::get('/ukrain', 'HomeController@getUa');
    //sitemap

});
Route::get('sitemap.xml','SitemapController@sitemap');
