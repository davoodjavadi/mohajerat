<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Content;
use App\Models\Service;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function sitemap()
    {
        $categories = Category::orderBy('id','DESC')->where('status','1')->whereNull('parent_id')->get();
        $categories2 = Category::orderBy('id','DESC')->where('status','1')->whereNotNull('parent_id')->get();
        $services = Service::orderBy('id','DESC')->where('status','1')->get();
        $articles = Content::Article()->orderBy('id','DESC')->where('status','1')->get();
        $news = Content::News()->orderBy('id','DESC')->where('status','1')->get();

        return response()->view('sitemap', [

            'categories'=> $categories,
            'categories2'=> $categories2,
            'services'=> $services,
            'articles'=> $articles,
            'news'=> $news,


        ])->header('Content-Type','text/xml');
    }}
