<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Lang;
use App\Models\Sloagan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SloaganController extends Controller
{
    public function getSloagan(Request $request ){

        $query=Sloagan::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.$request->get('title').'%');
        }
        $sloagans=$query->paginate(50);
        return view('admin.sloagan.index')
            ->with('sloagans',$sloagans);
    }


    public function getSloaganLang($id){

        $sloagans=Sloagan::where('lang_id', $id)->paginate(50);
        return view('admin.sloagan.index')
            ->with('sloagans',$sloagans);
    }

    public function getAddSloagan(){
        $sloagans=Sloagan::orderby('id','DESC')->get();
        $langs = Lang::all();
        return View('admin.sloagan.add')
            ->with('sloagans',$sloagans)
            ->with('langs',$langs);
    }

    public function postAddSloagan(AdminRequest $request){
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['view_sp'] = $request->has('view_sp');


        if ($request->hasFile('image')){
            $pathMain="assets/uploads/sloagan/";
            $extension=$request->file('image')->getClientOriginalExtension();
            $fileName=md5(microtime()).".$extension";
            $request->file('image')->move($pathMain,$fileName);
            $input['image']=$fileName;
        }else{
            return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
        }

//        dd($input);
        $sloagan=Sloagan::create($input);
        return Redirect::action('Admin\SloaganController@getSloagan')->with('success','آیتم با موفقیت افزوده شد');
    }


    public function getEditSloagan($id){
        $data = Sloagan::find($id);
        $langs = Lang::all();

        return view('admin.sloagan.edit')
            -> with('data',$data)
            -> with('langs',$langs);
    }


    public function postEditSloagan($id,AdminRequest $request){

        $input = $request->all();
        $sloagan=Sloagan::find($id);
        $input['status'] = $request->has('status');
        $input['view_sp'] = $request->has('view_sp');

//        if ($request->get('url') != $newsnews->url){
//            $redirects=Redirects::create([
//                "old_address"=>$news->url,
//                "new_address"=>$input['url'],
//            ]);
//        }
        if ($request->hasFile('image')){
            File::delete('assets/uploads/sloagan/'. $sloagan->image);
            $pathMain="assets/uploads/sloagan/";
            $extension=$request->file('image')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('image')->move($pathMain,$fileName);
                $input['image']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['image'] = $sloagan->image;
        }

        $sloagan->update($input);
        return Redirect::action('Admin\SloaganController@getSloagan')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }

    public function getDeleteSloagan($id){
        $sloagan=Sloagan::find($id);
//        $redirect = Redirects::create([
//            "old_address" => @$sloagan->url,
//            "new_address" => '',
//
//        ]);
        File::delete('assets/uploads/sloagan/'. $sloagan->image);
        Sloagan::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }
}
