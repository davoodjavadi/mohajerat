@extends('layouts.admin.master')

@section('content')
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-6 main-header">
                <h2>پنل<span> ادمین </span></h2>

            </div>
            <div class="col-lg-6 breadcrumb-right">
                <ol class="breadcrumb">

                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12  xl-100 box-col-12">
            <div class="card">
                <div class="mobile-clock-widget" style="
    background: url({{asset('assets/admin/images/other-images/img-cropper.jpg')}}) center center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 220px;
    color: #fff;
    background-blend-mode: overlay;
    background-color: #41394c;">


                    <div class="clock-details">
                        <div class="date f-44 mb-2" id="date">
                            <span id="monthDay">{{jdate('d')}} {{jdate('F')}}</span><span id="year">,&nbsp;{{jdate('Y')}}</span></div>
                        <div>
                            <p class="m-0 f-18 text-light">تهران, ایران                                            </p>
                            <p class="m-0 f-18 text-light">{{jdate('H:i')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12 xl-100">
            <div class="row">
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\ServiceController@getService')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-building-o" style="font-size: 35px; color: #eeece9"></i>
                                                <h5 class="f-w-700 mb-0"> خدمات </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\CategoryController@getCategory')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-sitemap" style="font-size: 35px; color: #eeece9"></i>
                                                <h5 class="f-w-700 mb-0">دسته خدمات </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\ArticleController@getArticle')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-book " style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0"> مقالات </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\NewsController@getNews')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-newspaper-o" style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0"> اخبار </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\SliderController@getSlider')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-image" style="font-size: 35px;color:#fff;"></i>
                                                <h5 class="f-w-700 mb-0"> اسلایدر </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\SloaganController@getSloagan')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-comment " style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0"> شعار ها </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\MessageController@getContact')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-envelope " style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0"> پیام ها </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\MessageController@getComment')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-comment " style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0"> کامنت ها </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\SettingController@getSetting')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-gears " style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0">تنظیمات </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="col-xl-3 xl-50 col-md-6 box-col-6">
                    <div class="card gradient-primary o-hidden">
                        <a href="{{\Illuminate\Support\Facades\URL::action('Admin\SocialController@getSocial')}}">
                            <div class="card-body tag-card">
                                <div class="default-chart">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="widgets-bottom d-flex justify-content-between align-items-center">
                                                <i class="fa fa-instagram " style="font-size: 35px;color:#fff;"></i>

                                                <h5 class="f-w-700 mb-0">سوشال مدیا </h5>
                                            </div>
                                        </div>
                                    </div>

                                </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
                            </div>

                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- Container-fluid Ends-->
@endsection
