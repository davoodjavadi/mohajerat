<div class="life">
    <div class="container">
        <div class="life-in ">
            <h3 class="fw-bolder text-center h1 my-4">{{@$lang->image_middle_title}}</h3>
            <p class="text-center fw-bold">
                {{@$lang->image_middle_description}}
            </p>
            <div class="d-flex justify-content-center">
                <a href="{{URL::action('Site\HomeController@getContact')}}" type="button" class="btn btn-life py-1 px-4">{{@$lang->about_title}}</a>
            </div>
        </div>
    </div>
</div>
