@extends('layouts.admin.master')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row d-flex justify-content-between">
                <div class="col-lg-6 main-header">
                    <h2 class="text-dark">کشورها</h2>

                </div>
                <div class="col-lg-6">
                    <a href="" data-target="#exampleModalCenter" data-toggle="modal" class="btn btn-pill btn-outline-dark"><i class="fa fa-search"></i></a>
                    <a href="#" data-target="#exampleModalCenter1" data-toggle="modal" class="btn btn-pill btn-secondary btn-air-secondary"> انتخاب پیش فرض </a>
                    <a href="{{URL::action('Admin\LangController@getAddLang')}}" class="btn btn-pill btn-primary btn-air-primary"> افزودن </a>
                </div>
                </div>
        </div>
    </div>
<div class="container-fluid">
    <div class="card-block row">
        <div class="col-sm-12 col-lg-12 col-xl-12">
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-center text-light">
                    <tr>
                        <th>#</th>
                        <th>عکس</th>
                        <th>نام</th>
                        <th>دسترسی سریع</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($langs as $key=>$lang)
                    <tr>
                        <th class="text-center">
                            {{($langs->appends(Request::except('page'))->currentPage() - 1) *
                            ($langs->appends(Request::except('page'))->perPage()) + $key+1}}
                        </th>
                        <td class="text-center">
                            @if(file_exists('assets/uploads/lang/small/'.$lang->image))
                                <img class="rounded-circle" width="50" src="{{asset('assets/uploads/lang/small/'.$lang->image)}}" alt="">
                            @else
                                <img class="rounded-circle" width="50" src="{{asset('assets/site/images/not-found1.png')}}" alt="">
                            @endif
                        </td>
                        <td class="text-center">{{ Illuminate\Support\Str::limit(@$lang->title,20, $end='...')}}</td>
                        <td class="text-center">
                            <a data-target="tooltip" title="خدمات" class="btn btn-sm btn-info"  href="{{URL::action('Admin\ServiceController@getServiceLang',$lang->id)}}"><i class="fa fa-building-o fa-md"></i></a>
                            <a data-target="tooltip" title="اسلایدر" class="btn btn-sm btn-primary"  href="{{URL::action('Admin\SliderController@getSliderLang',$lang->id)}}"><i class="fa fa-image fa-md"></i></a>
                            <a data-target="tooltip" title="شعار" class="btn btn-sm btn-success"  href="{{URL::action('Admin\SloaganController@getSloaganLang',$lang->id)}}"><i class="fa fa-comment fa-md"></i></a>
                        </td>
                        <td class="text-center">
                            <a data-target="tooltip" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" title="حذف" class="btn btn-sm btn-danger" href="{{URL::action('Admin\LangController@getDeleteLang',$lang->id)}}"><i class="fa fa-trash fa-md"></i></a>
                            <a data-target="tooltip" title="ویرایش" class="btn btn-sm btn-warning"  href="{{URL::action('Admin\LangController@getEditLang',$lang->id)}}"><i class="fa fa-edit fa-md"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagii">
                @if(count($langs))
                    {!! $langs->appends(Request::except('page'))->render() !!}
                @endif
            </div>        </div>
    </div>
</div>
{{--    search modal--}}
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{URL::current()}}" method="GET" class="m-0">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLabel">جستجو</h5>

                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">نام :</label>
                            <input type="text" class="form-control" name="title" >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger text-light" data-dismiss="modal">بستن</button>
                        <button type="submit" class="btn btn-success text-light">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--  default modal--}}
    <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{URL::action('Admin\LangController@setDefaultLang')}}" method="POST" class="m-0">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLabel">انتخاب زبان پیش فرض</h5>

                    </div>
                    <div class="modal-body">
                        <select id="optlist" class="form-control" name="default">
                            <option value="">انتخاب کنید </option>
                            @foreach($langs as $row)
                                <option value="{{$row['id']}}"
                                        @if($row['default'] == 1) selected @endif
                                >{{$row['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger text-light" data-dismiss="modal">بستن</button>
                        <button type="submit" class="btn btn-success text-light">تایید</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
