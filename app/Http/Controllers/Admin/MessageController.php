<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Concerns\FormatsMessages;

class MessageController extends Controller
{
//-----------------------------contact-------------------------------------
    public function getContact(Request $request)
    {
        $query= Contact::orderBy('id','DESC');
        if($request->get('name')){
            $query->where('name','LIKE','%'.Helper::persian2LatinDigit($request->get('name')).'%');

        }
        if($request->get('email')){
            $query->where('email','LIKE','%'.Helper::persian2LatinDigit($request->get('email')).'%');

        }
        if($request->get('message')){
            $query->where('message','LIKE','%'.Helper::persian2LatinDigit($request->get('message')).'%');

        }

        $data = $query->paginate(15);
        return View('admin.contact.index')
            ->with('data', $data);


    }

    public function postContactEdit($id,Request $request){
        $input = $request->all();

        $contact = Contact::find($id);

        $data = $contact->update($input);

        return Redirect::back()->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');
    }

    public function getContactDelete($id)
    {
        $content = Contact::find($id);
        Contact::destroy($id);
        return Redirect::back()
            ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');

    }
    public function postContactDelete(Request $request)
    {
        if (Contact::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }
//-----------------------------comment-------------------------------------
    public function getComment(Request $request)
    {


        $data = Comment::orderby('id','DESC')->paginate(100);
        $status = [
            '1' => 'تایید شده',
            '0' => 'عدم تایید',
        ];
        return View('admin.comment.index')
            ->with('status', $status)
            ->with('data', $data);

    }

    public function getCommentEdit($id)
    {

        $data = Comment::find($id);
        $status = [
            '1' => 'تایید شده',
            '0' => 'عدم تایید',
        ];


        return View('admin.comment.edit')
            ->with('data', $data)
            ->with('status', $status);


    }
    public function postCommentEdit($id, Request $request)
    {

        $input = $request->all();
        $comments = Comment::find($id);
        $input["read_at"]=1;
        $comments->update($input);
        $array = array($input);
        $serialized_array = serialize($array);

        return Redirect::back()
            ->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');
    }
    public function postCommentDelete(Request $request)
    {
        if (Comment::destroy($request->get('deleteId'))) {
            return Redirect::action('Admin\CommentController@getIndex')
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }

    public function getCommentDelete($id)
    {
        $content = Comment::find($id);
            foreach ($content->replies as $row) {
                Comment::destroy($row['id']);
            }
        Comment::destroy($id);
        return Redirect::back()->with('success', 'کد مورد نظر با موفقیت حذف شد.');

    }




}
