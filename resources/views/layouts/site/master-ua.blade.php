<!doctype html>
<html lang="en" dir="ltr">

@include('layouts.site.blocks.head')

<body>
<div class="en">
    @include('layouts.message-swal')
    @include('layouts.site.blocks-ua.menu')
    @yield('content')



    @include('layouts.site.blocks-ua.footer')
    @include('layouts.site.blocks-ua.script')
</div>
</body>

</html>
