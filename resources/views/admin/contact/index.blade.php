@extends ("layouts.admin.master")
@section('title','کامنت ها')
@section('part','کامنت ها')
@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0 d-flex justify-content-center">
            <div class="page-header">
                <h3 class=" py-2 px-4 rounded-lg">
                    پیام ها
                    <a href="" data-target="#exampleModalCenter" data-toggle="modal" class="btn btn-outline-dark"><i
                            class="fa fa-search"></i></a>
                </h3>
            </div>
        </div>
        @include('layouts.admin.blocks.message')
        <div class="col-lg-12 mx-auto">
            <div class="box card  border-0 rounded p-3">

                <div
                    class="box-body rounded-lg overflow-hidden border table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl no-padding rounded overflow-hidden">

                    <form method="post" action="{{URL::action('Admin\MessageController@postContactDelete')}}">
                        {{ csrf_field() }}
                        <div class="col-12 d-flex justify-content-end">

                            <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
                                    data-toggle="tooltip" data-original-title="Delete selected items"
                                    class="btn mb-md-3 btn-space btn-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-trash me-2" viewBox="0 0 16 16">
                                    <path
                                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd"
                                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                                حذف انتخاب شده ها
                            </button>


                        </div>
                        <table class="table table-hover table-bordered rounded m-0">
                            <thead>
                            <tr>
                                <th>
                                    <input id="check-all" style="opacity: 1;position:static;" type="checkbox"
                                           class="me-1">
                                    انتخاب همه
                                </th>
                                <th class="text-center">شماره</th>
                                <th class="text-center">نوع</th>
                                <th class="text-center">ایمیل</th>
                                <th class="text-center">تاریخ</th>
                                <th class="text-center">خوانده شده</th>
                                <th class="text-center">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key=>$row)
                                <tr>
                                    <td class="">
                                        <input style="opacity: 1;position:static;"
                                               name="deleteId[]" class="delete-all"
                                               type="checkbox"
                                               value="{{$row->id}}"/>
                                    </td>
                                    <td class="sorting_1">{{($data->appends(Request::except('page'))->currentPage() - 1) *
($data->appends(Request::except('page'))->perPage()) + $key+1}}</td>

                                    <td>
                                        {{ Illuminate\Support\Str::limit(@$row->type,20, $end='...')}}

                                        {{--                                @if($row->type==1)--}}
                                        {{--                                    <span class='label '>ارتباط با ما</span>--}}
                                        {{--                                @elseif($row->type==3)--}}
                                        {{--                                    <span class='label '>تماس با ما</span>--}}
                                        {{--                                @else--}}
                                        {{--                                    <span class='label '>فوتر </span>--}}
                                        {{--                                @endif</td>--}}
                                    </td>

                                    <td class="text-center">
                                        {{ Illuminate\Support\Str::limit(@$row->email,20, $end='...')}}

                                    </td>
                                    <td>{{jdate('Y/m/d H:i',$row->created_at->timestamp)}}</td>
                                    <td class="text-center">
                                        @if($row->read_at==1)
                                            <span class='label label-success'><i class="fa fa-check "></i></span>
                                        @else
                                            <span class='label label-danger'><i class="fa  fa-times"></i> </span>
                                        @endif
                                    </td>
                                    <td>

                                        <a href="" data-toggle="modal" data-target="#exampleModal1{{$row->id}}"

                                           class="btn btn-info ">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a data-target="tooltip"
                                           onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" title="حذف"
                                           class="btn  btn-danger"
                                           href="{{URL::action('Admin\MessageController@getContactDelete',$row->id)}}"><i
                                                class="fa fa-trash "></i></a>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>

                    @if(count($data))
                        {!! $data->appends(Request::except('page'))->render() !!}
                    @endif

                </div>
            </div>
        </div>
    </div>
    @foreach($data as $row)
        <div class="modal fade" id="exampleModal1{{$row->id}}" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLabel">مشاهده متن پیام</h5>

                    </div>
                    @include('layouts.message-swal')
                    <form method="POST" action="{{URL::action('Admin\MessageController@postContactEdit',$row->id)}}">
                        {{csrf_field()}}
                        <div class="modal-body">

                            <div class="card border-info">
                                <div class="d-flex justify-content-between border-bottom p-md-3">
                                    <div>
                                        نام:
                                        {{ $row->name}}
                                        <br>
                                        ایمیل:
                                        {{  $row->email}}
                                        <br>




                                    </div>

                                </div>
                                <div class="card-body">
                                    متن پیام: {{$row->message}}
                                </div>


                            </div>
                            <input type="hidden" value="1" name="read_at">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger text-light" data-dismiss="modal">بستن</button>
                            <button type="submit" class="btn btn-primary text-light">خوانده شد</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{URL::current()}}" method="GET" class="m-0">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLabel">جستجو</h5>

                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="email">ایمیل :</label>
                            <input type="text" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label for="message">متن :</label>
                            <textarea type="text" class="form-control" name="message"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger text-light" data-dismiss="modal">بستن</button>
                        <button type="submit" class="btn btn-success text-light">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@stop
@section('js')

    <script>
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>
@stop
