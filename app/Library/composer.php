<?php



use App\Models\Setting;
use App\Models\Lang;
use Illuminate\Support\Facades\View;

$langs = [];
$setting_main=[];
$socials_main=[];
$parentcat_main=[];


$langs = Lang::all();
$setting_main = Setting::first();
$socials_main = \App\Models\Social::orderby('id')->where('status','1')->get();
$parentcat_main = \App\Models\Category::orderby('id')->where('status','1')->whereNull('parent_id')->take(4)->get();

View::share([
    'langs' => $langs,
    'setting_main' => $setting_main,
    'socials_main' => $socials_main,
    'parentcat_main' => $parentcat_main,


]);
