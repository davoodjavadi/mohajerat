<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests\NewsRequest;
use App\Library\Helper;
use app\Library\UploadImg;
use App\Models\Category;
use App\Models\Content;
use App\Models\Service;
use App\Models\Article;
use App\Models\Redirects;
use App\Models\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ArticleController extends Controller
{
    public static function  is_english($str){
        if (strlen($str) == strlen(utf8_decode($str))) {
            return false;
        } else {
            return true;
        }
    }


    public function getArticle(Request $request){

        $query= Article::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.Helper::persian2LatinDigit($request->get('title')).'%');
        }

        $articles=$query->paginate(50);
        return view('admin.article.index')
            ->with('articles',$articles);
    }


    public function getAddArticle(){
        $langs = Lang::all();
        $articles= Article::orderby('id','DESC')->get();
        return View('admin.article.add')
            ->with('articles',$articles)
            ->with('langs',$langs);
    }


    public function postAddArticle(NewsRequest  $request){
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        $u = Content::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();

        if ($u) {
            return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
        }
        if ($request->hasFile('image')) {
            $path = "assets/uploads/article/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }


        $article=Article::create($input);
        return Redirect::action('Admin\ArticleController@getArticle')->with('success','آیتم با موفقیت افزوده شد');
    }


    public function getEditArticle($id){
        $data = Article::find($id);
       $langs = Lang::all();

        return view('admin.article.edit')
            -> with('data',$data)
            -> with('langs',$langs);
    }


    public function postEditArticle($id,Request $request){

        $input = $request->all();
        //dd($input);
        $article=Article::find($id);
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        if ($input['url'] != $article->url) {
            $u = Article::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();
            if ($u) {
                return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
            }

            $redirects=Redirects::create([
                "old_address"=>'blog/'.$article->url,
                "new_address"=>'blog/'.$input['url'],
            ]);
        }



        if ($request->hasFile('image')) {
            $path = "assets/uploads/article/";
            File::delete($path . '/big/' . $article->image);
            File::delete($path . '/medium/' . $article->image);
            File::delete($path . '/small/' . $article->image);
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        else {
            $input['image'] = $article->image;
        }

//        if ($request->hasFile('icon')){
//            File::delete('assets/uploads/content/article/'. $article->icon);
//            $pathMain="assets/uploads/content/article/";
//            $extension=$request->file('icon')->getClientOriginalExtension();
//            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
//            if (true){
//                $fileName=md5(microtime()).".$extension";
//                $request->file('icon')->move($pathMain,$fileName);
//                $input['icon']=$fileName;
//            }else{
//                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
//            }
//
//        }else{
//            $input['icon'] = $article->icon;
//        }

        $article->update($input);
        return Redirect::action('Admin\ArticleController@getArticle')->with('success','تغییرات آیتم با موفقیت افزوده شد');
    }


    public function getDeleteArticle($id){
        $article=Article::find($id);
        $redirect = Redirects::create([
            "old_address" =>'blog/'.@$article->url,
            "new_address" => '',

        ]);

        $path = "assets/uploads/article/";
        File::delete($path . '/big/' . $article->image);
        File::delete($path . '/medium/' . $article->image);
        File::delete($path . '/small/' . $article->image);

        Article::destroy($id);
        return Redirect::back()->with('success','آیتم مورد نظر با موفقیت حذف شد');
    }
}
