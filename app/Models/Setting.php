<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    protected $table='settings';
    use SoftDeletes;
    protected $fillable=['title','logo','favicon','email','email2','des_seo','contact_number','contact_number2','whatsapp_number','contact_address','map','map_tehran','index_title','index_description','aboutindex_title','aboutindex_description','about_title','about_img','about_description','aboutfooter_title','aboutfooter_description','contact_addressfa','index_titlefa','index_descriptionfa','aboutindex_titlefa','aboutindex_descriptionfa','contact_addressen','index_titleen','index_descriptionen','aboutindex_titleen','aboutindex_descriptionen' ,'ua_title','ua_description'];
}
