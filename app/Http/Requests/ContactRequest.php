<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:3|max:255',
            'message'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'نام الزامی است.',
            'name.min'=>'نام کمتر از سه است.',
            'name.max'=>'نام بیشتر از 255 کاراکتر است.',
            'message.required'=>'پیام اجباری است.',
        ];
    }

}
