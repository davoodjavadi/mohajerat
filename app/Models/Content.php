<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    protected $table='contents';
    use SoftDeletes;
    protected $fillable=['title','type','url','image','icon','short_description','description','status','title_seo','des_seo'];

    public function scopeArticle($query)
    {
        $data = $query->whereType('1');
        return $data;
    }
    public function scopeNews($query)
    {
        $data = $query->whereType('2');
        return $data;
    }
    public function scopeSlider($query)
    {
        $data = $query->whereType('3');
        return $data;
    }
    public function scopeActive($query)
    {
        $data = $query->where('status','1');
        return $data;
    }
    public function comments(){
        return $this->morphMany('App\Models\Comment','Commentable')->whereNull('parent_id');
    }
}
