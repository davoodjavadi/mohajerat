<div class="d-xs-block d-sm-none">
    <div class="scrollsr">
        @foreach($services as $service)
        <a href="{{url('/service/'.@$service->url)}}">
            <div class="service-inner-xs" style="background-image: url({{asset('assets/uploads/content/service/medium/'.@$service->image)}});">
                <div class="over1">
                    <h6 class="fw-bolder">{!! @$service->title !!}</h6>
                    <p class="text-center">
                        {!! Illuminate\Support\Str::limit(@$service->short_description,200, $end='...')!!}

                    </p>
                </div>
            </div>
        </a>
        @endforeach
    </div>
</div>
