@extends('layouts.site.master')
@section('title')
    {{'خدمات'}}
@stop
@section('image_seo'){{ asset('assets/uploads/content/'.@$setting_main->logo)}}@stop

@section('description')
@stop
@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder">خدمات</h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>خانه</a></li>
                <li class="breadcrumb-item active" aria-current="page">خدمات</li>
            </ol>
        </div>

    </nav>
    <div class="content py-3">
        <div class="cat-list mt-3">
            <div class="container">
                <div class="row w-100 m-0">
                    @foreach(@$category as $row)
                    <div class="col-xl-3 col-md-4 col-sm-6  p-1">
                        <div class="blog-box border rounded-3">
                            <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getSubCategory',@$row->url)}}" >
                                <figure class="m-0">
                                    <div class="img-box fullup-me position-relative">
                                        <img src="{{asset('assets/uploads/content/cat/'.@$row->image)}}" class="w-100">
                                        <span class="caption">
                                        </span>
                                        <h5
                                            class="position-absolute top-0 end-0 start-0 bottom-0 d-flex justify-content-center align-items-center text-white fw-bolder">
                                            {{@$row->title}} </h5>
                                    </div>
                                </figure>
                            </a>
                            @if(@$row->childs->count() > 0)
                            <div class="cat  my-2">
                                <ul class="p-0 m-0">
                                    @foreach(@$row->childs as $row2)
                                    <li class="list-unstyled p-1">
                                        <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getCategoryDetail',@$row2->url)}}" class="d-flex align-items-center">
                                            <i class="bi bi-chevron-left d-flex"></i>
                                            {{@$row2->title}}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
@stop
