@extends('layouts.admin.master')


@section('content')
    <div id="page-wrapper">
        <div class="container-fluid mt-md-3">

            <div class="col-lg-12">
                <div class="page-header">
                    <div class="row d-flex justify-content-between">
                        <div class="col-lg-6 main-header">
                            <h2 class="text-dark">ریدایرکت</h2>

                        </div>
                        <div class="col-lg-6">
                            <a href="{{URL::action('Admin\RedirectController@getRedirectAdd')}}" class="btn btn-pill btn-primary btn-air-primary"> افزودن </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <form method="post" action="{{url('/admin/redirect/delete')}}">
                        {{ csrf_field() }}
                        <div class="col-12 d-flex justify-content-end">
                            <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
                                    data-toggle="tooltip" data-original-title="Delete selected items"
                                    class="btn mb-md-3 btn-space btn-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-trash me-2" viewBox="0 0 16 16">
                                    <path
                                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                    <path fill-rule="evenodd"
                                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                </svg>
                                حذف انتخاب شده ها
                            </button>
                        </div>
                    <table class="table">
                        <thead class="bg-primary text-center text-light">
                        <tr>
                            <th>
                                <input id="check-all" style="opacity: 1;position:static;" type="checkbox" class="me-1" >
                                انتخاب همه </th>
                            <th >شماره</th>
                            <th style="width: 20px;">آدرس قدیمی</th>
                            <th style="width: 20px;">آدرس جدید</th>
                            <th > نوع</th>
                            <th >عملیات</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach($redirect as $key=>$redi)

                            <tr>

                                <td class="text-center">
                                    <input style="opacity: 1;position:static;"
                                           name="deleteId[]" class="delete-all"
                                           type="checkbox"
                                           value="{{$redi->id}}" />
                                </td>
                                <td  class="text-center">{{($redirect->appends(Request::except('page'))->currentPage() - 1) *
($redirect->appends(Request::except('page'))->perPage()) + $key+1}}</td>
                                <td class="text-center" width="20px;" >{{$redi->old_address}}</td>
                                <td class="text-center" width="20px;" >{{$redi->new_address}}</td>
                                <td class="text-center">@if($redi->type==1)301 @elseif($redi->type==2) 302 @endif</td>



                                <td class="text-center">
                                    <a class="btn btn-sm btn-danger" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" href="{{URL::action('Admin\RedirectController@getRedirectDelete',$redi->id)}}"><i class="fa fa-trash  fa-md"></i></a>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    </form>
                </div>
                <div class="pagii">
                    @if(count($redirect))
                        {!! $redirect->appends(Request::except('page'))->render() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script>
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>
@stop
