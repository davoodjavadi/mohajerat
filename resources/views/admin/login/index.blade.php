<!DOCTYPE html>
<html>
@include('layouts.admin.blocks.head')
<style>
    .login{

        background: url({{asset('assets/admin/images/other-images/2.jpg')}});
        height: 100vh;
        display: flex;
        align-items: center;
        justify-content: center;

        background-size: 200% 200%;
        animation: movingGradiant 60s linear infinite alternate ;

    }
@keyframes movingGradiant {
    from{background-position: 0 0;}
    to{background-position: 100% 100%;}

}
    .login .row {
        width: 100%;
        margin: 0px
    }
    .my-card{
        background-color: rgba(61, 47, 47, 0.29);
    }
</style>

<body class="hold-transition sidebar-mini">
@include('layouts.message-swal')
<div class="login bg-light">
    <div class="row">
        <div class="col-md-3 p-0 mx-auto">
            <div class="card p-3 border-0 rounded my-card">
                <form action="{{URL::action('Admin\LoginController@postLogin')}}" method="POST" enctype="multipart/form-data" class="m-0">
                    {{csrf_field()}}
                    <div class="form-group m-2">
                        <h3 class="text-center my-0 pb-3 text-dark">
                            ورود به پنل مدیریت
                        </h3>
                    </div>
                    <div class="form-group m-2">
                        <input type="text"  name="email" class="form-control rounded shadow-sm p-3  "
                               placeholder="ایمیل">
                    </div>
                    <div class="form-group  m-2" >
                        <input type="password" name="password" class="form-control rounded shadow-sm p-3 "
                               placeholder="رمز">
                    </div>
                    <div class="form-group m-2">
                        <button class="btn btn-outline-dark btn-block" type="submit"> ورود</button>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('layouts.admin.blocks.script')

</html>
