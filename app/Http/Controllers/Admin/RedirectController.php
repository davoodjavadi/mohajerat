<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RedirectRequest;

use App\Models\Redirects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RedirectController extends Controller
{
    //---Redirects-----
    public function getRedirect(){

        $redirect = Redirects::orderby('id','DESC')->paginate(100);

        return view('admin.redirect.index')
            ->with('redirect', $redirect);
    }
    public function getRedirectAdd(){
        return view('admin.redirect.add');

    }
    public function postRedirectAdd(RedirectRequest $request){
        $input=$request->all();
        $remove = array(url('/'.@$input->old_address).'/');
        $remove2 = array(url('/'.@$input->new_address).'/');
        $input['old_address']=str_replace($remove, "", $input['old_address']);
        $input['new_address']=str_replace($remove2, "", $input['new_address']);
       $redirect = Redirects::create($input);


        return Redirect::action('Admin\RedirectController@getRedirect');

    }
    public function getRedirectDelete($id){
        $content = Redirects::find($id);

        Redirects::destroy($id);
        return Redirect::action('Admin\RedirectController@getRedirect');
    }
    public function postDeleteRedirect(Request $request)
    {
        if (Redirects::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }

}
