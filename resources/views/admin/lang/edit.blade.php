@extends('layouts.admin.master')
@section('content')
    <div class="card border">
        <div class="card-header">
            <h5>ویرایش کشور</h5>
        </div>
        <form class="form theme-form" action="{{URL::action('Admin\LangController@postEditLang',$data->id)}}" method="POST" enctype="multipart/form-data">
            @include('admin.lang.form')
        </form>
    </div>
@endsection
