
<!-- latest jquery-->
<script src="{{asset('/assets/admin/js/jquery-3.5.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('/assets/admin/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('/assets/admin/js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('/assets/admin/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('/assets/admin/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('/assets/admin/js/sidebar-menu.js')}}"></script>
<script src="{{asset('/assets/admin/js/config.js')}}"></script>

<!-- Plugins JS start-->
<script src="{{asset('/assets/admin/js/typeahead/handlebars.js')}}"></script>
<script src="{{asset('/assets/admin/js/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('/assets/admin/js/typeahead/typeahead.custom.js')}}"></script>
<script src="{{asset('/assets/admin/js/typeahead-search/handlebars.js')}}"></script>
<script src="{{asset('/assets/admin/js/typeahead-search/typeahead-custom.js')}}"></script>
<script src="{{asset('/assets/admin/js/prism/prism.min.js')}}"></script>
<script src="{{asset('/assets/admin/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('/assets/admin/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('/assets/admin/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('/assets/admin/js/counter/counter-custom.js')}}"></script>
<script src="{{asset('/assets/admin/js/custom-card/custom-card.js')}}"></script>
{{--<script src="{{asset('/assets/admin/js/notify/bootstrap-notify.min.js')}}"></script>--}}
<script src="{{asset('/assets/admin/js/dashboard/default.js')}}"></script>
{{--<script src="{{asset('/assets/admin/js/notify/index.js')}}"></script>--}}
<script src="{{asset('/assets/admin/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('/assets/admin/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('/assets/admin/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
<script src="{{asset('/assets/admin/js/chat-menu.js')}}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('/assets/admin/js/script.js')}}"></script>
<script src="{{asset('/assets/admin/js/theme-customizer/customizer.js')}}"></script>
<!-- =============== ckeditor SCRIPTS ===============-->
<script src="{{asset('assets/admin/ckeditor/ckeditor.js')}}"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
