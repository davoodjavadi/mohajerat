<div class="services">
    <div class="container">
        <div class="service-in">
            <div class="title-service mb-4">
                <h3 class="text-one fw-bolder text-center h2">{!! @$lang->service_title !!}</h3>
                <p class="text-center">
                    {!! @$lang->description !!}
                </p>
            </div>
            @include('layouts.site.blocks.content.services-xs')
            <div class="d-xs-none d-sm-block d-md-block">
                <div class="row w-100 m-0">
                    @foreach($services as $service)
                    <div class="col-xl-2 col-md-4 col-sm-6 col-xs-6 p-0">
                        <div class="service-inner" style="background-image: url({{asset('assets/uploads/content/service/medium/'.@$service->image)}});">
                            <div class="over1">
                                <h6 class="fw-bolder">{!! @$service->title !!}</h6>
                            </div>
                            <div class="over2">
                                <a href="{{url('/service/'.@$service->url)}}">
                                    <h6 class="fw-bolder pb-4 text-white">{!! @$service->title !!}</h6>
                                </a>
                                <p class="text-center">
                                    {!! Illuminate\Support\Str::limit(@$service->short_description,200, $end='...')!!}
                                </p>
                                <a class="btn btn-more py-1 px-3" href="{{URL::action('Site\HomeController@getServiceDetail',@$service->url)}}">بیشتر </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="flying">
        <div class="container">
            <div class="bird-container bird-container--one">
                <div class="bird bird--one"></div>
            </div>

            <div class="bird-container bird-container--two">
                <div class="bird bird--two"></div>
            </div>

            <div class="bird-container bird-container--three">
                <div class="bird bird--three"></div>
            </div>

            <div class="bird-container bird-container--four">
                <div class="bird bird--four"></div>
            </div>
            <img src="{{asset('assets/site/images/services.png')}}" class="w-100">
        </div>
    </div>
</div>
