@extends('layouts.admin.master')

@section('content')
<div class="container-fluid  dashboard-content">

	<!-- ============================================================== -->
	<!-- pageheader -->
	<!-- ============================================================== -->
	<div class="row w-100 m-0">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0 d-flex justify-content-center">

			<div class="page-header">
				<h3 class=" py-2 px-4 rounded-lg">
                    کامنت ها




                </h3>
			</div>
		</div>
	</div>


			<div class="">



				<!-- ============================================================== -->
				<!-- end pageheader -->
				<!-- ============================================================== -->
				<div class="row w-100 m-0">
					<!-- ============================================================== -->
					<!-- basic table  -->
					<!-- ============================================================== -->
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="card">

							<div class="card-body px-1">
								<div class="table-responsive">

									<div id="DataTables_Table_0_wrapper"
										class="dataTables_wrapper dt-bootstrap4">

										<div class="row w-100 m-0">
											<div class="col-sm-12">
												<table
													class="table table-striped table-bordered first dataTable"
													id="DataTables_Table_0" role="grid"
													aria-describedby="DataTables_Table_0_info">

													<thead>
														<tr role="row">

															<th class="sorting_asc" tabindex="0"
																aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1"
																style="width: 80.0667px;"
																aria-sort="ascending"
																aria-label="Name: activate to sort column descending">
																ردیف
															</th>
															<th class="sorting_asc" tabindex="0"
																aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1"
																style="width: 213.75px;"
																aria-sort="ascending"
																aria-label="Name: activate to sort column descending">
                                                                متعلق به
															</th>
															<th class="sorting" tabindex="0"
																aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1"
																style="width: 80.0667px;"
																aria-label="Position: activate to sort column ascending">
                                                                نام
															</th>

                                                            <th class="sorting" tabindex="0"
                                                                aria-controls="DataTables_Table_0"
                                                                rowspan="1" colspan="1"
                                                                style="width: 80.0667px;"
                                                                aria-label="Position: activate to sort column ascending">
                                                                تاریخ
                                                            </th>
                                                            <th class="sorting" tabindex="0"
                                                                aria-controls="DataTables_Table_0"
                                                                rowspan="1" colspan="1"
                                                                style="width: 80.0667px;"
                                                                aria-label="Position: activate to sort column ascending">
                                                      وضعیت
                                                            </th>
                                                            <th class="sorting" tabindex="0"
                                                                aria-controls="DataTables_Table_0"
                                                                rowspan="1" colspan="1"
                                                                style="width: 80.0667px;"
                                                                aria-label="Position: activate to sort column ascending">
                                                                نمایش در صفحه
                                                            </th>
															<th class="sorting" tabindex="0"
																aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1"
																style="width: 155.217px;"
																aria-label="Age: activate to sort column ascending">
																عملیات
															</th>

														</tr>
													</thead>
													<tbody>
														@foreach(@$data as $key=>$row)
														<tr role="row" class="odd">

                                                            <td class="sorting_1">{{(@$data->appends(Request::except('page'))->currentPage() - 1) *
                                                                (@$data->appends(Request::except('page'))->perPage()) + $key+1}}</td>

															<td class="sorting_1">
                                                                @if(@$row->commentable_type == 'App\Models\Service')
                                                                    سرویس/ {{ Illuminate\Support\Str::limit(@$row->servis->title,20, $end='...')}}
                                                                @elseif(@$row->commentable_type == 'App\Models\Content')
                                                                @if(@$row->content->type == 1)
                                                                        مقاله/ {{ Illuminate\Support\Str::limit(@$row->content->title,20, $end='...')}}
                                                                    @else
                                                                        خبر/ {{ Illuminate\Support\Str::limit(@$row->content->title,20, $end='...')}}
                                                                    @endif
                                                                @endif</td>

															<td class="sorting_1"> {{@$row->name}}
                                                                @php  $com = \App\Models\Comment::where('id', $row->parent_id)->first(); @endphp
                                                                @if(isset($row->parent_id))/پاسخ کامنت {{ Illuminate\Support\Str::limit(@$com->name,20, $end='...')}} @endif

                                                            </td>
															<td class="sorting_1">{{jdate('Y/m/d H:i',$row->created_at->timestamp)}} </td>

                                                            <td class="sorting_1">@if($row->read_at==1)
                                                                    <span class='label label-success'>خوانده شده</span>
                                                                @else
                                                                    <span class='label label-danger'>خوانده نشده</span>
                                                                @endif</td>

															<td class="sorting_1">  @if($row->status==1)
                                                                    <span class='label label-success'>تایید شده</span>
                                                                @else
                                                                    <span class='label label-danger'>تایید نشده</span>
                                                                @endif </td>

															<td>
                                                                <a href="{{URL::action('Admin\MessageController@getCommentDelete',$row->id)}}" type="button" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');" class="btn btn-danger my-1 btn-circle"><i class="fa fa-trash"> </i></a>

                                                                <a href="#" data-toggle="modal"
                                                                   data-target="#messageModal{{$row->id}}"
                                                                   data-original-title="مشاهده سریع"
                                                                   title="مشاهده سریع"
                                                                   class="btn btn-space btn-info"><i
                                                                        class="fa fa-eye"></i></a>
                                                            @include('admin.comment.modal')
                                                                @if($row->parent_id == null)
                                                                <a href="messageModalrep{{$row->id}}" data-toggle="modal"
                                                                   data-target="#messageModalrep{{$row->id}}"
                                                                   data-original-title="پاسخ "
                                                                   title="پاسخ"
                                                                   class="btn btn-space btn-warning"><i
                                                                        class="fa fa-reply"></i></a>

                                                                    @include('admin.comment.modalreply')
                                                                @endif

															</td>
														</tr>

														@endforeach
													</tbody>
												</table>
												<div class="pagii">
													@if(count($data))
													{!!
													$data->appends(Request::except('page'))->render()
													!!}
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

	</div>
</div>

@stop

@section('js')
    <script>
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>

@stop
