<div class="sidebar d-sm-none d-xs-none d-md-block">
    <div class="related mb-2">
        <div class="row w-100 m-0">
            @if($related->isNotEmpty())
            <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">
                <div class="title-related">
                    <p class="h5 text-end fw-bolder text-one">
                        خدمات مرتبط
                    </p>
                </div>
            </div>
            @foreach(@$related as $rel)
            <div class="col-xl-12 col-md-12 col-sm-12 p-0 py-2">
                <a href="{{URL::action('Site\HomeController@getServiceDetail',@$rel->url)}}">
                    <div class="related-box">
                        <div class="row w-100 m-0">
                            <div class="col-xl-9 col-md-9 col-sm-8 col-xs-8 p-0 pe-2 align-self-center">
                                <p class="h6 m-0 fw-bolder text-end">{{@$rel->title}}</p>
                            </div>
                            <div class="col-xl-3 col-md-3 col-sm-4 col-xs-4 p-0 align-self-center">
                                <div class="border-box">
                                    <div class="img-box">
                                        @if(file_exists('assets/uploads/service/'.@$rel->icon))
                                        <img src="{{asset('assets/uploads/service/'.@$rel->icon)}}">
                                        @else
                                            <img src="{{asset('assets/site/images/not-found1.png')}}">
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </a>
            </div>
            @endforeach
             @endif
        </div>
    </div>
</div>
