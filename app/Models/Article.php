<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title','lang_id','description','short_description','status','image','title_seo','description_seo','url'];


    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');
    }
}
