@extends('layouts.site.master')
@section('title'){{@$category->title_seo ? $category->title_seo : $category->title}} @stop
@section('image_seo'){{ @$category->image[0]->file ? asset('assets/uploads/content/cat/small/'.$category->image[0]->file) : asset('assets/uploads/content/'.@$setting_main->logo)}}
@endsection

@section('description')
    @if($category->description_seo != null)
        {!! $category->description_seo !!}
    @else
        {!! strip_tags(\Illuminate\Support\Str::words($category->description,100)) !!}
    @endif
@stop
@section('content')
    @include('layouts.site.blocks.menu-inner')
    <div class="header-inner">
        <div class="container">
            <div class="title-inner">
                <h1 class="text-one text-center fw-bolder">{{$category->title}}</h1>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb" class="content">
        <div class="container">
            <ol class="breadcrumb p-0 m-0 px-1 pb-3">
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getIndex')}}" class="text-one">
                        <i class="bi bi-house-door-fill text-one"></i>
                        خانه</a></li>
                <li class="breadcrumb-item"><a href="{{URL::action('Site\HomeController@getCategory')}}" class="text-one">
                        خدمات</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$category->title}}</li>
            </ol>
        </div>

    </nav>
    <div class="content py-3">
        <div class="costumer mt-3">
            <div class="container">
                <div class="description  mb-5">
                    <div class="des-in p-2">
                        <p class="text-justify">{!! @$category->description !!}
                        </p>
                    </div>
                </div>
                <div class="row w-100 m-0">
                    @foreach(@$category->childs as $sub)
                        <div class="col-xl-3 col-md-6 col-sm-6 mb-5 p-1">
                            <a href="{{URL::action('Site\HomeController@getCategoryDetail',@$sub->url)}}">
                                <div class="costumer-in position-relative">
                                    <div class="box-costumer position-absolute">
                                        <div class="img-box">
                                            <img src="{{asset('assets/uploads/content/cat/'.@$sub->icon)}}" class="w-100">
                                        </div>
                                    </div>
                                    <h5 class="fw-bolder">{{@$sub->title}}</h5>
                                    <p class="text-justify mb-2">
                                        {!! strip_tags(\Illuminate\Support\Str::words(@$sub->description,100)) !!}
                                    </p>
                                    <a href="{{\Illuminate\Support\Facades\URL::action('Site\HomeController@getCategoryDetail',@$sub->url)}}" class="btn btn-custom mb-4 px-3 py-1">بیشتر</a>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
