@extends('layouts.admin.master')
@section('content')
    <div class="card border">
        <div class="card-header">
            <h5>ویرایش تصویر</h5>
        </div>
        <form class="form theme-form" action="{{URL::action('Admin\SliderController@postEditSlider',@$data->id)}}" method="POST" enctype="multipart/form-data">
            @include('admin.slider.form')
        </form>
    </div>
@endsection
