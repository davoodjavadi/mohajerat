<div class="sidebar d-xs-block d-sm-block d-md-none">
    <div class="sticky">
        <div class="related mb-3">
            <div class="row w-100 m-0">
                @if($related->isNotEmpty())
                <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">
                    <div class="title-related">
                        <p class="h5 text-start fw-bolder text-one">
                            اخبار مرتبط
                        </p>
                    </div>
                </div>
                @foreach($related as $rel2)
                <div class="col-xl-12 col-md-12 col-sm-12 p-0 py-2">
                    <a href="{{URL::action('Site\HomeController@getNewsDetail',@$rel2->url)}}">
                        <div class="related-box">
                            <div class="row w-100 m-0">
                                <div
                                    class="col-xl-3 col-md-3 col-sm-4 col-xs-4 p-0 pe-3 align-self-center h-50">
                                    <div class="border-box h-100">
                                        <div class="img-box h-100">
                                            @if(file_exists('assets/uploads/news/small/'.@$rel2->image))
                                            <img src="{{asset('assets/uploads/news/small/'.@$rel2->image)}}">
                                            @else
                                                <img src="{{asset('assets/site/images/not-found1.png')}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="col-xl-9 col-md-9 col-sm-8 col-xs-8 p-0 pe-2 align-self-center">
                                    <p class="h6 m-0 fw-bolder text-start">{{@$rel2->title}}</p>
                                </div>


                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                   @endif
            </div>
        </div>


        <div class="share ms-4 mb-3 m-xs-0">
            <div class="row w-100 m-0">
                <div class="col-xl-12 col-md-12 col-sm-12 p-0 ">
                    <div class="title-cat mt-2">
                        <p class="h5  fw-bolder text-one">
                            اشتراک گذاری در :
                        </p>
                    </div>
                </div>
                <div class="share-list p-0 d-flex justify-content-start">
                    <a href="https://www.instagram.com/?url={{url('/'.@$news->url)}}">
                        <i class="bi bi-instagram d-flex  fs-4"></i>
                    </a>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{url('/'.@$news->url)}}">
                        <i class="bi bi-facebook d-flex  fs-4"></i>
                    </a>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url('/'.@$news->url)}}">
                        <i class="bi bi-linkedin d-flex  fs-4"></i>
                    </a>
                    <a href="whatsapp://send?text={{url('/'.@$news->url)}}">
                        <i class="bi bi-whatsapp d-flex  fs-4"></i>
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
