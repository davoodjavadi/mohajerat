
<div class="iconsidebar-menu">
    <div class="sidebar">
        <ul class="iconMenu-bar custom-scrollbar">
            <li><a class="bar-icons" href="javascript:void(0)">
                    <i class="fa fa-home"></i><span>عمومی    </span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">
                    <li class="iconbar-header">داشبورد</li>
                    <li><a href="{{URL::action('Admin\IndexController@getIndex')}}">صفحه اول</a></li>
                    <li> <a href="{{URL::action('Admin\LangController@getLang')}}">کشور</a> </li>
                    <li> <a href="{{URL::action('Admin\MenuController@getMenu')}}">منو</a> </li>

                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa fa-building-o"></i><span>خدمات </span></a>

                <ul class="iconbar-mainmenu custom-scrollbar">
                    <li  class="iconbar-header sub-header"> دسته بندی </li>
                    <li> <a href="{{URL::action('Admin\CategoryController@getCategory')}}"> لیست دسته بندی </a> </li>
                    <li> <a href="{{URL::action('Admin\CategoryController@getAddCategory')}}"> افزودن دسته بندی </a> </li>
                    <li  class="iconbar-header sub-header"> خدمات </li>
                    <li> <a href="{{URL::action('Admin\ServiceController@getService')}}"> لیست خدمات </a> </li>
                    <li> <a href="{{URL::action('Admin\ServiceController@getAddService')}}"> افزودن خدمات </a> </li>


                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa fa-newspaper-o"></i><span>بلاگ </span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">

                    <li  class="iconbar-header sub-header"> مقالات </li>
                    <li> <a href="{{URL::action('Admin\ArticleController@getArticle')}}"> لیست مقالات </a> </li>
                    <li> <a href="{{URL::action('Admin\ArticleController@getAddArticle')}}"> افزودن مقالات </a> </li>
                    <li  class="iconbar-header sub-header"> اخبار </li>
                    <li> <a href="{{URL::action('Admin\NewsController@getNews')}}"> لیست اخبار </a> </li>
                    <li> <a href="{{URL::action('Admin\NewsController@getAddNews')}}"> افزودن اخبار </a> </li>

                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa fa-image"></i><span>مدیا </span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">


                    <li  class="iconbar-header sub-header"> اسلایدر </li>
                    <li> <a href="{{URL::action('Admin\SliderController@getSlider')}}"> لیست اسلایدر </a> </li>
                    <li> <a href="{{URL::action('Admin\SliderController@getAddSlider')}}"> افزودن اسلایدر </a> </li>


                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa fa-comment"></i><span>شعار ها </span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">

                    <li  class="iconbar-header sub-header"> شعار ها </li>
                    <li> <a href="{{URL::action('Admin\SloaganController@getSloagan')}}"> لیست  شعار ها</a> </li>
                    <li> <a href="{{URL::action('Admin\SloaganController@getAddSloagan')}}"> افزودن  شعار ها</a> </li>


                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa fa-envelope"></i></i><span>درخواست ها</span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">

                    <li  class="iconbar-header sub-header"> درخواست ها </li>
                    <li> <a href="{{URL::action('Admin\MessageController@getContact')}}"> پیام ها </a> </li>
                    <li> <a href="{{URL::action('Admin\MessageController@getComment')}}"> کامنت ها </a> </li>



                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa  fa-gear"></i><span>تنظیمات</span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">

                    <li  class="iconbar-header sub-header"> تنظیمات </li>
                    <li> <a href="{{URL::action('Admin\SettingController@getSetting')}}">تنظیمات</a> </li>
                    <li  class="iconbar-header sub-header"> سوشال مدیا </li>
                    <li> <a href="{{URL::action('Admin\SocialController@getSocial')}}"> لیست سوشال مدیا</a> </li>
                    <li> <a href="{{URL::action('Admin\SocialController@getAddSocial')}}"> افزودن سوشال مدیا</a> </li>



                </ul>
            </li>
            <li><a class="bar-icons" href="javascript:void(0)"><i class="fa fa-cloud-download"></i></i><span>سئو</span></a>
                <ul class="iconbar-mainmenu custom-scrollbar">

                    <li  class="iconbar-header sub-header"> افزونه های سئو </li>
                    <li> <a href="{{URL::action('Admin\UploaderController@getUploader')}}">آپلودر</a> </li>
                    <li> <a href="{{URL::action('Admin\RedirectController@getRedirect')}}">ریدایرکتر</a> </li>


                </ul>
            </li>
            <li><a class="bar-icons" href="{{url('/logout')}}"><i class=" text-danger fa fa-arrow-left fa-lg"></i><span>خروج</span></a>






            </li>


        </ul>
    </div>
</div>
