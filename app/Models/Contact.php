<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    protected $table='contacts';
    use SoftDeletes;
    protected $fillable=['name','subject','type','message','email','phone','read_at','lang_id'];

    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');;
    }
}
