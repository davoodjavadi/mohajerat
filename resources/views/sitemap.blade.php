<!--<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>-->
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
>
    <url>
        <loc>
            {{url('/')}}
        </loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>


    <url>
        <loc>
            {{url('/categories/')}}
        </loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc>
            {{url('/news/')}}
        </loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>
            {{url('/blog/')}}
        </loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>
            {{url('/about-us/')}}
        </loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>
            {{url('/contact-us/')}}
        </loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    @foreach($categories as $cat)
        <url>
            <loc>
                {{url('/category-detail/'.@$cat->url)}}
            </loc>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @foreach($categories2 as $cat2)
        <url>
            <loc>
                {{url('/category/'.@$cat2->url)}}
            </loc>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @foreach($services as $service)
        <url>
            <loc>
                {{url('/service/'.@$service->url)}}
            </loc>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @foreach($articles as $art)
        <url>
            <loc>
                {{url('/blog/'.@$art->url)}}
            </loc>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach

    @foreach($news as $new)
        <url>
            <loc>
                {{url('/news/'.@$new->url)}}
            </loc>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>
