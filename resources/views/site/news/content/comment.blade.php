
@foreach($comments as $cm)

    <ul class="comment">
        <li class="list-unstyled ">
            <div class="header-cm mb-3">
                <img src="{{asset('assets/site/images/user.png')}}" class="img-cm img-fluid text-success">

                <div class="text-cm position-relative w-100  p-3 ms-3">
                    <div class="name-cm d-flex justify-content-between">
                        <div class="">
                            <p class="h6 fw-bolder m-0">{{@$cm->name}}</p>
                            <span>{{jdate('Y/m/d',@$cm->created_at->timestamp)}}</span>
                        </div>
                        <button type="button" class="btn btn-reply d-flex" data-bs-toggle="modal" data-bs-target="#exampleModal{{$cm->id}}">
                            <i class="bi bi-reply d-flex me-1"></i> پاسخ
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal{{$cm->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <form action="{{URL::action('Site\HomeController@postComment')}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="commentable_id" value="{{@$news->id}}">
                                        <input type="hidden" name="commentable_type" value="{{"App\Models\Content"}}">
                                        <input type="hidden" name="parent_id" value="{{@$cm->id}}">
                                        <div class="modal-header">
                                            <p class="modal-title h5 text-one" id="exampleModalLabel">پاسخ</p>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="mb-3">
                                                <label for="exampleFormControlInput1"
                                                       class="form-label text-one">نام   </label>
                                                <input name="name" type="text" class="form-control"
                                                       id="exampleFormControlInput1"
                                                       placeholder="برای مثال : رضا عباسی">
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleFormControlInput1"
                                                       class="form-label text-one">ایمیل</label>
                                                <input name="email" type="text" class="form-control"
                                                       id="exampleFormControlInput1"
                                                       placeholder="name@example.com">
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleFormControlTextarea1"
                                                       class="form-label text-one">نظر خود را بنویسید
                                                </label>
                                                <textarea name="message" class="form-control" id="exampleFormControlTextarea1"
                                                          rows="4"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-success">ارسال</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="">{{@$cm->message}}</p>
                </div>
            </div>
            @if(@$cm->replies->count() > 0)
                @foreach(@$cm->replies as $rep)
                    <ul class="comment">
                        <li class="list-unstyled">
                            <div class="header-cm mb-3">
                                <img src="{{asset('assets/site/images/user.png')}}"
                                     class="img-cm img-fluid">

                                <div class="text-cm position-relative w-100  p-3 ms-3">
                                    <div class="name-cm d-flex justify-content-between">
                                        <div class="">
                                            <p class="h6 fw-bolder m-0">{{@$rep->name}}</p>
                                            @if($rep->reply_id == null)
                                                @php
                                                    $parent=\App\Models\Comment::where('id',@$rep->parent_id)->first();
                                                @endphp

                                                <span>پاسخ به : {{@$parent->name}}</span>
                                                <br>
                                            @else
                                                @php
                                                    $parent2=\App\Models\Comment::where('id',@$rep->reply_id)->first();
                                                @endphp
                                                <span>پاسخ به : {{@$parent2->name}}</span>
                                                <br>
                                            @endif
                                            <span>{{jdate('Y/m/d',@$rep->created_at->timestamp)}}</span>
                                        </div>
                                        <button type="button" class="btn btn-reply d-flex" data-bs-toggle="modal" data-bs-target="#exampleModal1{{$rep->id}}">
                                            <i class="bi bi-reply d-flex me-1"></i> پاسخ
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal1{{$rep->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                                <div class="modal-content">
                                                    <form action="{{URL::action('Site\HomeController@postComment')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="commentable_id" value="{{@$news->id}}">
                                                        <input type="hidden" name="commentable_type" value="{{"App\Models\Content"}}">
                                                        <input type="hidden" name="parent_id" value="{{@$cm->id}}">
                                                        <input type="hidden" name="reply_id" value="{{@$rep->id}}">
                                                        <div class="modal-header">
                                                            <p class="modal-title h5 text-one" id="exampleModalLabel">پاسخ</p>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="exampleFormControlInput1"
                                                                       class="form-label text-one">نام   </label>
                                                                <input name="name" type="text" class="form-control"
                                                                       id="exampleFormControlInput1"
                                                                       placeholder="برای مثال : رضا عباسی">
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="exampleFormControlInput1"
                                                                       class="form-label text-one">ایمیل</label>
                                                                <input name="email" type="text" class="form-control"
                                                                       id="exampleFormControlInput1"
                                                                       placeholder="name@example.com">
                                                            </div>

                                                            <div class="mb-3">
                                                                <label for="exampleFormControlTextarea1"
                                                                       class="form-label text-one">نظر خود را بنویسید
                                                                </label>
                                                                <textarea name="message" class="form-control" id="exampleFormControlTextarea1"
                                                                          rows="4"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <button type="submit" class="btn btn-success">ارسال</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="">{{@$rep->message}}</p>
                                </div>
                            </div>

                        </li>
                    </ul>
                @endforeach
            @endif
        </li>
    </ul>
@endforeach

