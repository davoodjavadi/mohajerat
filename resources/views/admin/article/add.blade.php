@extends('layouts.admin.master')
@section('content')
<div class="card border">
    <div class="card-header">
        <h5>افزودن مقاله</h5>
    </div>
    <form class="form theme-form" action="{{URL::action('Admin\ArticleController@postAddArticle')}}" method="POST" enctype="multipart/form-data">
@include('admin.article.form')
    </form>
</div>
@endsection
