<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryRequest;
use app\Library\UploadImg;
use App\Models\Gallery;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UploaderController extends Controller
{
    public function getUploader()
    {
        $uploaders=Upload::orderby('id','desc')->paginate(100);

        return view('admin.uploader.index')
            ->with('uploaders' , $uploaders);
    }
    public function getAddUploader()
    {

        return view('admin.uploader.add');
    }
    public function postAddUploader(GalleryRequest $request)
    {

        $input = $request->all();
//        if ($request->hasFile('image')){
//            $pathMain="assets/uploads/content";
//            $extension=$request->file('image')->getClientOriginalExtension();
//            $fileName=md5(microtime()).".$extension";
//            $request->file('image')->move($pathMain,$fileName);
//            $input['image']=$fileName;
//        }
        if ($request->hasFile('image')) {
            $path = "assets/uploads/content/uploader/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }


//        dd($input);
        $uploader=Upload::create($input);
        return Redirect::action('Admin\UploaderController@getUploader')->with('success','تصویر  با موفقیت افزوده شد');
    }

    public function getDeleteUploader($id)
    {
        Upload::destroy($id);
        return Redirect::back()->with('success',' تصویر موردنظر با موفقیت حذف شد');

    }
}
