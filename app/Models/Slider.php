<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

    protected $fillable = ['lang_id','title','status','description','image'];


    public function lang()
    {
        return $this->belongsTo('App\Models\Lang', 'lang_id');
    }
}
