<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdmingeneralRequest;
use App\Library\Helper;
use app\Library\MakeTree;
use app\Library\UploadImg;
use App\Models\Category;
use App\Models\Lang;
use App\Models\Redirects;
use App\Models\Content;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ServiceController extends Controller
{
    public static function  is_english($str){
        if (strlen($str) == strlen(utf8_decode($str))) {
            return false;
        } else {
            return true;
        }
    }
    public function getService(Request $request ){

        $query= Service::orderBy('id','DESC');
        if($request->get('title')){
            $query->where('title','LIKE','%'.$request->get('title').'%');

        }

        $services=$query->paginate(50);
        return view('admin.service.index')
            ->with('services',$services);
    }

    public function getServiceLang($id){
         $services = Service::where('lang_id',$id)->paginate(30);

        return view('admin.service.index')
            ->with('services',$services);
    }

    public function getAddService(){
        $langs = Lang::all();
        $services=Service::orderby('id','DESC')->get();
        $categories=Category::orderby('id','DESC')->get();
        if (!empty($categories)) {
            MakeTree::getData($categories);
            $categories = MakeTree::GenerateArray(array('get'));
        }
        return View('admin.service.add')
            ->with('categories',$categories)
            ->with('services',$services)
            ->with('langs',$langs);
    }


    public function postAddService(AdmingeneralRequest $request){
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['view_sp'] = $request->has('view_sp');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        $u = Service::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();

        if ($u) {
            return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
        }
        if ($request->hasFile('image')) {
            $path = "assets/uploads/service/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        if ($request->hasFile('icon')){
            $pathMain="assets/uploads/service/";
            $extension=$request->file('icon')->getClientOriginalExtension();
            $fileName=md5(microtime()).".$extension";
            $request->file('icon')->move($pathMain,$fileName);
            $input['icon']=$fileName;
        }else{
            return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
        }

//        dd($input);
        $service=Service::create($input);
        return Redirect::action('Admin\ServiceController@getService')->with('success','سرویس با موفقیت افزوده شد');
    }


    public function getEditService($id){
        $langs = Lang::all();
        $data = Service::find($id);
        $services = Service::orderby('id','DESC')->where('id','<>',$data->id)->get();
        $categories=Category::orderby('id','DESC')->get();
        if (!empty($categories)) {
            MakeTree::getData($categories);
            $categories = MakeTree::GenerateArray(array('get'));
        }
        return view('admin.service.edit')
            -> with('data',$data)
            ->with('services',$services)
            ->with('categories',$categories)
            ->with('langs',$langs);
    }


    public function postEditService($id,AdmingeneralRequest $request){

        $input = $request->all();
        $service=Service::find($id);
        $input['status'] = $request->has('status');
        $input['view_sp'] = $request->has('view_sp');
        $input['url']=str_replace(' ', '-',$input['url']);
        if(self::is_english($request['url'])){
            return redirect()->back()->with('error', 'آدرس url را به انگلیسی بنویسید');

        }
        if ($input['url'] != $service->url) {

            $u = Service::where('url', $request->get('url'))->orWhere('url', str_replace(' ', '-', $request['url']))->first();


            if ($u) {
                return redirect()->back()->with('error', 'آیتمی با این آدرس وجود دارد');
            }
            $redirects=Redirects::create([
                "old_address"=>'service/'.$service->url,
                "new_address"=>'service/'.$input['url'],
            ]);
        }


        if ($request->hasFile('image')) {
            $path = "assets/uploads/service/";
            File::delete($path . '/big/' . $service->image);
            File::delete($path . '/medium/' . $service->image);
            File::delete($path . '/small/' . $service->image);
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        else {
            $input['image'] = $service->image;
        }
        if ($request->hasFile('icon')){
            File::delete('assets/uploads/service/'. $service->icon);
            $pathMain="assets/uploads/service/";
            $extension=$request->file('icon')->getClientOriginalExtension();
            $ext = ['jpg','jpeg','png','xls','mp3','ogg'];
            if (true){
                $fileName=md5(microtime()).".$extension";
                $request->file('icon')->move($pathMain,$fileName);
                $input['icon']=$fileName;
            }else{
                return Redirect::back()->with('error','فایل ارسالی صحیح نیست');
            }

        }else{
            $input['icon'] = $service->icon;
        }

        $service->update($input);
        return Redirect::action('Admin\ServiceController@getService')->with('success','تغییرات سرویس با موفقیت افزوده شد');
    }


    public function getDeleteService($id){
        $service=Service::find($id);
        $redirects=Redirects::create([
            "old_address"=>'service/'.$service->url,
            "new_address"=>'',
        ]);
        File::delete('assets/uploads/service/'. $service->image);
        Service::destroy($id);
        return Redirect::action('Admin\ServiceController@getService')->with('success','سرویس مورد نظر با موفقیت حذف شد');
    }
}
