<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="@yield('robots','noindex','nofollow')" />
    <link rel="shortcut icon" href="{{asset('assets/uploads/content/'.@$setting_main->favicon)}}" type="image/x-icon">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/bootstrap.rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/site/css/style.css?v0.09')}}">
    <link rel="stylesheet" href="{{asset('assets/site/css/responsive.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
    <script src="https://use.fontawesome.com/4e7411b485.js"></script>
    <script src="{{asset('assets/admin/js/sweetalert.min.js')}}"></script>

    <title>
        @yield('title',$lang->title_seo)
    </title>
    <meta name="robots" content="@yield('robots','noindex ,nofollow')" />
    <meta name="description" content="@yield('description',@$lang->description_seo)" />
    <link rel="canonical" href="{{url()->current()}}" />
    <meta property="og:site_name" content="@yield('title',@$lang->title_seo)" />
    <meta property="og:title" content="@yield('title',@$lang->title_seo)">
    <meta property="og:description" content="@yield('description',@$lang->description_seo)" />
    <meta property="og:locale" content="fa_ir" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:image" content="@yield('image_seo',asset('assets/uploads/content/'.@$setting_main->logo))" />
    <meta property="og:type" content="article" />
    <meta property="og:type" content="website" />
    <meta name="title" content="@yield('title',@$lang->title_seo)">
</head>
